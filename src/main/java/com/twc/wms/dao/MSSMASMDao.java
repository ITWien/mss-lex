/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.MSSMASM;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class MSSMASMDao extends database {

    public MSSMASM findByCod(String cod) {

        MSSMASM p = new MSSMASM();

        String sql = "SELECT TOP 1 * FROM MSSMASM WHERE MATNR like ? + '%' ";

        try {

            if (cod.equals("")) {
                sql = "SELECT TOP 1 * FROM MSSMASM WHERE MATNR = ? ";
            }

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, cod);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setMatGroup(result.getString("MATNR"));
                p.setDesc(result.getString("ARKTX"));
                p.setSpec(result.getString("MATCOM"));
                p.setPrice(result.getString("PRICE"));
                p.setUnit(result.getString("VRKME"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public MSSMASM findByCodFromSAPMAS(String cod) {

        MSSMASM p = new MSSMASM();

        String sql = "SELECT TOP 1 SAPDESC, SAPBUN "
                + "FROM SAPMAS "
                + "WHERE SAPMAT like ? + '%' ";

        try {

            if (cod.equals("")) {
                sql = "SELECT TOP 1 SAPDESC, SAPBUN "
                        + "FROM SAPMAS "
                        + "WHERE SAPMAT = ? ";
            }

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, cod);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setDesc(result.getString("SAPDESC"));
                p.setUnit(result.getString("SAPBUN"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public List<MSSMASM> selectMatGroup() {

        List<MSSMASM> QMList = new ArrayList<MSSMASM>();

        String sql = "SELECT DISTINCT MATNR FROM MSSMASM "
                + "ORDER BY MATNR ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMASM p = new MSSMASM();

                String matG = result.getString("MATNR").substring(0, 8);

                p.setMatGroup(matG.trim());

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<MSSMASM> selectDesc() {

        List<MSSMASM> QMList = new ArrayList<MSSMASM>();

        String sql = "SELECT DISTINCT ARKTX FROM MSSMASM "
                + "ORDER BY ARKTX ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMASM p = new MSSMASM();

                p.setDesc(result.getString("ARKTX"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<MSSMASM> selectPrice() {

        List<MSSMASM> QMList = new ArrayList<MSSMASM>();

        String sql = "SELECT DISTINCT PRICE FROM MSSMASM "
                + "ORDER BY PRICE ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMASM p = new MSSMASM();

                p.setPrice(result.getString("PRICE"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<MSSMASM> selectUnit() {

        List<MSSMASM> QMList = new ArrayList<MSSMASM>();

        String sql = "SELECT DISTINCT VRKME FROM MSSMASM "
                + "ORDER BY VRKME ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMASM p = new MSSMASM();

                p.setUnit(result.getString("VRKME"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<MSSMASM> selectSpec() {

        List<MSSMASM> QMList = new ArrayList<MSSMASM>();

        String sql = "SELECT DISTINCT MATCOM FROM MSSMASM "
                + "ORDER BY MATCOM ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMASM p = new MSSMASM();

                p.setSpec(result.getString("MATCOM"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

}
