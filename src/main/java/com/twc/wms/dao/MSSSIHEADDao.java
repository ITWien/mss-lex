/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.MSSSIHEAD;
import com.twc.wms.entity.UserAuth;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class MSSSIHEADDao extends database {

    public String findMaxQno() {

        String p = "";

        String sql = "SELECT ISNULL(MAX(MSSSIHQNO),0) AS MAXQNO FROM MSSSIHEAD ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = result.getString("MAXQNO");

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public List<MSSSIHEAD> findAll() {

        List<MSSSIHEAD> MSSSIHEADList = new ArrayList<MSSSIHEAD>();

        String sql = "SELECT [MSSSIHQNO]\n"
                + "      ,FORMAT([MSSSIHLOADDT],'dd-MM-yyyy') AS [MSSSIHLOADDT]\n"
                + "      ,[MSSSIHCUR]\n"
                + "      ,[MSSSIHREMARK]\n"
                + "      ,ISNULL([MSSSIHUSER],'') AS MSSSIHUSER\n"
                + "	  ,ISNULL([USERS],'') AS [USERS]\n"
                + "  FROM [RMShipment].[dbo].[MSSSIHEAD]\n"
                + "  LEFT JOIN MSSUSER ON [USERID] = [MSSSIHUSER]";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSSIHEAD p = new MSSSIHEAD();

                p.setMSSSIHQNO(result.getString("MSSSIHQNO"));
                p.setMSSSIHLOADDT(result.getString("MSSSIHLOADDT"));
                p.setMSSSIHCUR(result.getString("MSSSIHCUR"));
                p.setMSSSIHREMARK(result.getString("MSSSIHREMARK"));
                p.setMSSSIHUSER(result.getString("MSSSIHUSER") + " : " + result.getString("USERS"));

                MSSSIHEADList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return MSSSIHEADList;

    }

    public MSSSIHEAD findByQno(String qno) {

        MSSSIHEAD p = new MSSSIHEAD();

        String sql = "SELECT [MSSSIHQNO]\n"
                + "      ,FORMAT([MSSSIHLOADDT],'yyyy-MM-dd') AS [MSSSIHLOADDT]\n"
                + "      ,[MSSSIHCUR]\n"
                + "      ,[MSSSIHREMARK]\n"
                + "      ,ISNULL([MSSSIHUSER],'') AS MSSSIHUSER\n"
                + "	  ,ISNULL([USERS],'') AS [USERS]\n"
                + "  FROM [RMShipment].[dbo].[MSSSIHEAD]\n"
                + "  LEFT JOIN MSSUSER ON [USERID] = [MSSSIHUSER]\n"
                + "  WHERE MSSSIHQNO = '" + qno + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setMSSSIHQNO(result.getString("MSSSIHQNO"));
                p.setMSSSIHLOADDT(result.getString("MSSSIHLOADDT"));
                p.setMSSSIHCUR(result.getString("MSSSIHCUR"));
                p.setMSSSIHREMARK(result.getString("MSSSIHREMARK"));
                p.setMSSSIHUSER(result.getString("MSSSIHUSER") + " : " + result.getString("USERS"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public boolean edit(String qno, String loadDate, String ccy, String remark) {

        boolean result = false;

        String sql = "UPDATE MSSSIHEAD "
                + "SET [MSSSIHCUR] = '" + ccy + "'\n"
                + "      ,[MSSSIHLOADDT] = '" + loadDate + "'\n"
                + "      ,[MSSSIHREMARK] = '" + remark + "'\n"
                + "      ,[MSSSIHEDT] = CURRENT_TIMESTAMP\n"
                + "WHERE MSSSIHQNO = '" + qno + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

    public void delete(String qno) {

        String sql = "DELETE FROM MSSSIHEAD WHERE MSSSIHQNO = '" + qno + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
