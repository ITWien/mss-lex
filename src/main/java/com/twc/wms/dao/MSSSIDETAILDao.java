/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.MSSSIDETAIL;
import com.twc.wms.entity.UserAuth;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class MSSSIDETAILDao extends database {

    public boolean createDet(String[] inv, String uid) {

        boolean result = false;

        String list = "";
        for (int i = 0; i < inv.length; i++) {
            if (i == 0) {
                list += "'" + inv[i] + "'";
            } else {
                list += ",'" + inv[i] + "'";
            }
        }

        String sql = "INSERT INTO MSSSIHEAD \n"
                + "([MSSSIHCOM]\n"
                + "      ,[MSSSIHQNO]\n"
                + "      ,[MSSSIHLOADDT]\n"
                + "	  ,[MSSSIHCUR]\n"
                + "      ,[MSSSIHEDT]\n"
                + "      ,[MSSSIHCDT]\n"
                + "      ,[MSSSIHUSER])\n"
                + "VALUES ('TWC', (SELECT ISNULL(MAX(MSSSIDQNO),0)+1 FROM MSSSIDETAIL), CURRENT_TIMESTAMP, '', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + uid + "')\n"
                + "INSERT INTO MSSSIDETAIL \n"
                + "([MSSSIDCOM]\n"
                + "      ,[MSSSIDQNO]\n"
                + "      ,[MSSSIDINVNO]\n"
                + "      ,[MSSSIDCIF]\n"
                + "      ,[MSSSIDFOB]\n"
                + "      ,[MSSSIDCTN]\n"
                + "      ,[MSSSIDMETER]\n"
                + "      ,[MSSSIDPC]\n"
                + "      ,[MSSSIDSET]\n"
                + "      ,[MSSSIDCONE]\n"
                + "      ,[MSSSIDKGS]\n"
                + "      ,[MSSSIDNW]\n"
                + "      ,[MSSSIDGW]\n"
                + "      ,[MSSSIDM3]\n"
                + "      ,[MSSSIDEDT]\n"
                + "      ,[MSSSIDCDT]\n"
                + "      ,[MSSSIDUSER])\n"
                + "SELECT COM, QNO, INV, CIF, CIF AS FOB\n"
                + ",(SELECT MAX(SWCARNO) FROM SWGLINE WHERE SWIVNO = TMPFOB.INV) AS CTN\n"
                + ",(\n"
                + "SELECT SUM(SWQUANTY)\n"
                + "FROM(\n"
                + "SELECT SWQUANTY\n"
                + ",(SELECT TOP 1 QCUUM FROM QCUSTOMS WHERE QCUMAT = SUBSTRING(SWITEM,1,8)) AS UNIT\n"
                + "FROM SWGLINE \n"
                + "WHERE SWIVNO = INV\n"
                + "AND SWITEM != ''\n"
                + ")TMPMETER\n"
                + "WHERE UNIT = 'MTR'\n"
                + ") AS METER\n"
                + ",(\n"
                + "SELECT SUM(SWQUANTY)\n"
                + "FROM(\n"
                + "SELECT SWQUANTY\n"
                + ",(SELECT TOP 1 QCUUM FROM QCUSTOMS WHERE QCUMAT = SUBSTRING(SWITEM,1,8)) AS UNIT\n"
                + "FROM SWGLINE \n"
                + "WHERE SWIVNO = INV\n"
                + "AND SWITEM != ''\n"
                + ")TMPMETER\n"
                + "WHERE UNIT = 'PC'\n"
                + ") AS PC\n"
                + ",(\n"
                + "SELECT SUM(SWQUANTY)\n"
                + "FROM(\n"
                + "SELECT SWQUANTY\n"
                + ",(SELECT TOP 1 QCUUM FROM QCUSTOMS WHERE QCUMAT = SUBSTRING(SWITEM,1,8)) AS UNIT\n"
                + "FROM SWGLINE \n"
                + "WHERE SWIVNO = INV\n"
                + "AND SWITEM != ''\n"
                + ")TMPMETER\n"
                + "WHERE UNIT = 'SET'\n"
                + ") AS [SET]\n"
                + ",(\n"
                + "SELECT SUM(SWQUANTY)\n"
                + "FROM(\n"
                + "SELECT SWQUANTY\n"
                + ",(SELECT TOP 1 QCUUM FROM QCUSTOMS WHERE QCUMAT = SUBSTRING(SWITEM,1,8)) AS UNIT\n"
                + "FROM SWGLINE \n"
                + "WHERE SWIVNO = INV\n"
                + "AND SWITEM != ''\n"
                + ")TMPMETER\n"
                + "WHERE UNIT = 'CONE'\n"
                + ") AS CONE\n"
                + ",(\n"
                + "SELECT SUM(SWQUANTY)\n"
                + "FROM(\n"
                + "SELECT SWQUANTY\n"
                + ",(SELECT TOP 1 QCUUM FROM QCUSTOMS WHERE QCUMAT = SUBSTRING(SWITEM,1,8)) AS UNIT\n"
                + "FROM SWGLINE \n"
                + "WHERE SWIVNO = INV\n"
                + "AND SWITEM != ''\n"
                + ")TMPMETER\n"
                + "WHERE UNIT = 'KG'\n"
                + ") AS KGS\n"
                + ",(SELECT SUM(SWNWEIGHT) FROM SWGLINE WHERE SWIVNO = TMPFOB.INV AND SWITEM != '') AS NW\n"
                + ",(SELECT SUM(SWGWEIGHT) FROM SWGLINE WHERE SWIVNO = TMPFOB.INV) AS GW\n"
                + ",(SELECT SUM((SWDMLWD/100)*(SWDMLEN/100)*(SWDMHG/100)) FROM SWGLINE WHERE SWIVNO = TMPFOB.INV) AS M3\n"
                + ",CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '" + uid + "'\n"
                + "FROM(\n"
                + "SELECT 'TWC' AS COM, (SELECT ISNULL(MAX(MSSSIDQNO),0)+1 FROM MSSSIDETAIL) AS QNO\n"
                + ",INV\n"
                + ",(\n"
                + "SELECT SUM(CIF)\n"
                + "FROM(\n"
                + "SELECT \n"
                + "(SELECT cast((SUM(SWQUANTY) * PRICE) as decimal(18,2)) FROM SWGLINE WHERE SWITEM like TMP3.SWITEM + '%' and SWIVNO = TMP.INV) AS CIF\n"
                + "FROM(\n"
                + "SELECT SWITEM\n"
                + ",(SELECT TOP 1 QCUPRICE FROM QCUSTOMS WHERE QCUMAT = SWITEM) AS PRICE\n"
                + "FROM(\n"
                + "SELECT DISTINCT SUBSTRING(SWITEM,1,8) AS SWITEM\n"
                + "  FROM SWGLINE\n"
                + "  WHERE SWIVNO = TMP.INV\n"
                + "AND SWITEM != ''\n"
                + ")TMP2\n"
                + ")TMP3\n"
                + ")TMPCIF\n"
                + ") AS CIF\n"
                + "FROM(\n"
                + "SELECT DISTINCT SWIVNO AS INV\n"
                + "FROM SWGLINE SWG\n"
                + "WHERE SWIVNO IN (" + list + ")\n"
                + ")TMP\n"
                + ")TMPFOB\n"
                + "ORDER BY INV";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

    public ResultSet findForPrintMSS214(String qno) throws SQLException {

        String sql = "SELECT [MSSSIHQNO]\n"
                + "      ,FORMAT([MSSSIHLOADDT],'dd-MM-yyyy') AS [MSSSIHLOADDT]\n"
                + "      ,[MSSSIHCUR]\n"
                + "      ,[MSSSIHREMARK]\n"
                + "      ,[MSSSIDQNO]\n"
                + "      ,[MSSSIDINVNO]\n"
                + "      ,[MSSSIDDEPT]\n"
                + "      ,[MSSSIDCIF]\n"
                + "      ,[MSSSIDINS]\n"
                + "      ,[MSSSIDFRE]\n"
                + "      ,[MSSSIDFOB]\n"
                + "      ,[MSSSIDCTN]\n"
                + "      ,[MSSSIDMETER]\n"
                + "      ,[MSSSIDPC]\n"
                + "      ,[MSSSIDSET]\n"
                + "      ,[MSSSIDCONE]\n"
                + "      ,[MSSSIDKGS]\n"
                + "      ,[MSSSIDNW]\n"
                + "      ,[MSSSIDGW]\n"
                + "      ,[MSSSIDM3]\n"
                + "FROM MSSSIDETAIL\n"
                + "LEFT JOIN MSSSIHEAD ON MSSSIHQNO = MSSSIDQNO\n"
                + "WHERE MSSSIDQNO = '" + qno + "'";
        ResultSet result = null;

        try {
            PreparedStatement ps = connect.prepareStatement(sql);

            result = ps.executeQuery();
        } catch (Exception e) {

            e.printStackTrace();

        }

        return result;

    }

    public List<MSSSIDETAIL> findByQno(String qno) {

        List<MSSSIDETAIL> MSSSIDETAILList = new ArrayList<MSSSIDETAIL>();

        String sql = "SELECT [MSSSIDQNO]\n"
                + "      ,[MSSSIDINVNO]\n"
                + "      ,[MSSSIDDEPT]\n"
                + "      ,ISNULL(FORMAT([MSSSIDCIF],'#,#0.00'),'-') AS [MSSSIDCIF]\n"
                + "      ,FORMAT([MSSSIDINS],'#,#0.00') AS [MSSSIDINS]\n"
                + "      ,FORMAT([MSSSIDFRE],'#,#0.00') AS [MSSSIDFRE]\n"
                + "      ,ISNULL(FORMAT([MSSSIDFOB],'#,#0.00'),'-') AS [MSSSIDFOB]\n"
                + "      ,ISNULL(FORMAT([MSSSIDCTN],'#,#0'),'-') AS [MSSSIDCTN]\n"
                + "      ,ISNULL(FORMAT([MSSSIDMETER],'#,#0.00'),'-') AS [MSSSIDMETER]\n"
                + "      ,ISNULL(FORMAT([MSSSIDPC],'#,#0.00'),'-') AS [MSSSIDPC]\n"
                + "      ,ISNULL(FORMAT([MSSSIDSET],'#,#0.00'),'-') AS [MSSSIDSET]\n"
                + "      ,ISNULL(FORMAT([MSSSIDCONE],'#,#0.00'),'-') AS [MSSSIDCONE]\n"
                + "      ,ISNULL(FORMAT([MSSSIDKGS],'#,#0.00'),'-') AS [MSSSIDKGS]\n"
                + "      ,ISNULL(FORMAT([MSSSIDNW],'#,#0.00'),'-') AS [MSSSIDNW]\n"
                + "      ,ISNULL(FORMAT([MSSSIDGW],'#,#0.00'),'-') AS [MSSSIDGW]\n"
                + "      ,ISNULL(FORMAT([MSSSIDM3],'#,#0.000'),'-') AS [MSSSIDM3]\n"
                + "  FROM [RMShipment].[dbo].[MSSSIDETAIL]\n"
                + "  WHERE [MSSSIDQNO] = '" + qno + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSSIDETAIL p = new MSSSIDETAIL();

                p.setMSSSIDQNO(result.getString("MSSSIDQNO"));
                p.setMSSSIDINVNO(result.getString("MSSSIDINVNO"));
                p.setMSSSIDDEPT(result.getString("MSSSIDDEPT"));
                p.setMSSSIDCIF(result.getString("MSSSIDCIF"));
                p.setMSSSIDINS(result.getString("MSSSIDINS"));
                p.setMSSSIDFRE(result.getString("MSSSIDFRE"));
                p.setMSSSIDFOB(result.getString("MSSSIDFOB"));
                p.setMSSSIDCTN(result.getString("MSSSIDCTN"));
                p.setMSSSIDMETER(result.getString("MSSSIDMETER"));
                p.setMSSSIDPC(result.getString("MSSSIDPC"));
                p.setMSSSIDSET(result.getString("MSSSIDSET"));
                p.setMSSSIDCONE(result.getString("MSSSIDCONE"));
                p.setMSSSIDKGS(result.getString("MSSSIDKGS"));
                p.setMSSSIDNW(result.getString("MSSSIDNW"));
                p.setMSSSIDGW(result.getString("MSSSIDGW"));
                p.setMSSSIDM3(result.getString("MSSSIDM3"));

                MSSSIDETAILList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return MSSSIDETAILList;

    }

    public boolean addDet(String qno, String inv, String dept, String cif,
            String ins, String fre,
            String fob, String ctn,
            String meter, String pc,
            String set, String cone,
            String kgs, String nw,
            String gw, String m3,
            String uid) {

        boolean result = false;

        String sql = "INSERT INTO MSSSIDETAIL \n"
                + "([MSSSIDCOM]\n"
                + "      ,[MSSSIDQNO]\n"
                + "      ,[MSSSIDINVNO]\n"
                + "      ,[MSSSIDDEPT]\n"
                + "      ,[MSSSIDCIF]\n"
                + "      ,[MSSSIDINS]\n"
                + "      ,[MSSSIDFRE]\n"
                + "      ,[MSSSIDFOB]\n"
                + "      ,[MSSSIDCTN]\n"
                + "      ,[MSSSIDMETER]\n"
                + "      ,[MSSSIDPC]\n"
                + "      ,[MSSSIDSET]\n"
                + "      ,[MSSSIDCONE]\n"
                + "      ,[MSSSIDKGS]\n"
                + "      ,[MSSSIDNW]\n"
                + "      ,[MSSSIDGW]\n"
                + "      ,[MSSSIDM3]\n"
                + "      ,[MSSSIDEDT]\n"
                + "      ,[MSSSIDCDT]\n"
                + "      ,[MSSSIDUSER])\n"
                + "VALUES ('TWC'"
                + ",'" + qno + "'"
                + ",'" + inv + "'"
                + ",'" + dept + "'"
                + ",CASE WHEN '" + cif + "' = '' THEN null ELSE '" + cif + "' END"
                + ",CASE WHEN '" + ins + "' = '' THEN null ELSE '" + ins + "' END"
                + ",CASE WHEN '" + fre + "' = '' THEN null ELSE '" + fre + "' END"
                + ",CASE WHEN '" + fob + "' = '' THEN null ELSE '" + fob + "' END"
                + ",CASE WHEN '" + ctn + "' = '' THEN null ELSE '" + ctn + "' END"
                + ",CASE WHEN '" + meter + "' = '' THEN null ELSE '" + meter + "' END"
                + ",CASE WHEN '" + pc + "' = '' THEN null ELSE '" + pc + "' END"
                + ",CASE WHEN '" + set + "' = '' THEN null ELSE '" + set + "' END"
                + ",CASE WHEN '" + cone + "' = '' THEN null ELSE '" + cone + "' END"
                + ",CASE WHEN '" + kgs + "' = '' THEN null ELSE '" + kgs + "' END"
                + ",CASE WHEN '" + nw + "' = '' THEN null ELSE '" + nw + "' END"
                + ",CASE WHEN '" + gw + "' = '' THEN null ELSE '" + gw + "' END"
                + ",CASE WHEN '" + m3 + "' = '' THEN null ELSE '" + m3 + "' END"
                + ",CURRENT_TIMESTAMP"
                + ",CURRENT_TIMESTAMP"
                + ",'" + uid + "')";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

    public boolean edit(String qno, String inv, String dept, String ins, String fre, String fob) {

        boolean result = false;

        String sql = "UPDATE MSSSIDETAIL "
                + "SET [MSSSIDDEPT] = '" + dept + "'\n"
                + "      ,[MSSSIDINS] = '" + ins + "'\n"
                + "      ,[MSSSIDFRE] = '" + fre + "'\n"
                + "      ,[MSSSIDFOB] = '" + fob + "'\n"
                + "      ,[MSSSIDEDT] = CURRENT_TIMESTAMP\n"
                + "WHERE MSSSIDQNO = '" + qno + "' AND MSSSIDINVNO = '" + inv + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

    public void deleteDet(String qno, String inv) {

        String sql = "DELETE FROM MSSSIDETAIL WHERE MSSSIDQNO = '" + qno + "' AND MSSSIDINVNO = '" + inv + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

    public void delete(String qno) {

        String sql = "DELETE FROM MSSSIDETAIL WHERE MSSSIDQNO = '" + qno + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
