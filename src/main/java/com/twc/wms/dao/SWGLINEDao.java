/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.MSSMASM;
import com.twc.wms.entity.MSSMASH;
import com.twc.wms.entity.SWGLINE;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class SWGLINEDao extends database {

    public List<SWGLINE> findAllInv() {

        List<SWGLINE> SWGList = new ArrayList<SWGLINE>();

        String sql = "SELECT DISTINCT cast(substring(SWIVNO,4,len(SWIVNO)-6) as int) as NUM, \n"
                + "                cast(substring(SWIVNO,len(SWIVNO)-1,len(SWIVNO)) as int) as YY,\n"
                + "				SWG.SWIVNO AS INV\n"
                + "FROM SWGLINE SWG\n"
                + "ORDER BY cast(substring(SWIVNO,len(SWIVNO)-1,len(SWIVNO)) as int) desc,\n"
                + "         cast(substring(SWIVNO,4,len(SWIVNO)-6) as int) desc";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                SWGLINE p = new SWGLINE();

                p.setInv(result.getString("INV"));

                SWGList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return SWGList;

    }

    public List<MSSMASM> findMatCodeList1(String invoice, String date, String cn1from, String cn1to) {

        List<MSSMASM> QMList = new ArrayList<MSSMASM>();

        String cn1 = "";

        if (cn1from != null) {
            if (!cn1from.equals("")) {
                cn1 = "AND (SWCARNO BETWEEN '" + cn1from + "' AND '" + cn1to + "') ";
            }
        }

        String sql = "SELECT DISTINCT SUBSTRING(SWITEM,1,8) AS SWITEM, SWCARNO "
                + "FROM SWGLINE "
                + "WHERE SWIVNO = '" + invoice + "' "
                + "AND SWITEM != '' "
                + cn1
                + "ORDER BY SWITEM ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMASM p = new MSSMASM();

                p.setMatGroup(result.getString("SWITEM"));
                p.setUnit(result.getString("SWCARNO"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<MSSMASM> findMatCodeList2(String invoice, String date, String cn2from, String cn2to) {

        List<MSSMASM> QMList = new ArrayList<MSSMASM>();

        String cn2 = "";

        if (cn2from != null) {
            if (!cn2from.equals("")) {
                cn2 = "AND (SWCARNO BETWEEN '" + cn2from + "' AND '" + cn2to + "') ";
            }
        }

        String sql = "SELECT DISTINCT SUBSTRING(SWITEM,1,8) AS SWITEM, SWCARNO "
                + "FROM SWGLINE "
                + "WHERE SWIVNO = '" + invoice + "' "
                + "AND SWITEM != '' "
                + cn2
                + "ORDER BY SWITEM ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMASM p = new MSSMASM();

                p.setMatGroup(result.getString("SWITEM"));
                p.setUnit(result.getString("SWCARNO"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<MSSMASM> findMatCodeList3(String invoice, String date, String cn3from, String cn3to) {

        List<MSSMASM> QMList = new ArrayList<MSSMASM>();

        String cn3 = "";

        if (cn3from != null) {
            if (!cn3from.equals("")) {
                cn3 = "AND (SWCARNO BETWEEN '" + cn3from + "' AND '" + cn3to + "') ";
            }
        }

        String sql = "SELECT DISTINCT SUBSTRING(SWITEM,1,8) AS SWITEM, SWCARNO "
                + "FROM SWGLINE "
                + "WHERE SWIVNO = '" + invoice + "' "
                + "AND SWITEM != '' "
                + cn3
                + "ORDER BY SWITEM ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMASM p = new MSSMASM();

                p.setMatGroup(result.getString("SWITEM"));
                p.setUnit(result.getString("SWCARNO"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<MSSMASM> findMatCodeList(String invoice, String date) {

        List<MSSMASM> QMList = new ArrayList<MSSMASM>();

        String sql = "SELECT DISTINCT SUBSTRING(SWITEM,1,8) AS SWITEM, SWCARNO "
                + "FROM SWGLINE "
                + "WHERE SWIVNO = '" + invoice + "' "
                + "AND SWITEM != '' "
                + "ORDER BY SWITEM ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMASM p = new MSSMASM();

                p.setMatGroup(result.getString("SWITEM"));
                p.setUnit(result.getString("SWCARNO"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<MSSMASH> findCusList() {

        List<MSSMASH> QMList = new ArrayList<MSSMASH>();

        String sql = "SELECT DISTINCT POFG, NAME1, ADDR1, ADDR2 "
                + "FROM MSSMASH ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMASH p = new MSSMASH();

                p.setCode(result.getString("POFG"));
                p.setName(result.getString("NAME1"));
                p.setAddr1(result.getString("ADDR1"));
                p.setAddr2(result.getString("ADDR2"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<MSSMASM> findInvList() {

        List<MSSMASM> QMList = new ArrayList<MSSMASM>();
        List<String> tmp = new ArrayList<String>();

        String sql = "SELECT DISTINCT cast(substring(SWIVNO,4,len(SWIVNO)-6) as int) as NUM, \n"
                + "                cast(substring(SWIVNO,len(SWIVNO)-1,len(SWIVNO)) as int) as YY,\n"
                + "				SWIVNO\n"
                + "FROM SWGLINE \n"
                + "ORDER BY cast(substring(SWIVNO,len(SWIVNO)-1,len(SWIVNO)) as int) desc, \n"
                + "         cast(substring(SWIVNO,4,len(SWIVNO)-6) as int) desc";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMASM p = new MSSMASM();

                p.setDesc(result.getString("SWIVNO"));

                String car = "0";

                if (result.getString("SWIVNO") != null) {
                    car = findCN(result.getString("SWIVNO"));
                }

                p.setUnit(car);

                if (!tmp.contains(result.getString("SWIVNO"))) {
                    tmp.add(result.getString("SWIVNO"));
                    QMList.add(p);
                }

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public String findCN(String inv) {

        String p = "";

        String sql = "select max(SWCARNO) as CAR "
                + "from SWGLINE "
                + "where SWIVNO = '" + inv + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = result.getString("CAR");

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public List<MSSMASM> findInvList213() {

        List<MSSMASM> QMList = new ArrayList<MSSMASM>();

        String sql = "SELECT DISTINCT cast(substring(GRPNO,4,len(GRPNO)-6) as int) as NUM, \n"
                + "                cast(substring(GRPNO,len(GRPNO)-1,len(GRPNO)) as int) as YY,\n"
                + "				GRPNO\n"
                + "FROM MSSMASH \n"
                + "WHERE GRPNO like 'MWC%' \n"
                + "ORDER BY cast(substring(GRPNO,len(GRPNO)-1,len(GRPNO)) as int) desc, \n"
                + "         cast(substring(GRPNO,4,len(GRPNO)-6) as int) desc";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMASM p = new MSSMASM();

                p.setDesc(result.getString("GRPNO"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public List<MSSMASM> findCNList() {

        List<MSSMASM> QMList = new ArrayList<MSSMASM>();

        String sql = "SELECT DISTINCT SWCARNO "
                + "FROM SWGLINE "
                + "ORDER BY SWCARNO ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSSMASM p = new MSSMASM();

                p.setDesc(result.getString("SWCARNO"));

                QMList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QMList;

    }

    public double findNW(String inv, int crS, int crF) {

        double p = 0;
        String carS = Integer.toString(crS);
        String carF = Integer.toString(crF);

        String sql = "SELECT SUM(SWNWEIGHT) AS NW "
                + "FROM SWGLINE "
                + "WHERE SWIVNO = '" + inv + "' "
                + "and SWITEM != '' "
                + "AND (SWCARNO BETWEEN '" + carS + "' AND '" + carF + "') ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = Double.parseDouble(result.getString("NW"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public double findNW215(String inv, int crS, int crF, int og) {

        double p = 0;
        String carS = Integer.toString(crS);
        String carF = Integer.toString(crF);

        String where = "";
        if (og == 1) {
            where = "WHERE QCUCOUNTRY = '' OR QCUCOUNTRY = 'Unknown'";
        } else if (og == 2) {
            where = "WHERE QCUCOUNTRY != '' AND QCUCOUNTRY != 'Unknown'";
        }

        String sql = "SELECT SUM(NW) AS NW\n"
                + "FROM(\n"
                + "SELECT SWNWEIGHT AS NW, SUBSTRING(SWITEM,1,8) AS SWITEM\n"
                + ",isnull((SELECT TOP 1 QCUCOUNTRY FROM QCUSTOMS WHERE QCUMAT = SUBSTRING(SWITEM,1,8)),'') as QCUCOUNTRY\n"
                + "FROM SWGLINE \n"
                + "WHERE SWIVNO = '" + inv + "' \n"
                + "and SWITEM != '' \n"
                + "AND (SWCARNO BETWEEN '" + carS + "' AND '" + carF + "') \n"
                + ")TMP\n"
                + where;

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = Double.parseDouble(result.getString("NW"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public double findGW(String inv, int crS, int crF) {

        double p = 0;
        String carS = Integer.toString(crS);
        String carF = Integer.toString(crF);

        String sql = "SELECT SUM(SWGWEIGHT) AS GW "
                + "FROM SWGLINE "
                + "WHERE SWIVNO = '" + inv + "' "
                + "AND (SWCARNO BETWEEN '" + carS + "' AND '" + carF + "') ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = Double.parseDouble(result.getString("GW"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public double findGW215(String inv, int crS, int crF, int og) {

        double p = 0;
        String carS = Integer.toString(crS);
        String carF = Integer.toString(crF);

        String where = "";
        if (og == 1) {
            where = "WHERE QCUCOUNTRY = '' OR QCUCOUNTRY = 'Unknown'";
        } else if (og == 2) {
            where = "WHERE QCUCOUNTRY != '' AND QCUCOUNTRY != 'Unknown'";
        }

        String sql = "SELECT SUM(GW) AS GW\n"
                + "FROM(\n"
                + "SELECT SWGWEIGHT AS GW, SUBSTRING(SWITEM,1,8) AS SWITEM\n"
                + ",isnull((SELECT TOP 1 QCUCOUNTRY FROM QCUSTOMS WHERE QCUMAT = SUBSTRING(SWITEM,1,8)),'') as QCUCOUNTRY\n"
                + "FROM SWGLINE \n"
                + "WHERE SWIVNO = '" + inv + "' \n"
                + "AND (SWCARNO BETWEEN '" + carS + "' AND '" + carF + "') \n"
                + ")TMP\n"
                + where;

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = Double.parseDouble(result.getString("GW"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public double findMS(String inv, int crS, int crF) {

        double p = 0;
        String carS = Integer.toString(crS);
        String carF = Integer.toString(crF);

        String sql = "SELECT SUM((SWDMLWD/100)*(SWDMLEN/100)*(SWDMHG/100)) as MSM "
                + "FROM SWGLINE "
                + "WHERE SWIVNO = '" + inv + "' "
                + "AND (SWCARNO BETWEEN '" + carS + "' AND '" + carF + "') ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = Double.parseDouble(result.getString("MSM"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public double findMS215(String inv, int crS, int crF, int og) {

        double p = 0;
        String carS = Integer.toString(crS);
        String carF = Integer.toString(crF);

        String where = "";
        if (og == 1) {
            where = "WHERE QCUCOUNTRY = '' OR QCUCOUNTRY = 'Unknown'";
        } else if (og == 2) {
            where = "WHERE QCUCOUNTRY != '' AND QCUCOUNTRY != 'Unknown'";
        }

        String sql = "SELECT SUM(MSM) AS MSM\n"
                + "FROM(\n"
                + "SELECT (SWDMLWD/100)*(SWDMLEN/100)*(SWDMHG/100) AS MSM, SUBSTRING(SWITEM,1,8) AS SWITEM\n"
                + ",isnull((SELECT TOP 1 QCUCOUNTRY FROM QCUSTOMS WHERE QCUMAT = SUBSTRING(SWITEM,1,8)),'') as QCUCOUNTRY\n"
                + "FROM SWGLINE \n"
                + "WHERE SWIVNO = '" + inv + "' \n"
                + "AND (SWCARNO BETWEEN '" + carS + "' AND '" + carF + "') \n"
                + ")TMP\n"
                + where;

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = Double.parseDouble(result.getString("MSM"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String findNW(String mat, String inv) {

        String p = "";

        String sql = "select sum(SWNWEIGHT) as NW "
                + "from SWGLINE "
                + "where SWITEM like '" + mat + "'+'%' and SWIVNO = '" + inv + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = (result.getString("NW"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String findQTTY(String mat, String inv) {

        String p = "";

        String sql = "select sum(ROUND(SWQUANTY, 2, 1)) as QTTY "
                + "from SWGLINE "
                + "where SWITEM like '" + mat + "'+'%' and SWIVNO = '" + inv + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = (result.getString("QTTY"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public String findAMOUNT(String mat, String inv, double price) {

        String p = "";

        String pr = Double.toString(price);

        String sql = "select sum(ROUND(SWQUANTY, 2, 1)) * " + pr + " as AMOUNT "
                + "from SWGLINE "
                + "where SWITEM like '" + mat + "'+'%' and SWIVNO = '" + inv + "' ";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p = (result.getString("AMOUNT"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

}
