/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.QCUSTOMS;
import com.twc.wms.entity.UserAuth;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class QCUSTOMSDao extends database {

    public List<QCUSTOMS> findAllPrint() {

        List<QCUSTOMS> QCUSTOMSList = new ArrayList<QCUSTOMS>();

        String sql = "SELECT [QCUMAT]\n"
                + "      ,[QCUDESCE]\n"
                + "      ,[QCUSPEC]\n"
                + "      ,[QCUNAME]\n"
                + "      ,[QCUBRAND]\n"
                + "      ,[QCUWIDTH]\n"
                + "      ,[QCUUM]\n"
                + "      ,[QCUPRICE]\n"
                + "      ,[QCUDESCT]\n"
                + "      ,[QCUUMC]\n"
                + "      ,[QCUCOUNTRY]\n"
                + "      ,[QCUHSC]\n"
                + "      ,[QCUTAF]\n"
                + "      ,[QCUREFHS]\n"
                + "      ,[QCUCOMP]\n"
                + "      ,[QCUREF]\n"
                + "      ,FORMAT([QCUISSDT],'yyyy-MM-dd') AS [QCUISSDT]\n"
                + "      ,[QCUNODAY]\n"
                + "      ,FORMAT([QCUEXPDT],'yyyy-MM-dd') AS [QCUEXPDT]\n"
                + "      ,FORMAT([QCUWANDT],'yyyy-MM-dd') AS [QCUWANDT]\n"
                + "  FROM QCUSTOMS\n";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QCUSTOMS p = new QCUSTOMS();

                p.setMatGroup(result.getString("QCUMAT"));
                p.setDescE(result.getString("QCUDESCE"));
                p.setSpec(result.getString("QCUSPEC"));
                p.setDesc(result.getString("QCUNAME"));
                p.setBrand(result.getString("QCUBRAND"));
                p.setWidth(result.getString("QCUWIDTH"));
                p.setUnit(result.getString("QCUUM"));
                p.setPrice(result.getString("QCUPRICE"));
                p.setDescT(result.getString("QCUDESCT"));
                p.setUnitC(result.getString("QCUUMC"));
                p.setCountry(result.getString("QCUCOUNTRY"));
                p.setHSC(result.getString("QCUHSC"));
                p.setTaf(result.getString("QCUTAF"));
                p.setRefHS(result.getString("QCUREFHS"));
                p.setComp(result.getString("QCUCOMP"));
                p.setRef(result.getString("QCUREF"));
                p.setIssdt(result.getString("QCUISSDT"));
                p.setNoday(result.getString("QCUNODAY"));
                p.setExpdt(result.getString("QCUEXPDT"));
                p.setWandt(result.getString("QCUWANDT"));

                QCUSTOMSList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QCUSTOMSList;

    }

    public List<QCUSTOMS> findAll() {

        List<QCUSTOMS> QCUSTOMSList = new ArrayList<QCUSTOMS>();

        String sql = "SELECT QCUMAT, QCUNAME, QCUSPEC, QCUBRAND, QCUPRICE\n"
                + ", FORMAT(QCUCDT,'yyyy-MM-dd') AS QCUCDT\n"
                + ",QCUCOMP, QCUDESCE, QCUDESCT, QCUWIDTH, QCUCOUNTRY, QCUREF\n"
                + ",QCUREFHS, QCUTAF, QCUUM, QCUHSC\n"
                + ", FORMAT(QCUEDTPUR,'yyyy-MM-dd') AS QCUEDTPUR\n"
                + ",ISNULL(QCUUSERPUR+' : '+UP.USERS,'XXX : XXX') AS QCUUSERPUR\n"
                + ", FORMAT(QCUEDTIMP,'yyyy-MM-dd') AS QCUEDTIMP\n"
                + ",ISNULL(QCUUSERIMP+' : '+UI.USERS,'XXX : XXX') AS QCUUSERIMP, ISNULL(QCUUSER+' : '+U.USERS,'XXX : XXX') AS QCUUSER\n"
                + ", FORMAT(QCUEDT,'yyyy-MM-dd') AS QCUEDT\n"
                + ",QCUUMC\n"
                + ", FORMAT(QCUISSDT,'yyyy-MM-dd') AS QCUISSDT\n"
                + ",QCUNODAY\n"
                + ", FORMAT(QCUEXPDT,'yyyy-MM-dd') AS QCUEXPDT\n"
                + ", FORMAT(QCUWANDT,'yyyy-MM-dd') AS QCUWANDT\n"
                + ", CASE WHEN FORMAT(QCUWANDT,'yyyy-MM-dd') = FORMAT(CURRENT_TIMESTAMP,'yyyy-MM-dd') THEN 'red' ELSE 'black' END AS SYSDT\n"
                + "FROM QCUSTOMS \n"
                + "LEFT JOIN MSSUSER U ON U.USERID = QCUSTOMS.QCUUSER\n"
                + "LEFT JOIN MSSUSER UP ON UP.USERID = QCUSTOMS.QCUUSERPUR\n"
                + "LEFT JOIN MSSUSER UI ON UI.USERID = QCUSTOMS.QCUUSERIMP";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QCUSTOMS p = new QCUSTOMS();

                String matG = result.getString("QCUMAT").trim();
                p.setMatGroup(matG);
                p.setDesc(result.getString("QCUNAME"));
                p.setSpec(result.getString("QCUSPEC"));
                p.setBrand(result.getString("QCUBRAND"));
                p.setPrice(result.getString("QCUPRICE"));
                p.setEditDate(result.getString("QCUEDT"));
                p.setCreateDate(result.getString("QCUCDT"));
                p.setComp(result.getString("QCUCOMP"));
                p.setDescE(result.getString("QCUDESCE"));
                p.setDescT(result.getString("QCUDESCT"));
                p.setWidth(result.getString("QCUWIDTH"));
                p.setCountry(result.getString("QCUCOUNTRY"));
                p.setRef(result.getString("QCUREF"));
                p.setExpDate(result.getString("QCUEXPDT"));
                p.setRefHS(result.getString("QCUREFHS"));
                p.setTaf(result.getString("QCUTAF"));
                p.setUnit(result.getString("QCUUM"));
                p.setHSC(result.getString("QCUHSC"));
                p.setEdtpur(result.getString("QCUEDTPUR"));

                if (result.getString("QCUUSERPUR").split(" ")[0].equals("XXX")) {
                    p.setUserpur("");
                } else {
                    p.setUserpur(result.getString("QCUUSERPUR").split(" ")[0] + " " + result.getString("QCUUSERPUR").split(" ")[1] + " " + result.getString("QCUUSERPUR").split(" ")[2]);

                }

                p.setEdtimp(result.getString("QCUEDTIMP"));

                if (result.getString("QCUUSERIMP").split(" ")[0].equals("XXX")) {
                    p.setUserimp("");
                } else {
                    p.setUserimp(result.getString("QCUUSERIMP").split(" ")[0] + " " + result.getString("QCUUSERIMP").split(" ")[1] + " " + result.getString("QCUUSERIMP").split(" ")[2]);
                }

                if (result.getString("QCUUSER").split(" ")[0].equals("XXX")) {
                    p.setUser("");
                } else {
                    p.setUser(result.getString("QCUUSER").split(" ")[0] + " " + result.getString("QCUUSER").split(" ")[1] + " " + result.getString("QCUUSER").split(" ")[2]);
                }

                p.setEdt(result.getString("QCUEDT"));
                p.setUnitC(result.getString("QCUUMC"));
                p.setIssdt(result.getString("QCUISSDT"));
                p.setNoday(result.getString("QCUNODAY"));
                p.setExpdt(result.getString("QCUEXPDT"));
                p.setWandt(result.getString("QCUWANDT"));

                p.setSysdt(result.getString("SYSDT"));

                QCUSTOMSList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QCUSTOMSList;

    }

    public List<QCUSTOMS> findAllMSS213(String inv, String sno) {

        List<QCUSTOMS> QCUSTOMSList = new ArrayList<QCUSTOMS>();

        String sql = "SELECT\n"
                + "   * , CASE WHEN VALUE IS NULL THEN 0.00 ELSE VALUE END AS VALUE2\n"
                + "FROM\n"
                + "   (\n"
                + "      SELECT\n"
                + "         SUBSTRING(MASH.GRPNO, 0 , 4) + RIGHT(MASH.GRPNO, 2) + RIGHT('000' + REPLACE(SUBSTRING(MASH.GRPNO, 4 , 2), '/', ''), 3) + RIGHT('000' + CONVERT(varchar(2), SEQN.ISSUENO), 3) AS RUNNO,\n"
                + "         MASD.BWTAR AS VT,\n"
                + "         MASM.PRICE AS PRICE,\n"
                + "         MASH.VBELN AS SaleNo,\n"
                + "         MASD.POSNR AS [No],\n"
                + "         MASD.MATNR AS [Material],\n"
                + "         MASM.ARKTX AS [Description],\n"
                + "         MASM.VRKME AS [U/M],\n"
                + "         SUM(MASD.KWMENG) AS REQ, \n"
                + "		 MAX(MASH.GRPNO) AS GrpSNo , \n"
                + "		 MAX(SEQN.ISSUENAME) AS SqcNo , \n"
                + "		 MAX(SEQN.ISSUENO) AS [ISSUENO] \n"
                + "      FROM\n"
                + "         MSSMASD MASD \n"
                + "         FULL JOIN\n"
                + "            MSSMASH MASH \n"
                + "            ON MASD.VBELN = MASH.VBELN \n"
                + "         FULL JOIN\n"
                + "            MSSMASM MASM \n"
                + "            ON MASD.MATNR = MASM.MATNR \n"
                + "         FULL JOIN\n"
                + "            MSSMATN MATN \n"
                + "            ON MASM.LGPBE = MATN.LGPBE \n"
                + "         FULL JOIN\n"
                + "            MSSSEQN SEQN \n"
                + "            ON MASD.ISSUENO = SEQN.ISSUENO \n"
                + "      WHERE\n"
                + "         MASH.POFG = 'MWC' \n"
                + "         AND MASH.GRPNO = '" + inv + "'\n"
                + "         AND MASH.VBELN = '" + sno + "'\n"
                + "      GROUP BY\n"
                + "         MASH.STYLE , MASH.LOT , MASH.COLOR , MASH.AMTFG , MASH.GRPNO , SEQN.ISSUENO , MASH.KUNNR , MASH.BWART , MASD.WERKS , MASH.NAME1 , MASH.NAME2 , MASD.LGORT , MASH.ADDR1 , MASH.ADDR2 , MASH.AUDAT , MASD.BWTAR , MASM.PRICE , MASH.VBELN , MASH.BSTKD , MASD.POSNR , MASD.MATNR , MASM.ARKTX , MASM.VRKME \n"
                + "   )\n"
                + "   QR1 \n"
                + "   LEFT JOIN\n"
                + "      (\n"
                + "         SELECT\n"
                + "            GRPNO,\n"
                + "            ISSUENO,\n"
                + "            VBELN,\n"
                + "            POSNR,\n"
                + "            MATNR,\n"
                + "            SUM(VALUE) AS VALUE \n"
                + "         FROM\n"
                + "            MSSPACD \n"
                + "         GROUP BY\n"
                + "            GRPNO,\n"
                + "            ISSUENO,\n"
                + "            VBELN,\n"
                + "            POSNR,\n"
                + "            MATNR \n"
                + "      )\n"
                + "      QR2 \n"
                + "      ON QR2.GRPNO = QR1.GrpSNo \n"
                + "      AND QR2.ISSUENO = QR1.ISSUENO \n"
                + "      AND QR2.VBELN = QR1.SaleNo \n"
                + "      AND QR1.No = QR2.POSNR \n"
                + "      AND QR1.Material = QR2.MATNR COLLATE Thai_CI_AS \n"
                + "ORDER BY\n"
                + "   SaleNo,\n"
                + "   SqcNo,\n"
                + "   No";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QCUSTOMS p = new QCUSTOMS();

                p.setDescE(result.getString("RUNNO"));//no
                p.setDescT(result.getString("SaleNo"));//sale no
                p.setMatGroup(result.getString("Material"));//code
                p.setDesc(result.getString("Description"));//name
                p.setPrice(result.getString("PRICE"));//price
                p.setComp(result.getString("VT").trim());//vt
                p.setRef(result.getString("REQ"));//qt
                p.setRefHS(result.getString("VALUE2"));//rqt
                p.setUnit(result.getString("U/M"));//unit
                double rqt = Double.parseDouble(result.getString("VALUE2"));
                double price = Double.parseDouble(result.getString("PRICE"));
                String amount = Double.toString(rqt * price);
                p.setHSC(amount);//amount

                QCUSTOMSList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QCUSTOMSList;

    }

    public List<QCUSTOMS> findTotalbyUM(String inv, String um) {

        List<QCUSTOMS> QCUSTOMSList = new ArrayList<QCUSTOMS>();

        String sql = "SELECT\n"
                + "   * , CASE WHEN VALUE IS NULL THEN 0.00 ELSE VALUE END AS VALUE2\n"
                + "FROM\n"
                + "   (\n"
                + "      SELECT\n"
                + "         SUBSTRING(MASH.GRPNO, 0 , 4) + RIGHT(MASH.GRPNO, 2) + RIGHT('000' + REPLACE(SUBSTRING(MASH.GRPNO, 4 , 2), '/', ''), 3) + RIGHT('000' + CONVERT(varchar(2), SEQN.ISSUENO), 3) AS RUNNO,\n"
                + "         MASD.BWTAR AS VT,\n"
                + "         MASM.PRICE AS PRICE,\n"
                + "         MASH.VBELN AS SaleNo,\n"
                + "         MASD.POSNR AS [No],\n"
                + "         MASD.MATNR AS [Material],\n"
                + "         MASM.ARKTX AS [Description],\n"
                + "         MASM.VRKME AS [U/M],\n"
                + "         SUM(MASD.KWMENG) AS REQ, \n"
                + "		 MAX(MASH.GRPNO) AS GrpSNo , \n"
                + "		 MAX(SEQN.ISSUENAME) AS SqcNo , \n"
                + "		 MAX(SEQN.ISSUENO) AS [ISSUENO] \n"
                + "      FROM\n"
                + "         MSSMASD MASD \n"
                + "         FULL JOIN\n"
                + "            MSSMASH MASH \n"
                + "            ON MASD.VBELN = MASH.VBELN \n"
                + "         FULL JOIN\n"
                + "            MSSMASM MASM \n"
                + "            ON MASD.MATNR = MASM.MATNR \n"
                + "         FULL JOIN\n"
                + "            MSSMATN MATN \n"
                + "            ON MASM.LGPBE = MATN.LGPBE \n"
                + "         FULL JOIN\n"
                + "            MSSSEQN SEQN \n"
                + "            ON MASD.ISSUENO = SEQN.ISSUENO \n"
                + "      WHERE\n"
                + "         MASH.POFG = 'MWC' \n"
                + "         AND MASH.GRPNO = '" + inv + "'\n"
                + "         AND MASM.VRKME like '" + um + "'+'%'\n"
                + "      GROUP BY\n"
                + "         MASH.STYLE , MASH.LOT , MASH.COLOR , MASH.AMTFG , MASH.GRPNO , SEQN.ISSUENO , MASH.KUNNR , MASH.BWART , MASD.WERKS , MASH.NAME1 , MASH.NAME2 , MASD.LGORT , MASH.ADDR1 , MASH.ADDR2 , MASH.AUDAT , MASD.BWTAR , MASM.PRICE , MASH.VBELN , MASH.BSTKD , MASD.POSNR , MASD.MATNR , MASM.ARKTX , MASM.VRKME \n"
                + "   )\n"
                + "   QR1 \n"
                + "   LEFT JOIN\n"
                + "      (\n"
                + "         SELECT\n"
                + "            GRPNO,\n"
                + "            ISSUENO,\n"
                + "            VBELN,\n"
                + "            POSNR,\n"
                + "            MATNR,\n"
                + "            SUM(VALUE) AS VALUE \n"
                + "         FROM\n"
                + "            MSSPACD \n"
                + "         GROUP BY\n"
                + "            GRPNO,\n"
                + "            ISSUENO,\n"
                + "            VBELN,\n"
                + "            POSNR,\n"
                + "            MATNR \n"
                + "      )\n"
                + "      QR2 \n"
                + "      ON QR2.GRPNO = QR1.GrpSNo \n"
                + "      AND QR2.ISSUENO = QR1.ISSUENO \n"
                + "      AND QR2.VBELN = QR1.SaleNo \n"
                + "      AND QR1.No = QR2.POSNR \n"
                + "      AND QR1.Material = QR2.MATNR COLLATE Thai_CI_AS \n"
                + "ORDER BY\n"
                + "   SaleNo,\n"
                + "   SqcNo,\n"
                + "   No";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                QCUSTOMS p = new QCUSTOMS();

                p.setDescE(result.getString("RUNNO"));//no
                p.setDescT(result.getString("SaleNo"));//sale no
                p.setMatGroup(result.getString("Material"));//code
                p.setDesc(result.getString("Description"));//name
                p.setPrice(result.getString("PRICE"));//price
                p.setComp(result.getString("VT").trim());//vt
                p.setRef(result.getString("REQ"));//qt
                p.setRefHS(result.getString("VALUE2"));//rqt
                p.setUnit(result.getString("U/M"));//unit
                double rqt = Double.parseDouble(result.getString("VALUE2"));
                double price = Double.parseDouble(result.getString("PRICE"));
                String amount = Double.toString(rqt * price);
                p.setHSC(amount);//amount

                QCUSTOMSList.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QCUSTOMSList;

    }

    public List<String> findNOList(String inv) {

        List<String> QCUSTOMSList = new ArrayList<String>();

        String sql = "SELECT\n"
                + "   * , CASE WHEN VALUE IS NULL THEN 0.00 ELSE VALUE END AS VALUE2\n"
                + "FROM\n"
                + "   (\n"
                + "      SELECT\n"
                + "         SUBSTRING(MASH.GRPNO, 0 , 4) + RIGHT(MASH.GRPNO, 2) + RIGHT('000' + REPLACE(SUBSTRING(MASH.GRPNO, 4 , 2), '/', ''), 3) + RIGHT('000' + CONVERT(varchar(2), SEQN.ISSUENO), 3) AS RUNNO,\n"
                + "         MASD.BWTAR AS VT,\n"
                + "         MASM.PRICE AS PRICE,\n"
                + "         MASH.VBELN AS SaleNo,\n"
                + "         MASD.POSNR AS [No],\n"
                + "         MASD.MATNR AS [Material],\n"
                + "         MASM.ARKTX AS [Description],\n"
                + "         MASM.VRKME AS [U/M],\n"
                + "         SUM(MASD.KWMENG) AS REQ, \n"
                + "		 MAX(MASH.GRPNO) AS GrpSNo , \n"
                + "		 MAX(SEQN.ISSUENAME) AS SqcNo , \n"
                + "		 MAX(SEQN.ISSUENO) AS [ISSUENO] \n"
                + "      FROM\n"
                + "         MSSMASD MASD \n"
                + "         FULL JOIN\n"
                + "            MSSMASH MASH \n"
                + "            ON MASD.VBELN = MASH.VBELN \n"
                + "         FULL JOIN\n"
                + "            MSSMASM MASM \n"
                + "            ON MASD.MATNR = MASM.MATNR \n"
                + "         FULL JOIN\n"
                + "            MSSMATN MATN \n"
                + "            ON MASM.LGPBE = MATN.LGPBE \n"
                + "         FULL JOIN\n"
                + "            MSSSEQN SEQN \n"
                + "            ON MASD.ISSUENO = SEQN.ISSUENO \n"
                + "      WHERE\n"
                + "         MASH.POFG = 'MWC' \n"
                + "         AND MASH.GRPNO = '" + inv + "'\n"
                + "      GROUP BY\n"
                + "         MASH.STYLE , MASH.LOT , MASH.COLOR , MASH.AMTFG , MASH.GRPNO , SEQN.ISSUENO , MASH.KUNNR , MASH.BWART , MASD.WERKS , MASH.NAME1 , MASH.NAME2 , MASD.LGORT , MASH.ADDR1 , MASH.ADDR2 , MASH.AUDAT , MASD.BWTAR , MASM.PRICE , MASH.VBELN , MASH.BSTKD , MASD.POSNR , MASD.MATNR , MASM.ARKTX , MASM.VRKME \n"
                + "   )\n"
                + "   QR1 \n"
                + "   LEFT JOIN\n"
                + "      (\n"
                + "         SELECT\n"
                + "            GRPNO,\n"
                + "            ISSUENO,\n"
                + "            VBELN,\n"
                + "            POSNR,\n"
                + "            MATNR,\n"
                + "            SUM(VALUE) AS VALUE \n"
                + "         FROM\n"
                + "            MSSPACD \n"
                + "         GROUP BY\n"
                + "            GRPNO,\n"
                + "            ISSUENO,\n"
                + "            VBELN,\n"
                + "            POSNR,\n"
                + "            MATNR \n"
                + "      )\n"
                + "      QR2 \n"
                + "      ON QR2.GRPNO = QR1.GrpSNo \n"
                + "      AND QR2.ISSUENO = QR1.ISSUENO \n"
                + "      AND QR2.VBELN = QR1.SaleNo \n"
                + "      AND QR1.No = QR2.POSNR \n"
                + "      AND QR1.Material = QR2.MATNR COLLATE Thai_CI_AS \n"
                + "ORDER BY\n"
                + "   SaleNo,\n"
                + "   SqcNo,\n"
                + "   No";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                if (!QCUSTOMSList.contains(result.getString("RUNNO") + "-" + result.getString("SaleNo"))) {
                    QCUSTOMSList.add(result.getString("RUNNO") + "-" + result.getString("SaleNo"));
                }

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return QCUSTOMSList;

    }

    public String check(String code) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String STRRETURN = null;
        try {
            String check = "Select * FROM [RMShipment].[dbo].[QCUSTOMS] where [QCUMAT] = '" + code + "'";
            ps = connect.prepareStatement(check);
            rs = ps.executeQuery();
            if (rs.next()) {
                STRRETURN = "f";
            } else {
                STRRETURN = "t";
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return STRRETURN;
    }

    public boolean add(String matGroup, String dog, String composit, String desc, String width, String unit, String price, String brand,
            String comp, String descT,
            String hsc, String taf, String refHS, String ref, String country,
            String unitC, String issDate, String nod, String expDate, String warnDate,
            String userid, String dept) {

        boolean result = false;

        String sql = "";

        if (dept.equals("bus")) {
            sql = "INSERT INTO QCUSTOMS "
                    + "([QCUCOM]\n"//not null
                    + "      ,[QCUMAT]\n"//not null
                    + "      ,[QCUDESCE]\n"
                    + "      ,[QCUSPEC]\n"
                    + "      ,[QCUNAME]\n"//not null
                    + "      ,[QCUBRAND]\n"
                    + "      ,[QCUWIDTH]\n"
                    + "      ,[QCUUM]\n"
                    + "      ,[QCUPRICE]\n"
                    + "      ,[QCUEDT]\n"
                    + "      ,[QCUCDT]\n"
                    + "      ,[QCUUSER])\n"
                    + "VALUES('TWC'"
                    + ",'" + matGroup + "'"
                    + ",'" + dog + "'"
                    + ",'" + composit + "'"
                    + ",'" + desc + "'"
                    + ",'" + brand + "'"
                    + ",'" + width + "'"
                    + ",'" + unit + "'"
                    + ",'" + price + "'"
                    + ",CURRENT_TIMESTAMP"
                    + ",CURRENT_TIMESTAMP"
                    + ",'" + userid + "') ";

        } else if (dept.equals("pur")) {
            sql = "INSERT INTO QCUSTOMS "
                    + "([QCUCOM]\n"//not null
                    + "      ,[QCUMAT]\n"//not null
                    + "      ,[QCUNAME]\n"//not null
                    + "      ,[QCUDESCT]\n"
                    + "      ,[QCUCOMP]\n"
                    + "      ,[QCUEDTPUR]\n"
                    + "      ,[QCUCDT]\n"
                    + "      ,[QCUUSERPUR])\n"
                    + "VALUES('TWC'"
                    + ",'" + matGroup + "'"
                    + ",'" + desc + "'"
                    + ",'" + descT + "'"
                    + ",'" + comp + "'"
                    + ",CURRENT_TIMESTAMP"
                    + ",CURRENT_TIMESTAMP"
                    + ",'" + userid + "') ";

        } else if (dept.equals("imp")) {
            sql = "INSERT INTO QCUSTOMS "
                    + "([QCUCOM]\n"//not null
                    + "      ,[QCUMAT]\n"//not null
                    + "      ,[QCUNAME]\n"//not null
                    + "      ,[QCUUMC]\n"
                    + "      ,[QCUCOUNTRY]\n"
                    + "      ,[QCUHSC]\n"
                    + "      ,[QCUTAF]\n"
                    + "      ,[QCUREFHS]\n"
                    + "      ,[QCUREF]\n"
                    + "      ,[QCUISSDT]\n"
                    + "      ,[QCUNODAY]\n"
                    + "      ,[QCUEXPDT]\n"
                    + "      ,[QCUWANDT]\n"
                    + "      ,[QCUEDTIMP]\n"
                    + "      ,[QCUCDT]\n"
                    + "      ,[QCUUSERIMP])\n"
                    + "VALUES('TWC'"
                    + ",'" + matGroup + "'"
                    + ",'" + desc + "'"
                    + ",'" + unitC + "'"
                    + ",'" + country + "'"
                    + ",'" + hsc + "'"
                    + ",'" + taf + "'"
                    + ",'" + refHS + "'"
                    + ",'" + ref + "'"
                    + ",'" + issDate + "'"
                    + ",'" + nod + "'"
                    + ",'" + expDate + "'"
                    + ",'" + warnDate + "'"
                    + ",CURRENT_TIMESTAMP"
                    + ",CURRENT_TIMESTAMP"
                    + ",'" + userid + "') ";

        }

//        System.out.println(sql);
//        System.out.println("***************************");
        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

    public QCUSTOMS findByCod(String matGroup) {

        QCUSTOMS p = new QCUSTOMS();

        String sql = "SELECT [QCUMAT]\n"
                + "      ,[QCUDESCE]\n"
                + "      ,[QCUSPEC]\n"
                + "      ,[QCUNAME]\n"
                + "      ,[QCUBRAND]\n"
                + "      ,[QCUWIDTH]\n"
                + "      ,[QCUUM]\n"
                + "      ,[QCUPRICE]\n"
                + "      ,[QCUDESCT]\n"
                + "      ,[QCUUMC]\n"
                + "      ,[QCUCOUNTRY]\n"
                + "      ,[QCUHSC]\n"
                + "      ,[QCUTAF]\n"
                + "      ,[QCUREFHS]\n"
                + "      ,[QCUCOMP]\n"
                + "      ,[QCUREF]\n"
                + "      ,FORMAT([QCUISSDT],'yyyy-MM-dd') AS [QCUISSDT]\n"
                + "      ,[QCUNODAY]\n"
                + "      ,FORMAT([QCUEXPDT],'yyyy-MM-dd') AS [QCUEXPDT]\n"
                + "      ,FORMAT([QCUWANDT],'yyyy-MM-dd') AS [QCUWANDT]\n"
                + "  FROM QCUSTOMS\n"
                + "  WHERE QCUMAT = '" + matGroup + "'";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setMatGroup(result.getString("QCUMAT"));
                p.setDescE(result.getString("QCUDESCE"));
                p.setSpec(result.getString("QCUSPEC"));
                p.setDesc(result.getString("QCUNAME"));
                p.setBrand(result.getString("QCUBRAND"));
                p.setWidth(result.getString("QCUWIDTH"));
                p.setUnit(result.getString("QCUUM"));
                p.setPrice(result.getString("QCUPRICE"));
                p.setDescT(result.getString("QCUDESCT"));
                p.setUnitC(result.getString("QCUUMC"));
                p.setCountry(result.getString("QCUCOUNTRY"));
                p.setHSC(result.getString("QCUHSC"));
                p.setTaf(result.getString("QCUTAF"));
                p.setRefHS(result.getString("QCUREFHS"));
                p.setComp(result.getString("QCUCOMP"));
                p.setRef(result.getString("QCUREF"));
                p.setIssdt(result.getString("QCUISSDT"));
                p.setNoday(result.getString("QCUNODAY"));
                p.setExpdt(result.getString("QCUEXPDT"));
                p.setWandt(result.getString("QCUWANDT"));

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public QCUSTOMS findByInv(String cod, String inv) {

        QCUSTOMS p = new QCUSTOMS();

        String sql = "SELECT QCUMAT, "
                + "        QCUNAME, "
                + "	   QCUDESCE, "
                + "	   QCUSPEC, "
                + "	   QCUWIDTH, "
                + "	   QCUUM, "
                + "	   isnull(QCUPRICE,0) as QCUPRICE, "
                + "        isnull(QCUCOUNTRY,'') as QCUCOUNTRY "
                + "FROM QCUSTOMS "
                + "WHERE QCUMAT = '" + cod + "' ";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                p.setMatGroup(result.getString("QCUMAT"));
                p.setDesc(result.getString("QCUNAME"));
                p.setSpec(result.getString("QCUSPEC"));
                p.setPrice(result.getString("QCUPRICE"));
                p.setDescE(result.getString("QCUDESCE"));
                p.setWidth(result.getString("QCUWIDTH"));
                p.setUnit(result.getString("QCUUM"));
                p.setCountry(result.getString("QCUCOUNTRY"));

                SWGLINEDao daonw = new SWGLINEDao();
                SWGLINEDao daoam = new SWGLINEDao();
                SWGLINEDao daoqt = new SWGLINEDao();

                double pr = Double.parseDouble(result.getString("QCUPRICE"));

                p.setHSC(daoqt.findQTTY(cod, inv));
                p.setTaf(daoam.findAMOUNT(cod, inv, pr));
                p.setRef(daonw.findNW(cod, inv));

            }

            if (p.getTaf() == null) {
                p.setMatGroup(cod);
                p.setDesc("***");
                p.setSpec("***");
                p.setPrice("***");
                p.setDescE("***");
                p.setWidth("***");
                p.setUnit("***");
                p.setHSC("0");
                p.setTaf("0");
                p.setRef("0");
            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return p;

    }

    public boolean edit(String matGroup, String dog, String composit, String desc, String width, String unit, String price, String brand,
            String comp, String descT,
            String hsc, String taf, String refHS, String ref, String country,
            String unitC, String issDate, String nod, String expDate, String warnDate,
            String userid, String dept) {

        boolean result = false;

        String sql = "";

        if (dept.trim().equals("bus")) {
            sql = "UPDATE QCUSTOMS \n"
                    + "SET [QCUDESCE] = '" + dog + "'\n"
                    + "      ,[QCUSPEC] = '" + composit + "'\n"
                    + "      ,[QCUBRAND] = '" + brand + "'\n"
                    + "      ,[QCUWIDTH] = '" + width + "'\n"
                    + "      ,[QCUUM] = '" + unit + "'\n"
                    + "      ,[QCUPRICE] = '" + price + "'\n"
                    + ", QCUEDT = CURRENT_TIMESTAMP, QCUUSER = '" + userid + "' \n"
                    + "WHERE QCUMAT = '" + matGroup + "' ";
        } else if (dept.trim().equals("pur")) {
//            String PRICE = "";
//            if (userid.trim().equals("91551")) {
//                PRICE = "      ,[QCUPRICE] = '" + price + "'\n";
//            }
            sql = "UPDATE QCUSTOMS \n"
                    + "SET [QCUDESCT] = '" + descT + "'\n"
                    + "      ,[QCUCOMP] = '" + comp + "'\n"
                    //                    + PRICE
                    + ", QCUEDTPUR = CURRENT_TIMESTAMP, QCUUSERPUR = '" + userid + "' \n"
                    + "WHERE QCUMAT = '" + matGroup + "' ";
        } else if (dept.trim().equals("imp")) {
            sql = "UPDATE QCUSTOMS \n"
                    + "SET [QCUUMC] = '" + unitC + "'\n"
                    + "      ,[QCUCOUNTRY] = '" + country + "'\n"
                    + "      ,[QCUHSC] = '" + hsc + "'\n"
                    + "      ,[QCUTAF] = '" + taf + "'\n"
                    + "      ,[QCUREFHS] = '" + refHS + "'\n"
                    + "      ,[QCUREF] = '" + ref + "'\n"
                    + "      ,[QCUISSDT] = '" + issDate + "'\n"
                    + "      ,[QCUNODAY] = '" + nod + "'\n"
                    + "      ,[QCUEXPDT] = '" + expDate + "'\n"
                    + "      ,[QCUWANDT] = '" + warnDate + "'\n"
                    + ", QCUEDTIMP = CURRENT_TIMESTAMP, QCUUSERIMP = '" + userid + "' \n"
                    + "WHERE QCUMAT = '" + matGroup + "' ";
        }
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            int record = ps.executeUpdate();

            if (record >= 1) {

                result = true;

            } else {

                result = false;

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }
        return result;

    }

    public void delete(String cod) {

        String sql = "DELETE FROM QCUSTOMS WHERE QCUMAT = ?";

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ps.setString(1, cod);

            ps.executeUpdate();

        } catch (Exception e) {

            e.printStackTrace();

        }

    }
}
