/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.dao;

import com.twc.wms.database.database;
import com.twc.wms.entity.MSS217;
import com.twc.wms.entity.UserAuth;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author nutthawoot.noo
 */
public class MSS217Dao extends database {

    public List<MSS217> findContainerList(String cus, String[] con) {

        List<MSS217> MSS217List = new ArrayList<MSS217>();

        String CON = "";
        if (con != null) {
            for (int i = 0; i < con.length; i++) {
                if (i == 0) {
                    CON += "'" + con[i] + "'";
                } else {
                    CON += ",'" + con[i] + "'";
                }
            }
        } else {
            CON = "''";
        }

        String sql = "SELECT distinct [SWTRUCK]\n"
                + "      ,[SWGRPST]\n"
                + "	 ,(select top 1 [POFG] COLLATE Thai_CI_AS from [MSSMASH] where [GRPNO] = [SWGRPST] COLLATE Thai_CI_AS) as [CUST]\n"
                + "FROM [RMShipment].[dbo].[SWGLINEUP]\n"
                + "WHERE [SWTRUCK] is not null\n"
                + "AND (select top 1 [POFG] COLLATE Thai_CI_AS from [MSSMASH] where [GRPNO] = [SWGRPST] COLLATE Thai_CI_AS) = '" + cus + "'\n"
                + "AND [SWTRUCK] in (" + CON + ")\n"
                + "ORDER BY [SWTRUCK],[SWGRPST]\n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSS217 p = new MSS217();

                p.setCon(result.getString("SWTRUCK"));
                p.setInv(result.getString("SWGRPST"));
                p.setCus(result.getString("CUST"));

                MSS217List.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return MSS217List;

    }

    public List<MSS217> findContainerDet(String cus, String con, String inv) {

        List<MSS217> MSS217List = new ArrayList<MSS217>();

        String sql = "DECLARE @CUSTOMER nvarchar(20) = '" + cus + "'\n"
                + "DECLARE @CONTAINER nvarchar(20) = '" + con + "'\n"
                + "DECLARE @INVNO nvarchar(20) = '" + inv + "'\n"
                + "\n"
                + "SELECT [CONTAINER],[INVNO]\n"
                + "      ,count(*) as [PACK]\n"
                + "	  ,format(sum([GW]),'#,#0.00') as [GW]\n"
                + "	  ,format(sum([CUM]),'#,#0.000') as [CUM]\n"
                + "FROM(\n"
                + "SELECT DISTINCT LINEUP.SWTRUCK as [CONTAINER], A.[GRP/NO] as [INVNO], A.[GW], ((A.[WD]/100)*(A.[LEN]/100)*(A.[HG]/100)) AS [CUM]\n"
                + "FROM ( SELECT STMT.[GRP/NO], STMT.CUSTOMER_DESC, STMT.[C/NO], \n"
                + "SUM(STMT.NW) AS NW, MAX(STMT.GW) AS GW, MAX(STMT.WD) AS WD, \n"
                + "MAX(STMT.LEN) AS LEN, MAX(STMT.HG) AS HG \n"
                + "FROM ( SELECT MASH.POFG AS [CUSTOMER], MASH.NAME1 AS [CUSTOMER_DESC], \n"
                + "QR1.*, LINE.SWDMLWD AS [WD], LINE.SWDMLEN AS [LEN], LINE.SWDMHG AS [HG], \n"
                + "LINE.SWDMUNIT AS [UNIT] \n"
                + "FROM ( SELECT DISTINCT POFG, GRPNO, NAME1 \n"
                + "FROM MSSMASH ) MASH INNER JOIN ( SELECT A.SWGRPST AS [GRP/NO], A.SWCARNO AS [C/NO], \n"
                + "SUBSTRING(A.SWITEM, 0, 9) AS [MAT/NO], SUBSTRING(A.SWITEM, 9, 12) AS [COLOR], \n"
                + "ARKTX AS [DESC], A.SWLINE AS [LINE], A.SWITUNIT AS [U/M], A.SWQUANTY AS [PICKUP], \n"
                + "A.SWNWEIGHT AS [NW], B.GW AS [GW] FROM SWGLINE A LEFT JOIN MSSMASM ON A.SWITEM = MATNR COLLATE Thai_CI_AS \n"
                + "LEFT JOIN ( SELECT SWIVNO, SWCARNO, SUM(SWGWEIGHT) AS GW FROM SWGLINE \n"
                + "GROUP BY SWIVNO, SWCARNO ) B ON A.SWIVNO = B.SWIVNO AND A.SWCARNO = B.SWCARNO \n"
                + "LEFT JOIN SWGHEAD C ON A.SWGRPST = C.SWGRPST AND A.SWCARNO = C.SWCARNO \n"
                + "WHERE A.SWGRPST = @INVNO AND A.SWLINE != 0 ) AS QR1 ON QR1.[GRP/NO] = MASH.GRPNO COLLATE Thai_CI_AS \n"
                + "AND MASH.POFG = @CUSTOMER INNER JOIN SWGLINE LINE ON LINE.SWCARNO = QR1.[C/NO] AND LINE.SWLINE = 0 \n"
                + "AND LINE.SWIVNO = @INVNO ) STMT \n"
                + "GROUP BY STMT.[GRP/NO], STMT.CUSTOMER_DESC, STMT.[C/NO] ) A \n"
                + "LEFT JOIN SWGLINEUP LINEUP ON A.[GRP/NO] = LINEUP.SWGRPST AND A.[C/NO] = LINEUP.SWCARNO\n"
                + ")TMP\n"
                + "WHERE [CONTAINER] = @CONTAINER\n"
                + "GROUP BY [CONTAINER],[INVNO]\n"
                + "ORDER BY [CONTAINER],[INVNO]\n";
//        System.out.println(sql);

        try {

            PreparedStatement ps = connect.prepareStatement(sql);

            ResultSet result = ps.executeQuery();

            while (result.next()) {

                MSS217 p = new MSS217();

                p.setCon(result.getString("CONTAINER"));
                p.setInv(result.getString("INVNO"));
                p.setPck(result.getString("PACK"));
                p.setGw(result.getString("GW"));
                p.setCum(result.getString("CUM"));

                MSS217List.add(p);

            }

            connect.close();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return MSS217List;

    }
}
