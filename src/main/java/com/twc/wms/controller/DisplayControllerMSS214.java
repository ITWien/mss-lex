/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.MSSSIHEADDao;
import com.twc.wms.dao.SWGLINEDao;
import com.twc.wms.entity.MSSSIHEAD;
import com.twc.wms.entity.SWGLINE;
import java.util.List;

/**
 *
 * @author wien
 */
public class DisplayControllerMSS214 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayMSS214.jsp";

    public DisplayControllerMSS214() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "MSS214");
        String forward = "";

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Invoice Report. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        List<MSSSIHEAD> pList = new MSSSIHEADDao().findAll();

        List<SWGLINE> invList = new SWGLINEDao().findAllInv();
        request.setAttribute("invList", invList);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("MSSSIHEADList", pList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
