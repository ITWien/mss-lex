/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MSSSIDETAILDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.MSSSIHEADDao;
import com.twc.wms.entity.MSSSIDETAIL;
import com.twc.wms.entity.MSSSIHEAD;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletOutputStream;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperRunManager;

/**
 *
 * @author wien
 */
public class PrintControllerMSS214 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public PrintControllerMSS214() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ccy = request.getParameter("ccy");
        String loadDate = request.getParameter("loadDate");
        String remark = request.getParameter("remark");
        String qno = request.getParameter("qnoH");

        new MSSSIHEADDao().edit(qno, loadDate, ccy, remark);

        String[] inv = request.getParameterValues("inv");
        String[] dept = request.getParameterValues("dept");
        String[] ins = request.getParameterValues("ins");
        String[] fre = request.getParameterValues("fre");
        String[] fob = request.getParameterValues("fob");

        if (inv != null) {
            for (int i = 0; i < inv.length; i++) {
                if (ins[i].trim().equals("")) {
                    ins[i] = "0";
                }
                if (fre[i].trim().equals("")) {
                    fre[i] = "0";
                }
                if (fob[i].trim().equals("")) {
                    fob[i] = "0";
                }
                new MSSSIDETAILDao().edit(qno, inv[i], dept[i].replace(",", ""), ins[i].replace(",", ""), fre[i].replace(",", ""), fob[i].replace(",", ""));
            }
        }

        try {

            ResultSet result = new MSSSIDETAILDao().findForPrintMSS214(qno);

            ServletOutputStream servletOutputStream = response.getOutputStream();
            File reportFile = new File(getServletConfig().getServletContext()
                    .getRealPath("/resources/jasper/MSS214.jasper"));
            byte[] bytes;

            HashMap<String, Object> map = new HashMap<String, Object>();
//                map.put("WH", wh);

            try {
                bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), map, new JRResultSetDataSource(result));
                response.setContentType("application/pdf");
                response.setContentLength(bytes.length);

                servletOutputStream.write(bytes, 0, bytes.length);
                servletOutputStream.flush();
                servletOutputStream.close();

            } catch (JRException e) {
                StringWriter stringWriter = new StringWriter();
                PrintWriter printWriter = new PrintWriter(stringWriter);
                e.printStackTrace(printWriter);
                response.setContentType("text/plain");
                response.getOutputStream().print(stringWriter.toString());

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        response.setHeader("Refresh", "0;/TABLE2/MSS214/edit?qno=" + qno);

    }

}
