/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MSSSIDETAILDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.MSSSIHEADDao;
import com.twc.wms.entity.MSSSIDETAIL;
import com.twc.wms.entity.MSSSIHEAD;
import java.util.List;

/**
 *
 * @author wien
 */
public class AddDetControllerMSS214 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public AddDetControllerMSS214() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String qno = request.getParameter("qno");
        String inv = request.getParameter("inv");
        String dept = request.getParameter("dept");
        String cif = request.getParameter("cif");
        String ins = request.getParameter("ins");
        String fre = request.getParameter("fre");
        String fob = request.getParameter("fob");
        String ctn = request.getParameter("ctn");
        String meter = request.getParameter("meter");
        String pc = request.getParameter("pc");
        String set = request.getParameter("set");
        String cone = request.getParameter("cone");
        String kgs = request.getParameter("kgs");
        String nw = request.getParameter("nw");
        String gw = request.getParameter("gw");
        String m3 = request.getParameter("m3");
        String uid = request.getParameter("uid");

        new MSSSIDETAILDao().addDet(qno, inv, dept, cif.trim(), ins.trim(), fre.trim(), fob.trim(), ctn.trim(), meter.trim(), pc.trim(), set.trim(), cone.trim(), kgs.trim(), nw.trim(), gw.trim(), m3.trim(), uid);

        response.setHeader("Refresh", "0;/TABLE2/MSS214/edit?qno=" + qno);
    }

}
