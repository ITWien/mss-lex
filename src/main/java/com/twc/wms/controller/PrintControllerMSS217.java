/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.twc.wms.dao.MSS217Dao;
import com.twc.wms.dao.QCUSTOMSDao;
import com.twc.wms.dao.SWGLINEDao;
import com.twc.wms.entity.MSS217;
import com.twc.wms.entity.MSSMASM;
import com.twc.wms.entity.MSSMASH;
import com.twc.wms.entity.QCUSTOMS;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.sl.usermodel.StrokeStyle.LineDash;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author wien
 */
public class PrintControllerMSS217 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/printMSS217.jsp";
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;
    DecimalFormat formatDou0 = new DecimalFormat("#,###,###,##0");
    DecimalFormat formatDou2 = new DecimalFormat("#,###,###,##0.00");
    DecimalFormat formatDou3 = new DecimalFormat("#,###,###,##0.000");

    public PrintControllerMSS217() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "MSS217");
        String forward = "";

        SWGLINEDao dao = new SWGLINEDao();
        List<MSSMASH> cusList = dao.findCusList();

        SWGLINEDao dao1 = new SWGLINEDao();
        List<MSSMASM> invList = dao1.findInvList();

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Report Container List");
                request.setAttribute("cusList", cusList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String customer = request.getParameter("customer");
        String[] containerInput = request.getParameterValues("containerInput");

        //        ***************EXCEL*************************************
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Report Container List");

        Map<String, Object[]> data = new TreeMap<String, Object[]>();

        int ni = 100001;
        data.put(Integer.toString(ni++), new Object[]{null, null, "Report Container List" + "<TITLE>"});
        data.put(Integer.toString(ni++), new Object[]{""});
        data.put(Integer.toString(ni++), new Object[]{
            "CONTAINER" + "<HEAD>",
            "INV NO." + "<HEAD>",
            "PACK" + "<HEAD>",
            "GW." + "<HEAD>",
            "CUM" + "<HEAD>"
        });

        List<MSS217> MSS217List = new MSS217Dao().findContainerList(customer, containerInput);

        String con = "first";
        int pack = 0;
        Double gw = 0.00;
        Double cum = 0.000;

        for (int i = 0; i < MSS217List.size(); i++) {
            List<MSS217> MSS217Det = new MSS217Dao().findContainerDet(MSS217List.get(i).getCus(), MSS217List.get(i).getCon(), MSS217List.get(i).getInv());

            if (con.equals("first")) {
                con = MSS217Det.get(0).getCon();
            } else if (!con.equals(MSS217Det.get(0).getCon())) {
                data.put(Integer.toString(ni++), new Object[]{"<BLANK>", "<BLANK>", formatDou0.format(pack) + "<DET_NUM>", formatDou2.format(gw) + "<DET_NUM>", formatDou3.format(cum) + "<DET_NUM>"});
                data.put(Integer.toString(ni++), new Object[]{"<BLANK>", "<BLANK>", "<BLANK>", "<BLANK>", "<BLANK>"});
                con = MSS217Det.get(0).getCon();
                pack = 0;
                gw = 0.00;
                cum = 0.000;
            } else {
                con = "";
            }

            pack += Integer.parseInt(MSS217Det.get(0).getPck().replace(",", ""));
            gw += Double.parseDouble(MSS217Det.get(0).getGw().replace(",", ""));
            cum += Double.parseDouble(MSS217Det.get(0).getCum().replace(",", ""));

            data.put(Integer.toString(ni++), new Object[]{
                con + "<DET_TEXT>",
                MSS217Det.get(0).getInv() + "<DET_TEXT>",
                MSS217Det.get(0).getPck() + "<DET_NUM>",
                MSS217Det.get(0).getGw() + "<DET_NUM>",
                MSS217Det.get(0).getCum() + "<DET_NUM>"
            });

            con = MSS217Det.get(0).getCon();

            if (i == MSS217List.size() - 1) {
                data.put(Integer.toString(ni++), new Object[]{"<BLANK>", "<BLANK>", formatDou0.format(pack) + "<DET_NUM>", formatDou2.format(gw) + "<DET_NUM>", formatDou3.format(cum) + "<DET_NUM>"});
            }

        }

        data.put(Integer.toString(ni++), new Object[]{""});

        //Set Column Width
        sheet.setColumnWidth(0, 4000);
        sheet.setColumnWidth(1, 4000);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 4000);
        sheet.setColumnWidth(4, 4000);
//
        XSSFFont my_font = workbook.createFont();
//        my_font.setBold(true);
        my_font.setFontHeight(12);

        CellStyle style_title = workbook.createCellStyle();
        style_title.setAlignment(HorizontalAlignment.CENTER);
//        style_title.setBorderBottom(BorderStyle.THICK);
//        style_title.setBorderTop(BorderStyle.THICK);
//        style_title.setBorderRight(BorderStyle.THIN);
//        style_title.setBorderLeft(BorderStyle.THIN);

        CellStyle style_head = workbook.createCellStyle();
        style_head.setAlignment(HorizontalAlignment.CENTER);
        style_head.setBorderBottom(BorderStyle.THICK);
        style_head.setBorderTop(BorderStyle.THICK);
        style_head.setBorderRight(BorderStyle.THICK);
        style_head.setBorderLeft(BorderStyle.THICK);

        CellStyle style_det_text = workbook.createCellStyle();
        style_det_text.setAlignment(HorizontalAlignment.LEFT);
        style_det_text.setBorderBottom(BorderStyle.THIN);
        style_det_text.setBorderTop(BorderStyle.THIN);
        style_det_text.setBorderRight(BorderStyle.THICK);
        style_det_text.setBorderLeft(BorderStyle.THICK);

        CellStyle style_det_num = workbook.createCellStyle();
        style_det_num.setAlignment(HorizontalAlignment.RIGHT);
        style_det_num.setBorderBottom(BorderStyle.THIN);
        style_det_num.setBorderTop(BorderStyle.THIN);
        style_det_num.setBorderRight(BorderStyle.THICK);
        style_det_num.setBorderLeft(BorderStyle.THICK);

        CellStyle style_blank = workbook.createCellStyle();
        style_blank.setAlignment(HorizontalAlignment.CENTER);
        style_blank.setBorderBottom(BorderStyle.THIN);
        style_blank.setBorderTop(BorderStyle.THIN);
        style_blank.setBorderRight(BorderStyle.THICK);
        style_blank.setBorderLeft(BorderStyle.THICK);
//
        // Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row
                Cell cell = row.createCell(cellnum++);
                cell.setCellValue((String) obj);

                if (cell.getStringCellValue().contains("<TITLE>")) {
                    cell.setCellStyle(style_title);
                    cell.setCellValue((String) cell.getStringCellValue().replace("<TITLE>", ""));

                } else if (cell.getStringCellValue().contains("<HEAD>")) {
                    cell.setCellStyle(style_head);
                    cell.setCellValue((String) cell.getStringCellValue().replace("<HEAD>", ""));

                } else if (cell.getStringCellValue().contains("<DET_TEXT>")) {
                    cell.setCellStyle(style_det_text);
                    cell.setCellValue((String) cell.getStringCellValue().replace("<DET_TEXT>", ""));

                } else if (cell.getStringCellValue().contains("<DET_NUM>")) {
                    cell.setCellStyle(style_det_num);
                    cell.setCellValue((String) cell.getStringCellValue().replace("<DET_NUM>", ""));

                } else if (cell.getStringCellValue().contains("<BLANK>")) {
                    cell.setCellStyle(style_blank);
                    cell.setCellValue((String) cell.getStringCellValue().replace("<BLANK>", ""));

                }

            }
        }

        try {
            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "REPORT_CONTAINER_LIST.xlsx";

            FileOutputStream outputStream = new FileOutputStream(pathEX);
            workbook.write(outputStream);
            workbook.close();

//            ******************************************************
            File fileEX = new File(pathEX);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(pathEX);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) fileEX.length());
            String fileNameEX = (new File(pathEX)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameEX + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(fileEX));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        response.setHeader("Refresh", "0;/TABLE2/MSS217/print");
    }
}
