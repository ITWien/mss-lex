/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import java.io.*;
import java.util.Iterator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;

/**
 *
 * @author wien
 */
public class TEST extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/page.jsp";

    public TEST() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String forward = "";

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "THAI CUSTOMS Information. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
////        String gg = "xxx";
//        String gg = request.getParameter("GG");
//        //to get the content type information from JSP Request Header
//        String contentType = request.getContentType();
//        //here we are checking the content type is not equal to Null and
//
//        if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
//            DataInputStream in = new DataInputStream(request.getInputStream());
//            //we are taking the length of Content type data
//            int formDataLength = request.getContentLength();
//            byte dataBytes[] = new byte[formDataLength];
//            int byteRead = 0;
//            int totalBytesRead = 0;
//            //this loop converting the uploaded file into byte code
//            while (totalBytesRead < formDataLength) {
//                byteRead = in.read(dataBytes, totalBytesRead,
//                        formDataLength);
//                totalBytesRead += byteRead;
//            }
//            String file = new String(dataBytes);
//            //for saving the file name
//            String saveFile = file
//                    .substring(file.indexOf("filename=\"") + 10);
//            saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
//            saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1,
//                    saveFile.indexOf("\""));
//            int lastIndex = contentType.lastIndexOf("=");
//            String boundary = contentType.substring(lastIndex + 1,
//                    contentType.length());
//            int pos;
//            //extracting the index of file 
//            pos = file.indexOf("filename=\"");
//            pos = file.indexOf("\n", pos) + 1;
//            pos = file.indexOf("\n", pos) + 1;
//            pos = file.indexOf("\n", pos) + 1;
//            int boundaryLocation = file.indexOf(boundary, pos) - 4;
//            int startPos = ((file.substring(0, pos)).getBytes()).length;
//            int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;
//            // creating a new file with the same name and writing the content in new file
//
//            ServletContext servletContext = getServletContext();
//            String savePath = servletContext.getRealPath("\\upload\\" + saveFile);
//
//            FileOutputStream fileOut = new FileOutputStream(savePath);
//            fileOut.write(dataBytes, startPos, (endPos - startPos));
//            fileOut.flush();
//            fileOut.close();
//        }
//
//        String forward = "";
//
//        try {
//            String action = request.getParameter("action");
//            if (action == null) {
//                forward = PAGE_VIEW;
//                if (gg != null) {
//                    request.setAttribute("msg", "Success ! " + gg);
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        RequestDispatcher view = request.getRequestDispatcher(forward);
//        view.forward(request, response);

        ServletContext servletContext = request.getSession().getServletContext();
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        String txtResponse = "0";

        if (isMultipart) {

            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            try {
                List<FileItem> multiparts = upload.parseRequest(request);

                for (FileItem item : multiparts) {
                    if (!item.isFormField()) {
                        System.out.println(item.getFieldName() + " : " + item.getString());
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(txtResponse);

    }
}
