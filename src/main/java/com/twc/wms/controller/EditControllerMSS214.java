/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MSSSIDETAILDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.MSSSIHEADDao;
import com.twc.wms.entity.MSSSIDETAIL;
import com.twc.wms.entity.MSSSIHEAD;
import java.util.List;

/**
 *
 * @author wien
 */
public class EditControllerMSS214 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editMSS214.jsp";

    public EditControllerMSS214() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "MSS214");
        String forward = "";

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Summary Invoice Report. Process");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********
        String qno = request.getParameter("qno");

        MSSSIHEAD h = new MSSSIHEADDao().findByQno(qno);
        request.setAttribute("head", h);

        List<MSSSIDETAIL> d = new MSSSIDETAILDao().findByQno(qno);
        request.setAttribute("detail", d);

        request.setAttribute("qno", qno);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String ccy = request.getParameter("ccy");
        String loadDate = request.getParameter("loadDate");
        String remark = request.getParameter("remark");
        String qno = request.getParameter("qnoH");

        new MSSSIHEADDao().edit(qno, loadDate, ccy, remark);

        String[] inv = request.getParameterValues("inv");
        String[] dept = request.getParameterValues("dept");
        String[] ins = request.getParameterValues("ins");
        String[] fre = request.getParameterValues("fre");
        String[] fob = request.getParameterValues("fob");

        if (inv != null) {
            for (int i = 0; i < inv.length; i++) {
                if (ins[i].trim().equals("")) {
                    ins[i] = "0";
                }
                if (fre[i].trim().equals("")) {
                    fre[i] = "0";
                }
                if (fob[i].trim().equals("")) {
                    fob[i] = "0";
                }
                new MSSSIDETAILDao().edit(qno, inv[i], dept[i].replace(",", ""), ins[i].replace(",", ""), fre[i].replace(",", ""), fob[i].replace(",", ""));
            }
        }

        response.setHeader("Refresh", "0;/TABLE2/MSS214/edit?qno=" + qno);

    }

}
