/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QCUSTOMSDao;
import com.twc.wms.dao.MSSMASMDao;
import com.twc.wms.dao.UserDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.MSSMASM;
import com.twc.wms.entity.QCUSTOMS;
import com.twc.wms.entity.UserAuth;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class CreateControllerMSS003 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/createMSS003.jsp";

    public CreateControllerMSS003() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "MSS003/C");
        String forward = "";
        String matGroup = request.getParameter("matGroup");
        String comp = request.getParameter("comp");
        String descE = request.getParameter("descE");
        String descT = request.getParameter("descT");
        String width = request.getParameter("width");
        String hsc = request.getParameter("hsc");
        String taf = request.getParameter("taf");
        String country = request.getParameter("country");
        String ref = request.getParameter("ref");
        String expDate = request.getParameter("expDate");
        String refHS = request.getParameter("refHS");

        String dept = request.getParameter("dept");

        String uidx = request.getParameter("uidx");
        request.setAttribute("uidx", uidx);

        if (dept != null) {
            if (dept.equals("bus")) {
                request.setAttribute("busShow", "");
                request.setAttribute("purShow", "display: none;");
                request.setAttribute("impShow", "display: none;");
            } else if (dept.equals("pur")) {
                request.setAttribute("busShow", "display: none;");
                request.setAttribute("purShow", "");
                request.setAttribute("impShow", "display: none;");
            } else if (dept.equals("imp")) {
                request.setAttribute("busShow", "display: none;");
                request.setAttribute("purShow", "display: none;");
                request.setAttribute("impShow", "");
            }
        }

        request.setAttribute("dept", dept);

        String dog = request.getParameter("dog");
        String composit = request.getParameter("composit");
        String brand = request.getParameter("brand");
        String unitC = request.getParameter("unitC");
        String issDate = request.getParameter("issDate");
        String nod = request.getParameter("nod");
        String warnDate = request.getParameter("warnDate");

        MSSMASMDao dao5 = new MSSMASMDao();
        MSSMASMDao dao6 = new MSSMASMDao();
        if (matGroup == null) {
            matGroup = "";
        }
        MSSMASM cus = dao5.findByCod(matGroup);
        MSSMASM cus2 = dao6.findByCodFromSAPMAS(matGroup);

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "THAI CUSTOMS Information. Entry");
                request.setAttribute("matGroup", matGroup);
                request.setAttribute("price", cus.getPrice());
                request.setAttribute("composit", cus.getSpec());
                request.setAttribute("comp", comp);
                request.setAttribute("descE", descE);
                request.setAttribute("descT", descT);
                request.setAttribute("width", width);
                request.setAttribute("hsc", hsc);
                request.setAttribute("taf", taf);
                request.setAttribute("country", country);
                request.setAttribute("ref", ref);
                request.setAttribute("expDate", expDate);
                request.setAttribute("refHS", refHS);

                request.setAttribute("dog", dog);
//                request.setAttribute("composit", composit);
                request.setAttribute("brand", brand);
                request.setAttribute("unitC", unitC);
                request.setAttribute("issDate", issDate);
                request.setAttribute("nod", nod);
                request.setAttribute("warnDate", warnDate);

                if (cus.getDesc() == null) {
                    request.setAttribute("desc", cus2.getDesc());
                    request.setAttribute("unit", cus2.getUnit());
                } else {
                    request.setAttribute("desc", cus.getDesc());
                    request.setAttribute("unit", cus.getUnit());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        MSSMASMDao dao = new MSSMASMDao();
        List<MSSMASM> QMList = dao.selectMatGroup();
        List<MSSMASM> matGroupList = new ArrayList<MSSMASM>();
        for (int i = 0; i < QMList.size(); i++) {
            if (matGroupList.isEmpty()) {
                matGroupList.add(QMList.get(i));
            } else {
                if (!matGroupList.get(matGroupList.size() - 1).getMatGroup().equals(QMList.get(i).getMatGroup())) {
                    matGroupList.add(QMList.get(i));
                }
            }
        }

        MSSMASMDao dao1 = new MSSMASMDao();
        List<MSSMASM> descList = dao1.selectDesc();

        MSSMASMDao dao2 = new MSSMASMDao();
        List<MSSMASM> priceList = dao2.selectPrice();

        MSSMASMDao dao3 = new MSSMASMDao();
        List<MSSMASM> unitList = dao3.selectUnit();

        MSSMASMDao dao4 = new MSSMASMDao();
        List<MSSMASM> specList = dao4.selectSpec();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("matGroupList", matGroupList);
        request.setAttribute("descList", descList);
        request.setAttribute("priceList", priceList);
        request.setAttribute("unitList", unitList);
        request.setAttribute("specList", specList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String matGroup = request.getParameter("matGroup");
        String dog = request.getParameter("dog");
        String composit = request.getParameter("composit");
        String desc = request.getParameter("desc");
        String width = request.getParameter("width");
        String unit = request.getParameter("unit");
        String price = request.getParameter("price");
        String brand = request.getParameter("brand");

        String comp = request.getParameter("comp");
        String descT = request.getParameter("descT");

        String hsc = request.getParameter("hsc");
        String taf = request.getParameter("taf");
        String refHS = request.getParameter("refHS");
        String ref = request.getParameter("ref");
        String country = request.getParameter("country");
        String unitC = request.getParameter("unitC");
        String issDate = request.getParameter("issDate");
        String nod = request.getParameter("nod");
        String expDate = request.getParameter("expDate");
        String warnDate = request.getParameter("warnDate");

        if (price.equals("")) {
            price = "0";
        }
        if (ref.equals("")) {
            ref = "0";
        }
        if (refHS.equals("")) {
            refHS = "0";
        }
        if (nod.equals("")) {
            nod = "0";
        }

        request.setAttribute("PROGRAMNAME", "MSS003/C");
        request.setAttribute("PROGRAMDESC", "THAI CUSTOMS Information. Entry");

        String userid = request.getParameter("userid");
        String dept = request.getParameter("dept");

        if (dept != null) {
            if (dept.equals("bus")) {
                request.setAttribute("busShow", "");
                request.setAttribute("purShow", "display: none;");
                request.setAttribute("impShow", "display: none;");
            } else if (dept.equals("pur")) {
                request.setAttribute("busShow", "display: none;");
                request.setAttribute("purShow", "");
                request.setAttribute("impShow", "display: none;");
            } else if (dept.equals("imp")) {
                request.setAttribute("busShow", "display: none;");
                request.setAttribute("purShow", "display: none;");
                request.setAttribute("impShow", "");
            }
        }

        request.setAttribute("dept", dept);
//
//        System.out.println(matGroup);
//        System.out.println(dog);
//        System.out.println(composit);
//        System.out.println(desc);
//        System.out.println(width);
//        System.out.println(unit);
//        System.out.println(price);
//        System.out.println(brand);
//        System.out.println("--------------------");
//        System.out.println(comp);
//        System.out.println(descT);
//        System.out.println("--------------------");
//        System.out.println(hsc);
//        System.out.println(taf);
//        System.out.println(refHS);
//        System.out.println(ref);
//        System.out.println(country);
//        System.out.println(unitC);
//        System.out.println(issDate);
//        System.out.println(nod);
//        System.out.println(expDate);
//        System.out.println(warnDate);
//        System.out.println("--------------------");
//        System.out.println(userid);
//        System.out.println(dept);
//        System.out.println("********************");
//
        String sendMessage = "";
        String forward = "";

        String uidx = request.getParameter("uidx");
        request.setAttribute("uidx", uidx);

        if (new QCUSTOMSDao().check(matGroup).equals("f")) {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show = function () {\n"
                    + "                $('#myModal').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show, 0);\n"
                    + "\n"
                    + "        </script>";
            forward = PAGE_VIEW;
            request.setAttribute("sendMessage", sendMessage);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        } else {
            if (new QCUSTOMSDao().add(matGroup, dog, composit, desc, width, unit, price, brand, comp, descT, hsc, taf, refHS, ref, country, unitC, issDate, nod, expDate, warnDate, userid, dept)) {
                response.setHeader("Refresh", "0;/TABLE2/MSS003/display?uid=" + uidx);
            } else {
                sendMessage = "<script type=\"text/javascript\">\n"
                        + "            var show2 = function () {\n"
                        + "                $('#myModal2').modal('show');\n"
                        + "            };\n"
                        + "\n"
                        + "            window.setTimeout(show2, 0);\n"
                        + "\n"
                        + "        </script>";
                forward = PAGE_VIEW;
                request.setAttribute("sendMessage", sendMessage);

                RequestDispatcher view = request.getRequestDispatcher(forward);
                view.forward(request, response);
            }
        }
    }
}
