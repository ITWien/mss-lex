/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QCUSTOMSDao;
import com.twc.wms.dao.SWGLINEDao;
import com.twc.wms.entity.MSSMASM;
import com.twc.wms.entity.MSSMASH;
import com.twc.wms.entity.QCUSTOMS;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author wien
 */
public class PrintControllerMSS213 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/printMSS213.jsp";
    private static final String FILE_PATH = "/report/";
    private static final DecimalFormat formatDou = new DecimalFormat("#,##0.00");
    private static final DecimalFormat formatDou3 = new DecimalFormat("#,##0.000");
    private static final int BUFSIZE = 4096;

    public PrintControllerMSS213() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "MSS213");
        String forward = "";

        SWGLINEDao dao = new SWGLINEDao();
        List<MSSMASH> cusList = dao.findCusList();

        SWGLINEDao dao1 = new SWGLINEDao();
        List<MSSMASM> invList = dao1.findInvList213();

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "Check List Material Cost");
                request.setAttribute("cusList", cusList);
                request.setAttribute("invList", invList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        request.setAttribute("PROGRAMNAME", "MSS213");
        request.setAttribute("PROGRAMDESC", "Check List Material Cost");

        String customer = request.getParameter("cutomer");
        String invoice = request.getParameter("invoice");
        String cif = request.getParameter("cif");
        String cur = request.getParameter("cur");

        if (cif.isEmpty()) {
            cif = "0";
        }

        // Blank workbook 
        XSSFWorkbook workbook = new XSSFWorkbook();

        QCUSTOMSDao daono = new QCUSTOMSDao();
        List<String> noList = daono.findNOList(invoice);

        List<String> amountList = new ArrayList<String>();
        List<String> unitList = new ArrayList<String>();
        double allAmount = 0;

        for (int k = 0; k < noList.size(); k++) {
            String[] pts = noList.get(k).split("-");
            String No = pts[0];
            String saleNo = pts[1];

            // Create a blank sheet 
            XSSFSheet sheet = workbook.createSheet(saleNo + "-" + No);

            QCUSTOMSDao dao = new QCUSTOMSDao();
            List<QCUSTOMS> QCUSTOMSList = dao.findAllMSS213(invoice, saleNo);

            // This data needs to be written (Object[]) 
            Map<String, Object[]> data = new TreeMap<String, Object[]>();
            int r = 11;
            data.put(Integer.toString(r), new Object[]{"No.", No});
            data.put(Integer.toString(r + 1), new Object[]{"Sale Order no.", saleNo});
            data.put(Integer.toString(r + 2), new Object[]{"No.", "VT", "�����ѵ�شԺ", "�����ѵ�شԺ",
                "Unit", "�ԡ��ԧ", "*", "�Ҥҵ��˹���", "Amount"});

            int rowNum = r + 3;
            int no = 10;
            double totalAmount = 0;
            int totalRow = 3;
            for (int i = 0; i < QCUSTOMSList.size(); i++) {
                if (QCUSTOMSList.get(i).getDescE().trim().equals(No.trim())) {
                    double qt = Double.parseDouble(QCUSTOMSList.get(i).getRef());
                    double rqt = Double.parseDouble(QCUSTOMSList.get(i).getRefHS());
                    String realqty = QCUSTOMSList.get(i).getRefHS();

                    String star = "";
                    String amt5 = "";

                    String qty = formatDou.format(Double.parseDouble(realqty));
                    String price = formatDou.format(Double.parseDouble(QCUSTOMSList.get(i).getPrice()));
                    String amount = formatDou3.format(Double.parseDouble(QCUSTOMSList.get(i).getHSC()));
                    if (!amount.substring(amount.length() - 1).equals("5")) {
                        amount = formatDou.format(Double.parseDouble(QCUSTOMSList.get(i).getHSC()));
                    } else {
                        amt5 = amount;
                        amount = amount.substring(0, amount.length() - 1);
                    }

                    if (qt != rqt) {
                        star = "*";
                    }

                    data.put(Integer.toString(rowNum), new Object[]{Integer.toString(no),
                        QCUSTOMSList.get(i).getComp(), QCUSTOMSList.get(i).getMatGroup(), QCUSTOMSList.get(i).getDesc(),
                        QCUSTOMSList.get(i).getUnit(), qty, star, price, amount, amt5});

                    if (!unitList.contains(QCUSTOMSList.get(i).getUnit())) {
                        unitList.add(QCUSTOMSList.get(i).getUnit());
                    }

                    totalAmount += Double.parseDouble(amount.replace(",", ""));
                    rowNum++;
                    no += 10;
                    totalRow++;
                }
            }

            data.put(Integer.toString(rowNum), new Object[]{"", "", "", "", "", "", "", "Total", formatDou.format(totalAmount)});

            amountList.add(formatDou.format(totalAmount));
            allAmount += totalAmount;

            // Create a Font for styling header cells 
            org.apache.poi.ss.usermodel.Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setFontHeightInPoints((short) 12);

            // Styling header
            CellStyle styleH = workbook.createCellStyle();
            styleH.setAlignment(HorizontalAlignment.CENTER);
            styleH.setFont(headerFont);

            // Styling bold
            CellStyle styleB = workbook.createCellStyle();
            styleB.setFont(headerFont);

            // Styling
            CellStyle style_rt = workbook.createCellStyle();
            style_rt.setAlignment(HorizontalAlignment.RIGHT);

            // Styling
            CellStyle style_ct = workbook.createCellStyle();
            style_ct.setAlignment(HorizontalAlignment.CENTER);

            //Set Column Width
            sheet.setColumnWidth(0, 4000);
            sheet.setColumnWidth(1, 3000);
            sheet.setColumnWidth(2, 4000);
            sheet.setColumnWidth(3, 10000);
            sheet.setColumnWidth(4, 2000);
            sheet.setColumnWidth(5, 3000);
            sheet.setColumnWidth(6, 800);
            sheet.setColumnWidth(7, 3500);
            sheet.setColumnWidth(8, 3000);

            // Iterate over data and write to sheet 
            Set<String> keyset = data.keySet();
            int rownum = 0;
            for (String key : keyset) {
                // this creates a new row in the sheet 
                Row row = sheet.createRow(rownum++);
                Object[] objArr = data.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                    // this line creates a cell in the next column of that row 
                    Cell cell = row.createCell(cellnum++);
                    if (rownum == 3) {
                        cell.setCellStyle(styleH);
                    } else if (rownum < 3) {
                        if (cellnum == 1) {
                            cell.setCellStyle(styleB);
                        }
                    } else if (rownum < (totalRow + 1) && rownum > 3) {
                        if (cellnum == 1 || cellnum == 2 || cellnum == 5) {
                            cell.setCellStyle(style_ct);
                        } else if (cellnum == 6 || cellnum >= 8) {
                            cell.setCellStyle(style_rt);
                        } else if (cellnum == 7) {
                            cell.setCellStyle(style_ct);
                        }
                    } else if (rownum == (totalRow + 1)) {
                        if (cellnum == 8) {
                            cell.setCellStyle(styleB);
                        } else if (cellnum == 9) {
                            cell.setCellStyle(style_rt);
                        }
                    }
                    if (obj instanceof String) {
                        cell.setCellValue((String) obj);
                    } else if (obj instanceof Integer) {
                        cell.setCellValue((Integer) obj);
                    } else if (obj instanceof Double) {
                        cell.setCellValue((Double) obj);
                    }
                }
            }
        }

        // Create a blank sheet 
        XSSFSheet sheet = workbook.createSheet(invoice.substring(0, 3) + " Total");

        // This data needs to be written (Object[]) 
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
        int r = 11;
        data.put(Integer.toString(r), new Object[]{"Customer :", invoice.substring(0, 3)});
        data.put(Integer.toString(r + 1), new Object[]{"Invoice no. :", invoice});
        data.put(Integer.toString(r + 2), new Object[]{""});
        data.put(Integer.toString(r + 3), new Object[]{"No.", "Sale Order no.", "", "Amount", "", "Currency"});

        int rowNum = r + 4;
        int totalRow = 4;
        String oldNo = "";
        for (int i = 0; i < noList.size(); i++) {
            String[] pts = noList.get(i).split("-");
            String No = pts[0];
            String saleNo = pts[1];

            String pNo = "";
            if (oldNo.equals("")) {
                oldNo = No;
                pNo = No;
            } else {
                if (!oldNo.equals(No)) {
                    pNo = No;
                }
            }

            data.put(Integer.toString(rowNum), new Object[]{pNo, saleNo, "", amountList.get(i), "", cur});
            rowNum++;
            totalRow++;
        }

        data.put(Integer.toString(rowNum), new Object[]{""});
        rowNum++;
        data.put(Integer.toString(rowNum), new Object[]{"", "", "(CIF)", "", "Diff"});
        rowNum++;
        data.put(Integer.toString(rowNum), new Object[]{"", "Total", formatDou.format(Double.parseDouble(cif)),
            formatDou.format(allAmount), formatDou.format(Math.abs((Double.parseDouble(cif)) - allAmount)), cur});
        rowNum++;
        data.put(Integer.toString(rowNum), new Object[]{""});
        rowNum++;
        data.put(Integer.toString(rowNum), new Object[]{"", "Total by U/M", "Quantity", "Amount", "U/M"});
        rowNum++;

        for (int i = 0; i < unitList.size(); i++) {
            double amt = 0;
            double qty = 0;

            QCUSTOMSDao dao = new QCUSTOMSDao();
            List<QCUSTOMS> QTMList = dao.findTotalbyUM(invoice, unitList.get(i).trim());
            for (int j = 0; j < QTMList.size(); j++) {
                amt += Double.parseDouble(QTMList.get(j).getHSC());
                qty += Double.parseDouble(QTMList.get(j).getRefHS());
            }

            data.put(Integer.toString(rowNum), new Object[]{"", "", formatDou.format(qty), formatDou.format(amt), unitList.get(i).trim()});
            rowNum++;
        }

        // Create a Font for styling header cells 
        org.apache.poi.ss.usermodel.Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);

        // Styling header
        CellStyle styleH = workbook.createCellStyle();
        styleH.setAlignment(HorizontalAlignment.CENTER);
        styleH.setFont(headerFont);

        // Styling bold
        CellStyle styleB = workbook.createCellStyle();
        styleB.setFont(headerFont);

        // Styling
        CellStyle style_rt = workbook.createCellStyle();
        style_rt.setAlignment(HorizontalAlignment.RIGHT);

        // Styling
        CellStyle style_ct = workbook.createCellStyle();
        style_ct.setAlignment(HorizontalAlignment.CENTER);

        //Set Column Width
        sheet.setColumnWidth(0, 4000);
        sheet.setColumnWidth(1, 4000);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 4000);
        sheet.setColumnWidth(4, 4000);
        sheet.setColumnWidth(5, 3000);

        // Iterate over data and write to sheet 
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet 
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row 
                Cell cell = row.createCell(cellnum++);
                if (rownum == 1 || rownum == 2) {
                    if (cellnum == 1) {
                        cell.setCellStyle(styleB);
                    }
                } else if (rownum == 4) {
                    if (cellnum == 1) {
                        cell.setCellStyle(styleB);
                    } else if (cellnum > 1) {
                        cell.setCellStyle(styleH);
                    }
                } else if (rownum > 4 && rownum < (totalRow + 1)) {
                    if (cellnum == 2 || cellnum == 4) {
                        cell.setCellStyle(style_rt);
                    } else if (cellnum == 6) {
                        cell.setCellStyle(style_ct);
                    }
                } else if (rownum == (totalRow + 2)) {
                    cell.setCellStyle(style_ct);
                } else if (rownum == (totalRow + 3)) {
                    if (cellnum == 2) {
                        cell.setCellStyle(styleB);
                    } else if (cellnum > 2 && cellnum < 6) {
                        cell.setCellStyle(style_rt);
                    } else if (cellnum == 6) {
                        cell.setCellStyle(style_ct);
                    }
                } else if (rownum == (totalRow + 5)) {
                    cell.setCellStyle(styleB);
                } else if (rownum > (totalRow + 5)) {
                    if (cellnum == 3 || cellnum == 4) {
                        cell.setCellStyle(style_rt);
                    } else if (cellnum == 5) {
                        cell.setCellStyle(styleB);
                    }
                }
                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                } else if (obj instanceof Double) {
                    cell.setCellValue((Double) obj);
                }
            }
        }

        try {

            ServletContext servletContext = getServletContext();    // new by ji
            String realPath = servletContext.getRealPath(FILE_PATH);        // new by ji
            String saperate = realPath.contains(":") ? "\\" : "/";
            String path = realPath + saperate + "MSS213.xlsx";

            // this Writes the workbook
            FileOutputStream out = new FileOutputStream(path);
            workbook.write(out);
            out.close();

            File file = new File(path);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(path);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) file.length());
            String fileName = (new File(path)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(file));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        response.setHeader("Refresh", "0;/TABLE2/MSS213/print");

    }

}
