/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.twc.wms.dao.QCUSTOMSDao;
import com.twc.wms.dao.SWGLINEDao;
import com.twc.wms.entity.MSSMASM;
import com.twc.wms.entity.MSSMASH;
import com.twc.wms.entity.QCUSTOMS;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.sl.usermodel.StrokeStyle.LineDash;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author wien
 */
public class PrintControllerMSS215 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/printMSS215.jsp";
    private static final String FILE_PATH = "/report/";
    private static final String FILE_FONT = "/resources/vendors/fonts/";
    private static final String FILE_LOGO = "/resources/images/";
    private static final DecimalFormat formatDou = new DecimalFormat("#,##0.00");
    private static final DecimalFormat formatDou3 = new DecimalFormat("#,##0.000");

    public PrintControllerMSS215() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "MSS215");
        String forward = "";

        SWGLINEDao dao = new SWGLINEDao();
        List<MSSMASH> cusList = dao.findCusList();

        SWGLINEDao dao1 = new SWGLINEDao();
        List<MSSMASM> invList = dao1.findInvList();

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "INVOICE");
                request.setAttribute("cusList", cusList);
                request.setAttribute("invList", invList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        request.setAttribute("PROGRAMNAME", "MSS215");
        request.setAttribute("PROGRAMDESC", "INVOICE");

        String customer = request.getParameter("comname");
        String comaddr1 = request.getParameter("comaddr1");
        String comaddr2 = request.getParameter("comaddr2");

        String invoice = request.getParameter("invoice");
        String[] pts = invoice.split(" - ");
        String pt1 = pts[0];
        invoice = pt1;

        String date = request.getParameter("date");
        date = date.replace("-", "");

        String cn1from = request.getParameter("cn1from");
        String cn1to = request.getParameter("cn1to");
        String cn2from = request.getParameter("cn2from");
        String cn2to = request.getParameter("cn2to");
        String cn3from = request.getParameter("cn3from");
        String cn3to = request.getParameter("cn3to");
        String shipFrom = request.getParameter("shipFrom");
        String shipTo = request.getParameter("shipTo");
        String via = request.getParameter("via");
        String insur = request.getParameter("insur");
        String fr = request.getParameter("fr");
        String cur = request.getParameter("cur");

        if (cn1to.equals("")) {
            cn1to = cn1from;
        }
        if (cn2to.equals("")) {
            cn2to = cn2from;
        }
        if (cn3to.equals("")) {
            cn3to = cn3from;
        }

        List<String> FinalList = new ArrayList<String>();
        List<Integer> CARList = new ArrayList<Integer>();
        List<MSSMASM> MATCARList = new ArrayList<MSSMASM>();

        if (cn1from.equals("") && cn2from.equals("") && cn3from.equals("")) {
            SWGLINEDao daof = new SWGLINEDao();
            List<MSSMASM> matCodeListAll = daof.findMatCodeList(invoice, date);
            for (int i = 0; i < matCodeListAll.size(); i++) {
                FinalList.add(matCodeListAll.get(i).getMatGroup());
                CARList.add(Integer.parseInt(matCodeListAll.get(i).getUnit()));

                MSSMASM p = new MSSMASM();
                p.setMatGroup(matCodeListAll.get(i).getMatGroup());
                p.setUnit(matCodeListAll.get(i).getUnit());
                MATCARList.add(p);
            }
        } else {
            if (!cn1from.equals("")) {
                SWGLINEDao dao = new SWGLINEDao();
                List<MSSMASM> matCodeList1 = dao.findMatCodeList1(invoice, date, cn1from, cn1to);
                for (int i = 0; i < matCodeList1.size(); i++) {
                    FinalList.add(matCodeList1.get(i).getMatGroup());
                    CARList.add(Integer.parseInt(matCodeList1.get(i).getUnit()));

                    MSSMASM p = new MSSMASM();
                    p.setMatGroup(matCodeList1.get(i).getMatGroup());
                    p.setUnit(matCodeList1.get(i).getUnit());
                    MATCARList.add(p);
                }

                if (!cn2from.equals("")) {
                    SWGLINEDao dao2 = new SWGLINEDao();
                    List<MSSMASM> matCodeList2 = dao2.findMatCodeList2(invoice, date, cn2from, cn2to);
                    for (int i = 0; i < matCodeList2.size(); i++) {
                        FinalList.add(matCodeList2.get(i).getMatGroup());
                        CARList.add(Integer.parseInt(matCodeList2.get(i).getUnit()));

                        MSSMASM p = new MSSMASM();
                        p.setMatGroup(matCodeList2.get(i).getMatGroup());
                        p.setUnit(matCodeList2.get(i).getUnit());
                        MATCARList.add(p);
                    }

                    if (!cn3from.equals("")) {
                        SWGLINEDao dao3 = new SWGLINEDao();
                        List<MSSMASM> matCodeList3 = dao3.findMatCodeList3(invoice, date, cn3from, cn3to);
                        for (int i = 0; i < matCodeList3.size(); i++) {
                            FinalList.add(matCodeList3.get(i).getMatGroup());
                            CARList.add(Integer.parseInt(matCodeList3.get(i).getUnit()));

                            MSSMASM p = new MSSMASM();
                            p.setMatGroup(matCodeList3.get(i).getMatGroup());
                            p.setUnit(matCodeList3.get(i).getUnit());
                            MATCARList.add(p);
                        }
                    }
                }
            }
        }

        Set<String> hs = new HashSet<String>();
        hs.addAll(FinalList);
        FinalList.clear();
        FinalList.addAll(hs);
        Collections.sort(FinalList);

        Set<Integer> hs1 = new HashSet<Integer>();
        hs1.addAll(CARList);
        CARList.clear();
        CARList.addAll(hs1);
        Collections.sort(CARList);

        int mm = Integer.parseInt(date.substring(4, 6));
        date = ConvertMonth(mm) + ". " + Integer.parseInt(date.substring(6, 8)) + "," + date.substring(0, 4);

        String pathD = "";

        try {
            Document document = new Document();
            document.setMargins(4, 4, 45, 20);
            Rectangle ps = new Rectangle(842, 595);
//            Rectangle ps = new Rectangle(595, 842);
            document.setPageSize(ps);

            String filename = "XMSS215";
            // Edit by ji
            ServletContext servletContext = getServletContext();    // new by ji
            String realPath = servletContext.getRealPath(FILE_PATH);        // new by ji
            String saperate = realPath.contains(":") ? "\\" : "/";
            pathD = realPath + saperate + filename + ".pdf";        // new by ji

            PdfWriter.getInstance(document, new FileOutputStream(pathD));

            document.open();

            ServletContext servletContext3 = getServletContext();
            String realPath3 = servletContext3.getRealPath(FILE_FONT);
            String saperate3 = realPath3.contains(":") ? "\\" : "/";
            String path3 = realPath3 + saperate3 + "upcfb.ttf";

            ServletContext servletContext4 = getServletContext();
            String realPath4 = servletContext4.getRealPath(FILE_FONT);
            String saperate4 = realPath4.contains(":") ? "\\" : "/";
            String path4 = realPath4 + saperate4 + "upcfl.ttf";

            ServletContext servletContext5 = getServletContext();
            String realPath5 = servletContext5.getRealPath(FILE_LOGO);
            String saperate5 = realPath5.contains(":") ? "\\" : "/";
            String path5 = realPath5 + saperate5 + "wc.png";

            Font fontThai = new Font(BaseFont.createFont(path3,
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 20);

            Font fontThai_l = new Font(BaseFont.createFont(path4,
                    BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 14);

            ServletContext servletContextTMP = getServletContext();    // new by ji
            String realPathTMP = servletContextTMP.getRealPath(FILE_PATH);        // new by ji
            String saperateTMP = realPathTMP.contains(":") ? "\\" : "/";
            String pathTMP = realPathTMP + saperateTMP + "TMP_MSS215.xlsx";

            FileInputStream fileTMP = new FileInputStream(pathTMP);

            // Blank workbook 
            XSSFWorkbook workbook = new XSSFWorkbook(fileTMP);

            // Create a blank sheet 
            XSSFSheet sheet = workbook.getSheetAt(0);

            //merge cell
//            sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 4));
            //Set Column Width
//            sheet.setColumnWidth(0, 4000);
//            sheet.setColumnWidth(1, 6000);
//            sheet.setColumnWidth(2, 6000);
//            sheet.setColumnWidth(3, 8000);
//            sheet.setColumnWidth(4, 4000);
//            sheet.setColumnWidth(5, 10000);
//            sheet.setColumnWidth(6, 10000);
//            sheet.setColumnWidth(7, 10000);
//            sheet.setColumnWidth(8, 4000);
//            sheet.setColumnWidth(9, 4000);
//            sheet.setColumnWidth(10, 6000);
//            sheet.setColumnWidth(11, 3000);
//            sheet.setColumnWidth(12, 8000);
//            sheet.setColumnWidth(13, 7000);
//            sheet.setColumnWidth(14, 8000);
            // add logo
            Image logo = Image.getInstance(path5);
            logo.scaleAbsolute(60, 50);
            logo.setAlignment(Element.ALIGN_CENTER);
            logo.setSpacingBefore(20);
            document.add(logo);

            // This data needs to be written (Object[]) 
            Map<String, Object[]> data = new TreeMap<String, Object[]>();
            Map<String, Object[]> data2 = new TreeMap<String, Object[]>();
            Map<String, Object[]> data22 = new TreeMap<String, Object[]>();
            Map<String, Object[]> data3 = new TreeMap<String, Object[]>();
            Map<String, Object[]> data4 = new TreeMap<String, Object[]>();
            Map<String, Object[]> data5 = new TreeMap<String, Object[]>();

            data.put(Integer.toString(1), new Object[]{"ORDER OF :", "MATERIALS FOR PRODUCE UNDERWARE",
                "", "", "INVOICE NO.", invoice});
            data.put(Integer.toString(2), new Object[]{"PAYMENT TERM :", "T/T REMITTANCE",
                "", "", "DATE :", date});
            data.put(Integer.toString(3), new Object[]{"SOLD TO :", "MYANMAR WACOAL COMPANY LIMITED",
                "", "", "MARKS", "INV. NO. " + invoice});
            data.put(Integer.toString(4), new Object[]{"", "THILAWA SPECIAL ECONOMIC ZONE A LOT NO-B2,",
                "", "", "", "MYANMAR WACOAL"});
            data.put(Integer.toString(5), new Object[]{"", "THANLYIN, YANGON REGION, MYANMAR",
                "", "", "", "COMPANY LIMITED"});
            data.put(Integer.toString(6), new Object[]{"", "Tel: (+959) 799698722, 09777338260",
                "", "", "", "C/N. " + CARList.get(0) + " - " + CARList.get(CARList.size() - 1)});
            data.put(Integer.toString(7), new Object[]{"", "",
                "", "", "", "GW"});
            data.put(Integer.toString(8), new Object[]{"SHIPPED FROM : " + shipFrom, "", "", "TO : " + shipTo,
                "VIA : " + via});
//            data.put(Integer.toString(9), new Object[]{"GGG"});
//            data.put(Integer.toString(11), new Object[]{"Material Code", "Aricle No.", "Description", "Width/Size",
//                "Unit", "QTTY", "Unit Price", "Amount"});

            Paragraph phead = new Paragraph("THAI WACOAL PUBLIC CO.,LTD. 132 SOI CHAROENRAT 7, BANGKLO, "
                    + "BANGKHOLAEM, BANGKOK, THAILAND 10120 TEL.2893100-9 FAX.(662) 291-1788", fontThai_l);
            phead.setAlignment(Element.ALIGN_CENTER);
            document.add(phead);

            Paragraph pinv = new Paragraph("INVOICE", fontThai);
            pinv.setAlignment(Element.ALIGN_CENTER);
            document.add(pinv);

            PdfPTable table8 = new PdfPTable(3);
            table8.setWidthPercentage(97);
            table8.setWidths(new float[]{95, 8, 20});
            table8.addCell(DETgetCell("ORDER OF : MATERIALS FOR PRODUCE UNDERWARE", PdfPCell.ALIGN_LEFT, path4, false));
            table8.addCell(DETgetCell("INVOICE NO. ", PdfPCell.ALIGN_LEFT, path4, false));
            table8.addCell(DETgetCell(invoice, PdfPCell.ALIGN_LEFT, path4, false));
            document.add(table8);

            PdfPTable table9 = new PdfPTable(3);
            table9.setWidthPercentage(97);
            table9.setWidths(new float[]{95, 8, 20});
            table9.addCell(DETgetCell("PAYMENT TERM : T/T REMITTANCE", PdfPCell.ALIGN_LEFT, path4, false));
            table9.addCell(DETgetCell("DATE : ", PdfPCell.ALIGN_LEFT, path4, false));
            table9.addCell(DETgetCell(date, PdfPCell.ALIGN_LEFT, path4, false));
            document.add(table9);

            PdfPTable table1 = new PdfPTable(3);
            table1.setWidthPercentage(97);
            table1.setWidths(new float[]{95, 8, 20});
            table1.addCell(DETgetCell("SOLD TO : MYANMAR WACOAL COMPANY LIMITED", PdfPCell.ALIGN_LEFT, path4, false));
            table1.addCell(DETgetCell("MARKS ", PdfPCell.ALIGN_LEFT, path4, false));
            table1.addCell(DETgetCell("INV. NO. " + invoice, PdfPCell.ALIGN_LEFT, path4, false));
            document.add(table1);

            PdfPTable table2 = new PdfPTable(3);
            table2.setWidthPercentage(97);
            table2.setWidths(new float[]{95, 8, 20});
            table2.addCell(DETgetCell("          THILAWA SPECIAL ECONOMIC ZONE A LOT NO-B2,", PdfPCell.ALIGN_LEFT, path4, false));
            table2.addCell(DETgetCell(" ", PdfPCell.ALIGN_LEFT, path4, false));
            table2.addCell(DETgetCell("MYANMAR WACOAL", PdfPCell.ALIGN_LEFT, path4, false));
            document.add(table2);

            PdfPTable table3 = new PdfPTable(3);
            table3.setWidthPercentage(97);
            table3.setWidths(new float[]{95, 8, 20});
            table3.addCell(DETgetCell("          THANLYIN, YANGON REGION, MYANMAR", PdfPCell.ALIGN_LEFT, path4, false));
            table3.addCell(DETgetCell(" ", PdfPCell.ALIGN_LEFT, path4, false));
            table3.addCell(DETgetCell("COMPANY LIMITED", PdfPCell.ALIGN_LEFT, path4, false));
            document.add(table3);

            PdfPTable table4 = new PdfPTable(3);
            table4.setWidthPercentage(97);
            table4.setWidths(new float[]{95, 8, 20});
            table4.addCell(DETgetCell("          Tel: (+959) 799698722, 09777338260", PdfPCell.ALIGN_LEFT, path4, false));
            table4.addCell(DETgetCell(" ", PdfPCell.ALIGN_LEFT, path4, false));
            table4.addCell(DETgetCell("C/N. " + CARList.get(0) + " - " + CARList.get(CARList.size() - 1), PdfPCell.ALIGN_LEFT, path4, false));
            document.add(table4);

            PdfPTable table5 = new PdfPTable(3);
            table5.setWidthPercentage(97);
            table5.setWidths(new float[]{95, 8, 20});
            table5.addCell(DETgetCell(" ", PdfPCell.ALIGN_LEFT, path4, false));
            table5.addCell(DETgetCell(" ", PdfPCell.ALIGN_LEFT, path4, false));
            table5.addCell(DETgetCell("GW ", PdfPCell.ALIGN_LEFT, path4, false));
            document.add(table5);

            PdfPTable table6 = new PdfPTable(3);
            table6.setWidthPercentage(97);
            table6.addCell(DETgetCell("SHIPPED FROM : " + shipFrom, PdfPCell.ALIGN_LEFT, path4, false));
            table6.addCell(DETgetCell("TO : " + shipTo, PdfPCell.ALIGN_LEFT, path4, false));
            table6.addCell(DETgetCell("VIA : " + via, PdfPCell.ALIGN_LEFT, path4, false));
            table6.setSpacingAfter(10);
            document.add(table6);

            List<String> UnitList = new ArrayList<String>();
            List<Double> UnitValueList = new ArrayList<Double>();
            List<String> UnitList1 = new ArrayList<String>();
            List<Double> UnitValueList1 = new ArrayList<Double>();
            List<String> UnitList2 = new ArrayList<String>();
            List<Double> UnitValueList2 = new ArrayList<Double>();
            List<QCUSTOMS> QCUSTOMSList = new ArrayList<QCUSTOMS>();
            List<QCUSTOMS> QCUSTOMSList2 = new ArrayList<QCUSTOMS>();

            SWGLINEDao daonw = new SWGLINEDao();
            SWGLINEDao daogw = new SWGLINEDao();
            SWGLINEDao daoms = new SWGLINEDao();
            int carS = CARList.get(0);
            int carF = CARList.get(CARList.size() - 1);
            double nw = daonw.findNW(invoice, carS, carF);
            double nw1 = new SWGLINEDao().findNW215(invoice, carS, carF, 1);
            double nw2 = new SWGLINEDao().findNW215(invoice, carS, carF, 2);
            double gw = daogw.findGW(invoice, carS, carF);
            double gw1 = new SWGLINEDao().findGW215(invoice, carS, carF, 1);
            double gw2 = new SWGLINEDao().findGW215(invoice, carS, carF, 2);
            double ms = daoms.findMS(invoice, carS, carF);
            double ms1 = new SWGLINEDao().findMS215(invoice, carS, carF, 1);
            double ms2 = new SWGLINEDao().findMS215(invoice, carS, carF, 2);
            double cif = 0;
            double cif1 = 0;
            double cif2 = 0;

            PdfPTable dataTabH = new PdfPTable(9);
            dataTabH.setWidthPercentage(97);
            dataTabH.setWidths(new float[]{4, 9, 20, 46, 7, 7, 9, 7, 9});
            dataTabH.addCell(DETgetCell("Item No.", PdfPCell.ALIGN_CENTER, path3, true));
            dataTabH.addCell(DETgetCell("Material Code", PdfPCell.ALIGN_CENTER, path3, true));
            dataTabH.addCell(DETgetCell("Aricle No.", PdfPCell.ALIGN_CENTER, path3, true));
            dataTabH.addCell(DETgetCell("Description", PdfPCell.ALIGN_CENTER, path3, true));
            dataTabH.addCell(DETgetCell("Width/Size", PdfPCell.ALIGN_CENTER, path3, true));
            dataTabH.addCell(DETgetCell("Unit", PdfPCell.ALIGN_CENTER, path3, true));
            dataTabH.addCell(DETgetCell("QTTY", PdfPCell.ALIGN_CENTER, path3, true));
            dataTabH.addCell(DETgetCell("Unit Price", PdfPCell.ALIGN_CENTER, path3, true));
            dataTabH.addCell(DETgetCell("Amount", PdfPCell.ALIGN_CENTER, path3, true));
            document.add(dataTabH);

            PdfPTable dataH = new PdfPTable(8);
            dataH.setWidthPercentage(97);
            dataH.setWidths(new float[]{4, 9, 20, 20, 26, 7, 7, 25});
            dataH.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, true));
            dataH.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, true));
            dataH.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, true));
            dataH.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, true));
            dataH.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, true));
            dataH.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, true));
            dataH.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, true));
            dataH.addCell(DETgetCell("CIF " + shipTo, PdfPCell.ALIGN_CENTER, path4, true));
            document.add(dataH);

            data22.put(Integer.toString(1), new Object[]{"", "", "",
                "", "", "", "CIF " + shipTo});

            List<String> FinalList2 = new ArrayList<String>();

            int ni = 100001;
            int it = 0;

            List<String> carList1 = new ArrayList<String>();
            List<String> carList2 = new ArrayList<String>();

            for (int i = 0; i < FinalList.size(); i++) {

                QCUSTOMSDao dao1 = new QCUSTOMSDao();
                QCUSTOMS p = dao1.findByInv(FinalList.get(i), invoice);

                if (p.getCountry().trim().equals("") || p.getCountry().trim().equals("Unknown")) {

                    FinalList2.add(FinalList.get(i));

                } else {

                    QCUSTOMSList.add(p);
//                    cif += Double.parseDouble(p.getTaf());
//                    cif1 += Double.parseDouble(p.getTaf());

                    if (!UnitList.contains(p.getUnit())) {
                        UnitList.add(p.getUnit());
                    }

                    if (!UnitList1.contains(p.getUnit())) {
                        UnitList1.add(p.getUnit());
                    }

                    String amount = null;
                    if (p.getTaf() != null) {
                        amount = formatDou.format(Math.round(Double.parseDouble(p.getTaf()) * 100.0) / 100.0);
                        cif += Math.round(Double.parseDouble(p.getTaf()) * 100.0) / 100.0;
                        cif1 += Math.round(Double.parseDouble(p.getTaf()) * 100.0) / 100.0;
                    }

                    String qtty = null;
                    if (p.getHSC() != null) {
                        qtty = formatDou.format(Double.parseDouble(p.getHSC()));
                    }

                    PdfPTable dataTab = new PdfPTable(10);
                    dataTab.setWidthPercentage(97);
                    dataTab.setWidths(new float[]{4, 9, 20, 20, 26, 7, 7, 9, 7, 9});
                    dataTab.addCell(DETgetCell(Integer.toString(++it), PdfPCell.ALIGN_LEFT, path4, true));
                    dataTab.addCell(DETgetCell(p.getMatGroup(), PdfPCell.ALIGN_LEFT, path4, true));
                    dataTab.addCell(DETgetCell(p.getDesc(), PdfPCell.ALIGN_LEFT, path4, true));
                    dataTab.addCell(DETgetCell(p.getDescE(), PdfPCell.ALIGN_LEFT, path4, true));
                    dataTab.addCell(DETgetCell(p.getSpec(), PdfPCell.ALIGN_LEFT, path4, true));
                    dataTab.addCell(DETgetCell(p.getWidth(), PdfPCell.ALIGN_CENTER, path4, true));
                    dataTab.addCell(DETgetCell(p.getUnit(), PdfPCell.ALIGN_CENTER, path4, true));
                    dataTab.addCell(DETgetCell(qtty, PdfPCell.ALIGN_RIGHT, path4, true));
                    dataTab.addCell(DETgetCell(p.getPrice(), PdfPCell.ALIGN_RIGHT, path4, true));
                    dataTab.addCell(DETgetCell(amount, PdfPCell.ALIGN_RIGHT, path4, true));
                    document.add(dataTab);

                    //excel
                    data2.put(Integer.toString(ni), new Object[]{Integer.toString(it), p.getMatGroup(), p.getDesc(), p.getDescE(),
                        p.getSpec(), p.getWidth(), p.getUnit(), qtty, p.getPrice(), amount});
                    ni++;

                    for (int j = 0; j < MATCARList.size(); j++) {
                        if (FinalList.get(i).trim().equals(MATCARList.get(j).getMatGroup().trim())) {
                            if (!carList1.contains(MATCARList.get(j).getUnit())) {
                                carList1.add(MATCARList.get(j).getUnit());
                            }
                        }
                    }

                }
            }

            Paragraph wall = new Paragraph("---------------------------------------------------------------"
                    + "---------------------------------------", fontThai);
            wall.setAlignment(Element.ALIGN_CENTER);
            wall.setSpacingBefore(-24.0f);
            wall.setSpacingAfter(-2.0f);
            document.add(wall);

            for (int i = 0; i < UnitList1.size(); i++) {
                double qtu = 0;
                for (int j = 0; j < QCUSTOMSList.size(); j++) {
                    if (QCUSTOMSList.get(j).getUnit().equals(UnitList1.get(i))) {
                        qtu += Double.parseDouble(QCUSTOMSList.get(j).getHSC());
                    }
                }
                UnitValueList1.add(qtu);
            }
            
            for (int i = 0; i < FinalList2.size(); i++) {

                for (int j = 0; j < MATCARList.size(); j++) {
                    if (FinalList2.get(i).trim().equals(MATCARList.get(j).getMatGroup().trim())) {
                        if (!carList2.contains(MATCARList.get(j).getUnit())) {
                            carList2.add(MATCARList.get(j).getUnit());
                        }
                    }
                }

                QCUSTOMSDao dao1 = new QCUSTOMSDao();
                QCUSTOMS p = dao1.findByInv(FinalList2.get(i), invoice);

                QCUSTOMSList.add(p);
                QCUSTOMSList2.add(p);
//                cif += Double.parseDouble(p.getTaf());
//                cif2 += Double.parseDouble(p.getTaf());

                if (!UnitList.contains(p.getUnit())) {
                    UnitList.add(p.getUnit());
                }

                if (!UnitList2.contains(p.getUnit())) {
                    UnitList2.add(p.getUnit());
                }

                String amount = null;
                if (p.getTaf() != null) {
                    amount = formatDou.format(Math.round(Double.parseDouble(p.getTaf()) * 100.0) / 100.0);
                    cif += Math.round(Double.parseDouble(p.getTaf()) * 100.0) / 100.0;
                    cif2 += Math.round(Double.parseDouble(p.getTaf()) * 100.0) / 100.0;
                }

                String qtty = null;
                if (p.getHSC() != null) {
                    qtty = formatDou.format(Double.parseDouble(p.getHSC()));
                }

                PdfPTable dataTab = new PdfPTable(10);
                dataTab.setWidthPercentage(97);
                dataTab.setWidths(new float[]{4, 9, 20, 20, 26, 7, 7, 9, 7, 9});
                dataTab.addCell(DETgetCell(Integer.toString(++it), PdfPCell.ALIGN_LEFT, path4, true));
                dataTab.addCell(DETgetCell(p.getMatGroup(), PdfPCell.ALIGN_LEFT, path4, true));
                dataTab.addCell(DETgetCell(p.getDesc(), PdfPCell.ALIGN_LEFT, path4, true));
                dataTab.addCell(DETgetCell(p.getDescE(), PdfPCell.ALIGN_LEFT, path4, true));
                dataTab.addCell(DETgetCell(p.getSpec(), PdfPCell.ALIGN_LEFT, path4, true));
                dataTab.addCell(DETgetCell(p.getWidth(), PdfPCell.ALIGN_CENTER, path4, true));
                dataTab.addCell(DETgetCell(p.getUnit(), PdfPCell.ALIGN_CENTER, path4, true));
                dataTab.addCell(DETgetCell(qtty, PdfPCell.ALIGN_RIGHT, path4, true));
                dataTab.addCell(DETgetCell(p.getPrice(), PdfPCell.ALIGN_RIGHT, path4, true));
                dataTab.addCell(DETgetCell(amount, PdfPCell.ALIGN_RIGHT, path4, true));
                document.add(dataTab);

                if (i == 0) {
                    //excel
                    data2.put(Integer.toString(ni), new Object[]{Integer.toString(it) + "xxOGxx", p.getMatGroup() + "xxOGxx", p.getDesc() + "xxOGxx", p.getDescE() + "xxOGxx",
                        p.getSpec() + "xxOGxx", p.getWidth() + "xxOGxx", p.getUnit() + "xxOGxx", qtty + "xxOGxx", p.getPrice() + "xxOGxx", amount + "xxOGxx"});
                    ni++;
                } else {
                    //excel
                    data2.put(Integer.toString(ni), new Object[]{Integer.toString(it), p.getMatGroup(), p.getDesc(), p.getDescE(),
                        p.getSpec(), p.getWidth(), p.getUnit(), qtty, p.getPrice(), amount});
                    ni++;
                }

            }

            for (int i = 0; i < UnitList2.size(); i++) {
                double qtu = 0;
                for (int j = 0; j < QCUSTOMSList2.size(); j++) {
                    if (QCUSTOMSList2.get(j).getUnit().equals(UnitList2.get(i))) {
                        qtu += Double.parseDouble(QCUSTOMSList2.get(j).getHSC());
                    }
                }
                UnitValueList2.add(qtu);
            }

            for (int i = 0; i < UnitList.size(); i++) {
                double qtu = 0;
                for (int j = 0; j < QCUSTOMSList.size(); j++) {
                    if (QCUSTOMSList.get(j).getUnit().equals(UnitList.get(i))) {
                        qtu += Double.parseDouble(QCUSTOMSList.get(j).getHSC());
                    }
                }
                UnitValueList.add(qtu);
            }

            String total = "";
            for (int i = 0; i < UnitList.size(); i++) {
                total += (formatDou.format(UnitValueList.get(i)) + " " + UnitList.get(i) + "; ");
            }

            String total1 = "";
            for (int i = 0; i < UnitList1.size(); i++) {
                total1 += (formatDou.format(UnitValueList1.get(i)) + " " + UnitList1.get(i) + "; ");
            }

            String total2 = "";
            for (int i = 0; i < UnitList2.size(); i++) {
                total2 += (formatDou.format(UnitValueList2.get(i)) + " " + UnitList2.get(i) + "; ");
            }

            double insur1 = Double.parseDouble(insur) * (Double.valueOf(carList1.size()) / Double.valueOf(CARList.size()));
            double insur2 = Double.parseDouble(insur) * (Double.valueOf(carList2.size()) / Double.valueOf(CARList.size()));

            double fr1 = Double.parseDouble(fr) * (Double.valueOf(carList1.size()) / Double.valueOf(CARList.size()));
            double fr2 = Double.parseDouble(fr) * (Double.valueOf(carList2.size()) / Double.valueOf(CARList.size()));

            double fob = cif - (Double.parseDouble(insur)) - (Double.parseDouble(fr));
            double fob1 = cif1 - insur1 - fr1;
            double fob2 = cif2 - insur2 - fr2;

            data3.put(Integer.toString(1), new Object[]{"", "", "TOTAL (" + total + ")", "", "", "", "CIF", cur, formatDou.format(cif)});
            data3.put(Integer.toString(2), new Object[]{"", "TOTAL :", Integer.toString(CARList.size()), "CARTONS", "", "", "INSURANCE", cur, formatDou.format(Double.parseDouble(insur))});
            data3.put(Integer.toString(3), new Object[]{"", "NET WEIGHT :", formatDou.format(nw), "KGM", "", "", "FREIGHT", cur, formatDou.format(Double.parseDouble(fr))});
            data3.put(Integer.toString(4), new Object[]{"", "GROSS WEIGHT :", formatDou.format(gw), "KGM", "", "", "FOB", cur, formatDou.format(fob)});
            data3.put(Integer.toString(5), new Object[]{"", "MEASURMENT :", formatDou3.format(ms), "CUM"});

            data5.put(Integer.toString(1), new Object[]{"C/O".toUpperCase(), "Country   TOTAL :".toUpperCase(), Integer.toString(carList2.size()) + "xxRTxx", "CARTONS"});

            data4.put(Integer.toString(1), new Object[]{"FORM D".toUpperCase(), "Country   TOTAL :".toUpperCase(), Integer.toString(carList1.size()) + "xxRTxx", "CARTONS"});

            PdfPTable table7 = new PdfPTable(7);
            table7.setWidthPercentage(97);
            table7.setWidths(new float[]{9, 20, 65, 7, 7, 7, 8});
            table7.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            table7.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            table7.addCell(DETgetCell("TOTAL (" + total + ")", PdfPCell.ALIGN_LEFT, path4, false));
            table7.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            table7.addCell(DETgetCell("CIF", PdfPCell.ALIGN_CENTER, path4, false));
            table7.addCell(DETgetCell(cur, PdfPCell.ALIGN_CENTER, path4, false));
            table7.addCell(DETgetCell(formatDou.format(cif), PdfPCell.ALIGN_RIGHT, path4, false));
            table7.setSpacingBefore(10);
            document.add(table7);

            PdfPTable tab1 = new PdfPTable(9);
            tab1.setWidthPercentage(97);
            tab1.setWidths(new float[]{9, 20, 20, 30, 7, 7, 7, 7, 8});
            tab1.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            tab1.addCell(DETgetCell("TOTAL :", PdfPCell.ALIGN_RIGHT, path4, false));
            tab1.addCell(DETgetCell(Integer.toString(CARList.size()), PdfPCell.ALIGN_RIGHT, path4, false));
            tab1.addCell(DETgetCell(" CARTONS", PdfPCell.ALIGN_LEFT, path4, false));
            tab1.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            tab1.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            tab1.addCell(DETgetCell("INSURANCE", PdfPCell.ALIGN_CENTER, path4, false));
            tab1.addCell(DETgetCell(cur, PdfPCell.ALIGN_CENTER, path4, false));
            tab1.addCell(DETgetCell(formatDou.format(Double.parseDouble(insur)), PdfPCell.ALIGN_RIGHT, path4, false));
            document.add(tab1);

            PdfPTable tab2 = new PdfPTable(9);
            tab2.setWidthPercentage(97);
            tab2.setWidths(new float[]{9, 20, 20, 30, 7, 7, 7, 7, 8});
            tab2.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            tab2.addCell(DETgetCell("NET WEIGHT :", PdfPCell.ALIGN_RIGHT, path4, false));
            tab2.addCell(DETgetCell(formatDou.format(nw), PdfPCell.ALIGN_RIGHT, path4, false));
            tab2.addCell(DETgetCell(" KGM", PdfPCell.ALIGN_LEFT, path4, false));
            tab2.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            tab2.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            tab2.addCell(DETgetCell("FREIGHT", PdfPCell.ALIGN_CENTER, path4, false));
            tab2.addCell(DETgetCell(cur, PdfPCell.ALIGN_CENTER, path4, false));
            tab2.addCell(DETgetCell(formatDou.format(Double.parseDouble(fr)), PdfPCell.ALIGN_RIGHT, path4, false));
            document.add(tab2);

            PdfPTable tab3 = new PdfPTable(9);
            tab3.setWidthPercentage(97);
            tab3.setWidths(new float[]{9, 20, 20, 30, 7, 7, 7, 7, 8});
            tab3.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            tab3.addCell(DETgetCell("GROSS WEIGHT :", PdfPCell.ALIGN_RIGHT, path4, false));
            tab3.addCell(DETgetCell(formatDou.format(gw), PdfPCell.ALIGN_RIGHT, path4, false));
            tab3.addCell(DETgetCell(" KGM", PdfPCell.ALIGN_LEFT, path4, false));
            tab3.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            tab3.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            tab3.addCell(DETgetCell("FOB", PdfPCell.ALIGN_CENTER, path4, false));
            tab3.addCell(DETgetCell(cur, PdfPCell.ALIGN_CENTER, path4, false));
            tab3.addCell(DETgetCell(formatDou.format(fob), PdfPCell.ALIGN_RIGHT, path4, false));
            document.add(tab3);

            PdfPTable tab4 = new PdfPTable(9);
            tab4.setWidthPercentage(97);
            tab4.setWidths(new float[]{9, 20, 20, 30, 7, 7, 7, 7, 8});
            tab4.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            tab4.addCell(DETgetCell("MEASURMENT :", PdfPCell.ALIGN_RIGHT, path4, false));
            tab4.addCell(DETgetCell(formatDou3.format(ms), PdfPCell.ALIGN_RIGHT, path4, false));
            tab4.addCell(DETgetCell(" CUM", PdfPCell.ALIGN_LEFT, path4, false));
            tab4.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            tab4.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
            tab4.addCell(DETgetCell("", PdfPCell.ALIGN_CENTER, path4, false));
            tab4.addCell(DETgetCell("", PdfPCell.ALIGN_CENTER, path4, false));
            tab4.addCell(DETgetCell("", PdfPCell.ALIGN_RIGHT, path4, false));
            document.add(tab4);

            //-------------------------------------------------------------22222
//            PdfPTable table72 = new PdfPTable(9);
//            table72.setWidthPercentage(97);
//            table72.setWidths(new float[]{9, 20, 20, 30, 7, 7, 7, 7, 7});
//            table72.addCell(DETgetCell("FORM D".toUpperCase(), PdfPCell.ALIGN_CENTER, path4, false));
//            table72.addCell(DETgetCell("Origin of Country       TOTAL :".toUpperCase(), PdfPCell.ALIGN_RIGHT, path4, false));
//            table72.addCell(DETgetCell(Integer.toString(carList2.size()), PdfPCell.ALIGN_RIGHT, path4, false));
//            table72.addCell(DETgetCell(" CARTONS", PdfPCell.ALIGN_LEFT, path4, false));
//            table72.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
//            table72.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
//            table72.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
//            table72.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
//            table72.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
//            table72.setSpacingBefore(10);
//            document.add(table72);
            //-------------------------------------------------------------11111
//            PdfPTable table71 = new PdfPTable(9);
//            table71.setWidthPercentage(97);
//            table71.setWidths(new float[]{9, 20, 20, 30, 7, 7, 7, 7, 7});
//            table71.addCell(DETgetCell("C/O".toUpperCase(), PdfPCell.ALIGN_CENTER, path4, false));
//            table71.addCell(DETgetCell("Origin of Country       TOTAL :".toUpperCase(), PdfPCell.ALIGN_RIGHT, path4, false));
//            table71.addCell(DETgetCell(Integer.toString(carList1.size()), PdfPCell.ALIGN_RIGHT, path4, false));
//            table71.addCell(DETgetCell(" CARTONS", PdfPCell.ALIGN_LEFT, path4, false));
//            table71.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
//            table71.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
//            table71.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
//            table71.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
//            table71.addCell(DETgetCell("", PdfPCell.ALIGN_LEFT, path4, false));
//            document.add(table71);
            document.close();

            ServletContext servletContext2 = getServletContext();
            String realPath2 = servletContext2.getRealPath(FILE_PATH);
            String saperate2 = realPath2.contains(":") ? "\\" : "/";
            String path2 = realPath2 + saperate2 + filename.substring(1) + ".pdf";

            new PrintControllerMSS215().manipulatePdf(pathD, path2, path3);
            File file = new File(pathD);
            file.delete();

            //*****************excel*******************************************
            // Styling border and alignment of cell.  
            CellStyle style_ct = workbook.createCellStyle();
            style_ct.setAlignment(HorizontalAlignment.CENTER);

            // Styling border and alignment of cell.
            CellStyle style_rt = workbook.createCellStyle();
            style_rt.setAlignment(HorizontalAlignment.RIGHT);

            // Styling border and alignment of cell.  
            CellStyle style_bd = workbook.createCellStyle();
            style_bd.setBorderBottom(BorderStyle.THIN);
            style_bd.setBorderTop(BorderStyle.THIN);
            style_bd.setBorderRight(BorderStyle.THIN);
            style_bd.setBorderLeft(BorderStyle.THIN);

            // Styling border and alignment of cell.  
            CellStyle style_bd_ct = workbook.createCellStyle();
            style_bd_ct.setAlignment(HorizontalAlignment.CENTER);
            style_bd_ct.setBorderBottom(BorderStyle.THIN);
            style_bd_ct.setBorderTop(BorderStyle.THIN);
            style_bd_ct.setBorderRight(BorderStyle.THIN);
            style_bd_ct.setBorderLeft(BorderStyle.THIN);

            // Styling border and alignment of cell.  
            CellStyle style_bd_rt = workbook.createCellStyle();
            style_bd_rt.setAlignment(HorizontalAlignment.RIGHT);
            style_bd_rt.setBorderBottom(BorderStyle.THIN);
            style_bd_rt.setBorderTop(BorderStyle.THIN);
            style_bd_rt.setBorderRight(BorderStyle.THIN);
            style_bd_rt.setBorderLeft(BorderStyle.THIN);

            // Iterate over data and write to sheet 
            Set<String> keyset = data.keySet();
            int rownum = 7;
            for (String key : keyset) {
                // this creates a new row in the sheet 
                Row row = sheet.createRow(rownum++);
                Object[] objArr = data.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                    // this line creates a cell in the next column of that row 
                    Cell cell = row.createCell(cellnum++);

                    if (obj instanceof String) {
                        cell.setCellValue((String) obj);
                    } else if (obj instanceof Integer) {
                        cell.setCellValue((Integer) obj);
                    } else if (obj instanceof Double) {
                        cell.setCellValue((Double) obj);
                    }
                }
            }

            // Iterate over data and write to sheet 
            Set<String> keyset22 = data22.keySet();
            int rownum22 = 17;
            for (String key22 : keyset22) {
                // this creates a new row in the sheet 
                Row row = sheet.createRow(rownum22++);
                Object[] objArr = data22.get(key22);
                int cellnum22 = 0;
                for (Object obj : objArr) {
                    // this line creates a cell in the next column of that row 
                    Cell cell = row.createCell(cellnum22++);
                    cell.setCellStyle(style_bd_ct);

                    if (obj instanceof String) {
                        cell.setCellValue((String) obj);
                    } else if (obj instanceof Integer) {
                        cell.setCellValue((Integer) obj);
                    } else if (obj instanceof Double) {
                        cell.setCellValue((Double) obj);
                    }
                }
            }

            // Iterate over data and write to sheet 
            Set<String> keyset2 = data2.keySet();
            int rownum2 = 18;
            for (String key2 : keyset2) {
                // this creates a new row in the sheet 
                Row row = sheet.createRow(rownum2++);
                Object[] objArr = data2.get(key2);
                int cellnum2 = 0;
                for (Object obj : objArr) {
                    // this line creates a cell in the next column of that row 
                    Cell cell = row.createCell(cellnum2++);
                    if (rownum2 == 18) {
                        cell.setCellStyle(style_bd_ct);
                    } else {
                        if (cellnum2 == 6 || cellnum2 == 7) {
                            cell.setCellStyle(style_bd_ct);
                        } else if (cellnum2 >= 8 && cellnum2 <= 10) {
                            cell.setCellStyle(style_bd_rt);
                        } else {
                            cell.setCellStyle(style_bd);
                        }
                    }

                    cell.setCellValue((String) obj);
                    if (cell.getStringCellValue().contains("xxOGxx")) {
                        CellStyle style = workbook.createCellStyle();
                        style.setBorderBottom(BorderStyle.THIN);
                        style.setBorderTop(BorderStyle.THICK);
                        style.setBorderRight(BorderStyle.THIN);
                        style.setBorderLeft(BorderStyle.THIN);
                        if (cellnum2 == 6 || cellnum2 == 7) {
                            style.setAlignment(HorizontalAlignment.CENTER);
                        } else if (cellnum2 >= 8 && cellnum2 <= 10) {
                            style.setAlignment(HorizontalAlignment.RIGHT);
                        }
                        cell.setCellStyle(style);

                        cell.setCellValue(cell.getStringCellValue().replace("xxOGxx", ""));
                    }

                }
            }

            sheet.addMergedRegion(new CellRangeAddress((rownum2 + 1), (rownum2 + 1), 2, 5));

            // Iterate over data and write to sheet 
            Set<String> keyset3 = data3.keySet();
            int rownum3 = rownum2 + 1;
            int rn3 = rownum3;
            for (String key3 : keyset3) {
                // this creates a new row in the sheet 
                Row row = sheet.createRow(rownum3++);
                Object[] objArr = data3.get(key3);
                int cellnum3 = 0;
                for (Object obj : objArr) {
                    // this line creates a cell in the next column of that row 
                    Cell cell = row.createCell(cellnum3++);
                    if (cellnum3 == 7 || cellnum3 == 8) {
                        cell.setCellStyle(style_ct);
                    } else if (cellnum3 == 9) {
                        cell.setCellStyle(style_rt);
                    }

                    if (rownum3 != (rn3 + 1)) {
                        if (cellnum3 == 2 || cellnum3 == 3) {
                            cell.setCellStyle(style_rt);
                        }
                    }

                    if (obj instanceof String) {
                        cell.setCellValue((String) obj);
                    } else if (obj instanceof Integer) {
                        cell.setCellValue((Integer) obj);
                    } else if (obj instanceof Double) {
                        cell.setCellValue((Double) obj);
                    }
                }
            }

//            sheet.addMergedRegion(new CellRangeAddress((rownum3 + 1), (rownum3 + 1), 2, 5));
            // Iterate over data and write to sheet 
            Set<String> keyset4 = data4.keySet();
            int rownum4 = rownum3 + 1;
            int rn4 = rownum4;
            for (String key4 : keyset4) {
                // this creates a new row in the sheet 
                Row row = sheet.createRow(rownum4++);
                Object[] objArr = data4.get(key4);
                int cellnum4 = 0;
                for (Object obj : objArr) {
                    // this line creates a cell in the next column of that row 
                    Cell cell = row.createCell(cellnum4++);
                    if (cellnum4 == 7 || cellnum4 == 8) {
                        cell.setCellStyle(style_ct);
                    } else if (cellnum4 == 9) {
                        cell.setCellStyle(style_rt);
                    }

                    if (rownum4 != (rn4 + 1)) {
                        if (cellnum4 == 2 || cellnum4 == 3) {
                            cell.setCellStyle(style_rt);
                        }
                    }

                    cell.setCellValue((String) obj);
                    if (cell.getStringCellValue().contains("xxRTxx")) {
                        CellStyle style = workbook.createCellStyle();
                        style.setAlignment(HorizontalAlignment.RIGHT);
                        cell.setCellStyle(style);

                        cell.setCellValue(cell.getStringCellValue().replace("xxRTxx", ""));
                    }

                }
            }

//            sheet.addMergedRegion(new CellRangeAddress((rownum4 + 1), (rownum4 + 1), 2, 5));
            // Iterate over data and write to sheet 
            Set<String> keyset5 = data5.keySet();
            int rownum5 = rownum4;
            int rn5 = rownum5;
            for (String key5 : keyset5) {
                // this creates a new row in the sheet 
                Row row = sheet.createRow(rownum5++);
                Object[] objArr = data5.get(key5);
                int cellnum5 = 0;
                for (Object obj : objArr) {
                    // this line creates a cell in the next column of that row 
                    Cell cell = row.createCell(cellnum5++);
                    if (cellnum5 == 7 || cellnum5 == 8) {
                        cell.setCellStyle(style_ct);
                    } else if (cellnum5 == 9) {
                        cell.setCellStyle(style_rt);
                    }

                    if (rownum5 != (rn5 + 1)) {
                        if (cellnum5 == 2 || cellnum5 == 3) {
                            cell.setCellStyle(style_rt);
                        }
                    }

                    cell.setCellValue((String) obj);
                    if (cell.getStringCellValue().contains("xxRTxx")) {
                        CellStyle style = workbook.createCellStyle();
                        style.setAlignment(HorizontalAlignment.RIGHT);
                        cell.setCellStyle(style);

                        cell.setCellValue(cell.getStringCellValue().replace("xxRTxx", ""));
                    }

                }
            }

            ServletContext servletContextEX = getServletContext();    // new by ji
            String realPathEX = servletContextEX.getRealPath(FILE_PATH);        // new by ji
            String saperateEX = realPathEX.contains(":") ? "\\" : "/";
            String pathEX = realPathEX + saperateEX + "MSS215.xlsx";

            // this Writes the workbook
            FileOutputStream out = new FileOutputStream(pathEX);
            workbook.write(out);
            out.close();

        } catch (DocumentException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            System.err.println(e.toString());
        } catch (NumberFormatException e) {
            System.err.println(e.toString());
        }

        String forward = PAGE_VIEW;
        String showPDF = "";

        showPDF = "<script type=\"text/javascript\">\n"
                + "                 window.onload = function (e) {\n"
                + "                     window.open(\"/TABLE2/MSS215/display\");\n"
                + "                     window.close();\n"
                + "                 };\n"
                + "        </script>";

        request.setAttribute("showPDF", showPDF);

        SWGLINEDao dao = new SWGLINEDao();
        List<MSSMASH> cusList = dao.findCusList();

        SWGLINEDao dao1 = new SWGLINEDao();
        List<MSSMASM> invList = dao1.findInvList();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("cusList", cusList);
        request.setAttribute("invList", invList);
        view.forward(request, response);

    }

    public PdfPCell DETgetCell(String text, int alignment, String srcFont, boolean border) throws IOException, DocumentException {
        Font fontThai = new Font(BaseFont.createFont(srcFont,
                BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 13);
        PdfPCell cell = new PdfPCell(new Phrase(text, fontThai));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        if (!border) {
            cell.setBorder(PdfPCell.NO_BORDER);
//            cell.setBorder(Rectangle.BOTTOM);
        } else {
            cell.setPaddingBottom(3);
            cell.setPaddingRight(3);
            cell.setPaddingLeft(3);
        }
        return cell;
    }

    public void manipulatePdf(String src, String dest, String srcFont) throws IOException, DocumentException {
        Font fontThai = new Font(BaseFont.createFont(srcFont,
                BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 14);
        Font fontThaiH = new Font(BaseFont.createFont(srcFont,
                BaseFont.IDENTITY_H, BaseFont.EMBEDDED), 13);
        PdfReader reader = new PdfReader(src);
        int n = reader.getNumberOfPages();
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        PdfContentByte pagecontent;
        for (int i = 0; i < n;) {
            pagecontent = stamper.getOverContent(++i);
            ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                    new Phrase(String.format("Page %s of %s", i, n), fontThai), 827, 575.5f, 0);
            if (i != 1) {
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("Item     "
                                + "Material Code                 "
                                + "Aricle No.                                    "
                                + "                            "
                                + "Description                                   "
                                + "                 "
                                + "Width/Size       "
                                + "Unit             "
                                + "QTTY          "
                                + "Unit Price           "
                                + "Amount"), fontThaiH), 820, 552.5f, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format("________________________________"
                                + "_______________________________________________"
                                + "_______________________________________________"
                                + "_______________________________________________"
                                + "_________________________"), fontThaiH), 420.5f, 565.5f, 0);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                        new Phrase(String.format("________________________________"
                                + "_______________________________________________"
                                + "_______________________________________________"
                                + "_______________________________________________"
                                + "_________________________"), fontThaiH), 421.5f, 565.5f, 0);

                if (i == n) {
                    ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                            new Phrase(String.format("________________________________"
                                    + "_______________________________________________"
                                    + "_______________________________________________"
                                    + "_______________________________________________"
                                    + "_________________________"), fontThaiH), 420.5f, 550.7f, 0);
                    ColumnText.showTextAligned(pagecontent, Element.ALIGN_CENTER,
                            new Phrase(String.format("________________________________"
                                    + "_______________________________________________"
                                    + "_______________________________________________"
                                    + "_______________________________________________"
                                    + "_________________________"), fontThaiH), 421.5f, 550.7f, 0);
                }

                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 824.7f, 565, 90);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 824.7f, 564, 90);

                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 15.7f, 565, 90);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 15.7f, 564, 90);

                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 43.5f, 565, 90);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 43.5f, 564, 90);

                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 105.5f, 565, 90);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 105.5f, 564, 90);

                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 241.5f, 565, 90);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 241.5f, 564, 90);

                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 557.0f, 565, 90);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 557.0f, 564, 90);

                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 605.5f, 565, 90);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 605.5f, 564, 90);

                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 653.5f, 565, 90);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 653.5f, 564, 90);

                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 714.5f, 565, 90);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 714.5f, 564, 90);

                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 763, 565, 90);
                ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                        new Phrase(String.format("____"), fontThaiH), 763, 564, 90);
            }
        }
        stamper.close();
        reader.close();
    }

    public String ConvertMonth(int mm) {
        String Smonth = "";
        switch (mm) {
            case 1:
                Smonth = "JAN";
                break;
            case 2:
                Smonth = "FEB";
                break;
            case 3:
                Smonth = "MAR";
                break;
            case 4:
                Smonth = "APR";
                break;
            case 5:
                Smonth = "MAY";
                break;
            case 6:
                Smonth = "JUN";
                break;
            case 7:
                Smonth = "JUL";
                break;
            case 8:
                Smonth = "AUG";
                break;
            case 9:
                Smonth = "SEP";
                break;
            case 10:
                Smonth = "OCT";
                break;
            case 11:
                Smonth = "NOV";
                break;
            case 12:
                Smonth = "DEC";
                break;
            default:
                Smonth = "Invalid month";
                break;
        }
        return Smonth;
    }
}
