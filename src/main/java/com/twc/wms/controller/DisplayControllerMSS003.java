/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.QCUSTOMSDao;
import com.twc.wms.entity.QCUSTOMS;
import com.twc.wms.dao.UserDao;
import com.twc.wms.entity.UserAuth;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class DisplayControllerMSS003 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/displayMSS003.jsp";

    public DisplayControllerMSS003() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "MSS003");
        String forward = "";

        String userid = request.getParameter("uid");
        String dept = request.getParameter("dept");

        UserDao dao = new UserDao();
        UserAuth ua = dao.findByUid(userid);

        if (userid == null) {
            ua.setDepartment("");
        }
//        if (dept != null) {
//            if (dept.equals("bus")) {
//                ua.setDepartment("��áԨ");
//            } else if (dept.equals("pur")) {
//                ua.setDepartment("�Ѵ����");
//            } else if (dept.equals("imp")) {
//                ua.setDepartment("import-export");
//            }
//        }

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "THAI CUSTOMS Information. Entry");
                if (ua.getDepartment().trim().equals("��áԨ")) {
                    request.setAttribute("sortBU", "selected");
                    request.setAttribute("sortPU", "hidden");
                    request.setAttribute("sortIM", "hidden");
                    request.setAttribute("disBU", "block");
                    request.setAttribute("disPU", "none");
                    request.setAttribute("disIM", "none");

                    dept = "bus";
                } else if (ua.getDepartment().trim().equals("�Ѵ����")) {
                    request.setAttribute("sortBU", "hidden");
                    request.setAttribute("sortPU", "selected");
                    request.setAttribute("sortIM", "hidden");
                    request.setAttribute("disBU", "none");
                    request.setAttribute("disPU", "block");
                    request.setAttribute("disIM", "none");

                    dept = "pur";
                } else if (ua.getDepartment().trim().equals("import-export")) {
                    request.setAttribute("sortBU", "hidden");
                    request.setAttribute("sortPU", "hidden");
                    request.setAttribute("sortIM", "selected");
                    request.setAttribute("disBU", "none");
                    request.setAttribute("disPU", "none");
                    request.setAttribute("disIM", "block");

                    dept = "imp";
                } else {
                    request.setAttribute("sortBU", "selected");
                    request.setAttribute("sortPU", "");
                    request.setAttribute("sortIM", "");
                    request.setAttribute("disBU", "block");
                    request.setAttribute("disPU", "none");
                    request.setAttribute("disIM", "none");

                    dept = "etc";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.setAttribute("dept", dept);
//    ********
        QCUSTOMSDao dao1 = new QCUSTOMSDao();
        List<QCUSTOMS> pList = dao1.findAll();

        Date dNow = new Date();

        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

        String year = ft.format(dNow).split("-")[0];
//        year = Integer.toString(Integer.parseInt(year) + 543);

        String nd = year + "-" + ft.format(dNow).split("-")[1] + "-" + ft.format(dNow).split("-")[2];

        request.setAttribute("sysDate", nd);

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("QCUSTOMSList", pList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
