/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QCUSTOMSDao;
import com.twc.wms.dao.MSSMASMDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.MSSMASM;
import com.twc.wms.entity.QCUSTOMS;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class EditControllerMSS003 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/editMSS003.jsp";
    private static final String PAGE_VIEW2 = "../views/editMSS003Pur.jsp";
    private static final String PAGE_VIEW3 = "../views/editMSS003Imp.jsp";

    public EditControllerMSS003() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "MSS003/E");
        String forward = "";
        String matGroup = request.getParameter("mat");
        String dept = request.getParameter("dept");

        String uidx = request.getParameter("uidx");
        request.setAttribute("uidx", uidx);

        QCUSTOMSDao dao5 = new QCUSTOMSDao();
        QCUSTOMS cus = dao5.findByCod(matGroup);

        try {
            String action = request.getParameter("action");
            if (action == null) {
                if (dept.equals("bus")) {
                    forward = PAGE_VIEW;
                } else if (dept.equals("pur")) {
                    forward = PAGE_VIEW2;
                } else if (dept.equals("imp")) {
                    forward = PAGE_VIEW3;
                }
                request.setAttribute("PROGRAMDESC", "THAI CUSTOMS Information. Entry");
                request.setAttribute("matGroup", matGroup);
                request.setAttribute("dog", cus.getDescE());
                request.setAttribute("composit", cus.getSpec());
                request.setAttribute("desc", cus.getDesc());
                request.setAttribute("width", cus.getWidth());
                request.setAttribute("unit", cus.getUnit());
                request.setAttribute("price", cus.getPrice());
                request.setAttribute("brand", cus.getBrand());
                request.setAttribute("comp", cus.getComp());
                request.setAttribute("descT", cus.getDescT());
                request.setAttribute("hsc", cus.getHSC());
                request.setAttribute("taf", cus.getTaf());
                request.setAttribute("refHS", cus.getRefHS());
                request.setAttribute("ref", cus.getRef());
                request.setAttribute("country", cus.getCountry());
                request.setAttribute("unitC", cus.getUnitC());
                request.setAttribute("issDate", cus.getIssdt());
                request.setAttribute("nod", cus.getNoday());
                request.setAttribute("expDate", cus.getExpdt());
                request.setAttribute("warnDate", cus.getWandt());

                request.setAttribute("dept", dept);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        MSSMASMDao dao1 = new MSSMASMDao();
        List<MSSMASM> descList = dao1.selectDesc();

        MSSMASMDao dao2 = new MSSMASMDao();
        List<MSSMASM> priceList = dao2.selectPrice();

        MSSMASMDao dao3 = new MSSMASMDao();
        List<MSSMASM> unitList = dao3.selectUnit();

        MSSMASMDao dao4 = new MSSMASMDao();
        List<MSSMASM> specList = dao4.selectSpec();

        RequestDispatcher view = request.getRequestDispatcher(forward);
        request.setAttribute("descList", descList);
        request.setAttribute("priceList", priceList);
        request.setAttribute("unitList", unitList);
        request.setAttribute("specList", specList);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String matGroup = request.getParameter("matGroup");
        String dog = request.getParameter("dog");
        String composit = request.getParameter("composit");
        String desc = request.getParameter("desc");
        String width = request.getParameter("width");
        String unit = request.getParameter("unit");
        String price = request.getParameter("price");
        String brand = request.getParameter("brand");

        String comp = request.getParameter("comp");
        String descT = request.getParameter("descT");

        String hsc = request.getParameter("hsc");
        String taf = request.getParameter("taf");
        String refHS = request.getParameter("refHS");
        String ref = request.getParameter("ref");
        String country = request.getParameter("country");
        String unitC = request.getParameter("unitC");
        String issDate = request.getParameter("issDate");
        String nod = request.getParameter("nod");
        String expDate = request.getParameter("expDate");
        String warnDate = request.getParameter("warnDate");

        if (price.equals("")) {
            price = "0";
        }
        if (ref.equals("")) {
            ref = "0";
        }
        if (refHS.equals("")) {
            refHS = "0";
        }
        if (nod.equals("")) {
            nod = "0";
        }

        request.setAttribute("PROGRAMNAME", "MSS003/E");
        request.setAttribute("PROGRAMDESC", "THAI CUSTOMS Information. Entry");

        String userid = request.getParameter("userid");
        String dept = request.getParameter("dept");

        String forward = "";

        request.setAttribute("dept", dept);

        String sendMessage = "";
//
//        System.out.println(matGroup);
//        System.out.println(dog);
//        System.out.println(composit);
//        System.out.println(desc);
//        System.out.println(width);
//        System.out.println(unit);
//        System.out.println(price);
//        System.out.println(brand);
//        System.out.println("--------------------");
//        System.out.println(comp);
//        System.out.println(descT);
//        System.out.println("--------------------");
//        System.out.println(hsc);
//        System.out.println(taf);
//        System.out.println(refHS);
//        System.out.println(ref);
//        System.out.println(country);
//        System.out.println(unitC);
//        System.out.println(issDate);
//        System.out.println(nod);
//        System.out.println(expDate);
//        System.out.println(warnDate);
//        System.out.println("--------------------");
//        System.out.println(userid);
//        System.out.println(dept);
//        System.out.println("********************");
//
        request.setAttribute("matGroup", matGroup);
        request.setAttribute("dog", dog);
        request.setAttribute("composit", composit);
        request.setAttribute("desc", desc);
        request.setAttribute("width", width);
        request.setAttribute("unit", unit);
        request.setAttribute("price", price);
        request.setAttribute("brand", brand);

        request.setAttribute("comp", comp);
        request.setAttribute("descT", descT);

        request.setAttribute("hsc", hsc);
        request.setAttribute("taf", taf);
        request.setAttribute("refHS", refHS);
        request.setAttribute("ref", ref);
        request.setAttribute("country", country);
        request.setAttribute("unitC", unitC);
        request.setAttribute("issDate", issDate);
        request.setAttribute("nod", nod);
        request.setAttribute("expDate", expDate);
        request.setAttribute("warnDate", warnDate);

        request.setAttribute("dept", dept);

        String uidx = request.getParameter("uidx");
        request.setAttribute("uidx", uidx);

        if (new QCUSTOMSDao().edit(matGroup, dog, composit, desc, width, unit, price, brand, comp, descT, hsc, taf, refHS, ref, country, unitC, issDate, nod, expDate, warnDate, userid, dept)) {
            response.setHeader("Refresh", "0;/TABLE2/MSS003/display?uid=" + uidx);
        } else {
            sendMessage = "<script type=\"text/javascript\">\n"
                    + "            var show2 = function () {\n"
                    + "                $('#myModal2').modal('show');\n"
                    + "            };\n"
                    + "\n"
                    + "            window.setTimeout(show2, 0);\n"
                    + "\n"
                    + "        </script>";
            if (dept.equals("bus")) {
                forward = PAGE_VIEW;
            } else if (dept.equals("pur")) {
                forward = PAGE_VIEW2;
            } else if (dept.equals("imp")) {
                forward = PAGE_VIEW3;
            }
            request.setAttribute("sendMessage", sendMessage);

            RequestDispatcher view = request.getRequestDispatcher(forward);
            view.forward(request, response);
        }
    }
}
