/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.FileOutputStream;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.twc.wms.dao.QCUSTOMSDao;
import com.twc.wms.entity.QCUSTOMS;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import org.apache.poi.xssf.usermodel.XSSFSheet;

/**
 *
 * @author wien
 */
public class PrintControllerMSS003 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/printMSS003.jsp";
    private static final String FILE_PATH = "/report/";
    private static final int BUFSIZE = 4096;

    public PrintControllerMSS003() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "MSS003/P");
        String forward = "";
        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "THAI CUSTOMS Information. Entry");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//    ********

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String cnt = request.getParameter("paraVal");
        String dept = request.getParameter("dept");
        int cnti = 0;
        cnti = Integer.parseInt(cnt);
        List<QCUSTOMS> QCUSTOMSList = new ArrayList<QCUSTOMS>();

        if (cnti <= 500) {
            String[] matG = request.getParameterValues("printCk-" + dept);

            if (matG != null) {
                for (int i = 0; i < matG.length; i++) {
                    QCUSTOMSDao dao = new QCUSTOMSDao();
                    QCUSTOMS p = dao.findByCod(matG[i]);
                    QCUSTOMSList.add(p);
                }
            }
        } else {
            QCUSTOMSList = new QCUSTOMSDao().findAllPrint();
        }

        // Blank workbook 
        XSSFWorkbook workbook = new XSSFWorkbook();

        // Create a blank sheet 
        XSSFSheet sheet = workbook.createSheet("MSS003");

        // This data needs to be written (Object[]) 
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
        data.put("1", new Object[]{"H.S. Code", "Tariff Database", "Material Master", "Name of Material",
            "COMPANY", "Description of Goods (ENG)", "Description of Goods (THAI)", "Specifications",
            "Width/Size", "UNIT", "UNIT CUSTOMS", "Country of Origin", "PRICE",
            "#REF NO. COST ASEAN", "Expiration Date", "Cost Asean Re#H.S. CODE"});

        int rowNum = 2;
        for (QCUSTOMS cus : QCUSTOMSList) {
            data.put(Integer.toString(rowNum), new Object[]{cus.getHSC(), cus.getTaf(), cus.getMatGroup(),
                cus.getDesc(), cus.getComp(), cus.getDescE(), cus.getDescT(), cus.getSpec(), cus.getWidth(),
                cus.getUnit(), cus.getUnitC(), cus.getCountry(), cus.getPrice(), cus.getRef(), cus.getExpdt(), cus.getRefHS()});
            rowNum++;
        }

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.BLUE.getIndex());

        // Styling border and alignment of cell.  
        CellStyle style = workbook.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFont(headerFont);
        style.setBorderBottom(BorderStyle.MEDIUM);
        style.setBorderTop(BorderStyle.MEDIUM);
        style.setBorderRight(BorderStyle.MEDIUM);
        style.setBorderLeft(BorderStyle.MEDIUM);

        // Styling alignment of cell 2  
        CellStyle style2 = workbook.createCellStyle();
        style2.setAlignment(HorizontalAlignment.RIGHT);

        //Set Column Width
        sheet.setColumnWidth(0, 4000);
        sheet.setColumnWidth(1, 6000);
        sheet.setColumnWidth(2, 6000);
        sheet.setColumnWidth(3, 8000);
        sheet.setColumnWidth(4, 4000);
        sheet.setColumnWidth(5, 10000);
        sheet.setColumnWidth(6, 10000);
        sheet.setColumnWidth(7, 10000);
        sheet.setColumnWidth(8, 4000);
        sheet.setColumnWidth(9, 4000);
        sheet.setColumnWidth(10, 4000);
        sheet.setColumnWidth(11, 6000);
        sheet.setColumnWidth(12, 3000);
        sheet.setColumnWidth(13, 8000);
        sheet.setColumnWidth(14, 7000);
        sheet.setColumnWidth(15, 8000);

        // Iterate over data and write to sheet 
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset) {
            // this creates a new row in the sheet 
            Row row = sheet.createRow(rownum++);
            Object[] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr) {
                // this line creates a cell in the next column of that row 
                Cell cell = row.createCell(cellnum++);
                if (rownum == 1) {
                    cell.setCellStyle(style);
                } else {
                    if (cellnum == 13 || cellnum == 14 || cellnum == 15 || cellnum == 16) {
                        cell.setCellStyle(style2);
                    }
                }
                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }
            }
        }

        try {

            ServletContext servletContext = getServletContext();    // new by ji
            String realPath = servletContext.getRealPath(FILE_PATH);        // new by ji
            String saperate = realPath.contains(":") ? "\\" : "/";
            String path = realPath + saperate + "MSS003.xlsx";

            // this Writes the workbook
            FileOutputStream out = new FileOutputStream(path);
            workbook.write(out);
            out.close();

            File file = new File(path);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(path);

            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) file.length());
            String fileName = (new File(path)).getName();

            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");

            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(file));

            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }

            in.close();
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        response.setHeader("Refresh", "0;/TABLE2/MSS003/display");
    }

}
