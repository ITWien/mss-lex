/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QCUSTOMSDao;
import com.twc.wms.dao.MSSMASMDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.MSSMASM;
import com.twc.wms.entity.QCUSTOMS;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class DeleteControllerMSS003 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;
    private static final String PAGE_VIEW = "../views/deleteMSS003.jsp";

    public DeleteControllerMSS003() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("PROGRAMNAME", "MSS003/D");
        String forward = "";
        String matGroup = request.getParameter("mat");
        String dept = request.getParameter("dept");

        String uidx = request.getParameter("uidx");
        request.setAttribute("uidx", uidx);

        QCUSTOMSDao dao5 = new QCUSTOMSDao();
        QCUSTOMS cus = dao5.findByCod(matGroup);

        try {
            String action = request.getParameter("action");
            if (action == null) {
                forward = PAGE_VIEW;
                request.setAttribute("PROGRAMDESC", "THAI CUSTOMS Information. Entry");
                request.setAttribute("matGroup", matGroup);
                request.setAttribute("desc", cus.getDesc());
                request.setAttribute("price", cus.getPrice());
                request.setAttribute("unit", cus.getUnit());
                request.setAttribute("comp", cus.getComp());
                request.setAttribute("descE", cus.getDescE());
                request.setAttribute("descT", cus.getDescT());
                request.setAttribute("spec", cus.getSpec());
                request.setAttribute("width", cus.getWidth());
                request.setAttribute("hsc", cus.getHSC());
                request.setAttribute("taf", cus.getTaf());
                request.setAttribute("country", cus.getCountry());
                request.setAttribute("ref", cus.getRef());
                request.setAttribute("expDate", cus.getExpDate());
                request.setAttribute("refHS", cus.getRefHS());

                request.setAttribute("dept", dept);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String matGroup = request.getParameter("matGroup");
        String dept = request.getParameter("dept");

        String uidx = request.getParameter("uidx");
        request.setAttribute("uidx", uidx);

        request.setAttribute("PROGRAMNAME", "MSS003/D");
        request.setAttribute("PROGRAMDESC", "THAI CUSTOMS Information. Entry");

        QCUSTOMSDao dao = new QCUSTOMSDao();

        dao.delete(matGroup.trim());
        response.setHeader("Refresh", "0;/TABLE2/MSS003/display?uid=" + uidx);
    }
}
