/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.MSSSIDETAILDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.dao.MSSSIHEADDao;
import com.twc.wms.entity.MSSSIDETAIL;
import com.twc.wms.entity.MSSSIHEAD;
import java.util.List;

/**
 *
 * @author wien
 */
public class DeleteControllerMSS214 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public DeleteControllerMSS214() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String qno = request.getParameter("qno");

        new MSSSIHEADDao().delete(qno);
        new MSSSIDETAILDao().delete(qno);

        response.setHeader("Refresh", "0;/TABLE2/MSS214/display");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
