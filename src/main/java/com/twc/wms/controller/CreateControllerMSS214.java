/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.controller;

import com.twc.wms.dao.QCUSTOMSDao;
import com.twc.wms.dao.MSSMASMDao;
import com.twc.wms.dao.MSSSIDETAILDao;
import com.twc.wms.dao.MSSSIHEADDao;
import com.twc.wms.dao.UserDao;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.twc.wms.entity.MSSMASM;
import com.twc.wms.entity.QCUSTOMS;
import com.twc.wms.entity.UserAuth;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wien
 */
public class CreateControllerMSS214 extends HttpServlet {

    private static final long serialVersionUID = 4707490878358448870L;

    public CreateControllerMSS214() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");

        String[] inv = request.getParameterValues("selectCheck");
        String uid = request.getParameter("userid");

        boolean add = false;

        if (inv != null) {
            add = new MSSSIDETAILDao().createDet(inv, uid);
        }

        if (add) {
            String maxQno = new MSSSIHEADDao().findMaxQno();
            response.setHeader("Refresh", "0;/TABLE2/MSS214/edit?qno=" + maxQno);
        } else {
            response.setHeader("Refresh", "0;/TABLE2/MSS214/display");
        }
    }
}
