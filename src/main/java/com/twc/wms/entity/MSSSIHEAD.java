/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class MSSSIHEAD {

    private String MSSSIHQNO;
    private String MSSSIHLOADDT;
    private String MSSSIHCUR;
    private String MSSSIHREMARK;
    private String MSSSIHUSER;

    public MSSSIHEAD() {

    }

    public String getMSSSIHQNO() {
        return MSSSIHQNO;
    }

    public void setMSSSIHQNO(String MSSSIHQNO) {
        this.MSSSIHQNO = MSSSIHQNO;
    }

    public String getMSSSIHLOADDT() {
        return MSSSIHLOADDT;
    }

    public void setMSSSIHLOADDT(String MSSSIHLOADDT) {
        this.MSSSIHLOADDT = MSSSIHLOADDT;
    }

    public String getMSSSIHCUR() {
        return MSSSIHCUR;
    }

    public void setMSSSIHCUR(String MSSSIHCUR) {
        this.MSSSIHCUR = MSSSIHCUR;
    }

    public String getMSSSIHREMARK() {
        return MSSSIHREMARK;
    }

    public void setMSSSIHREMARK(String MSSSIHREMARK) {
        this.MSSSIHREMARK = MSSSIHREMARK;
    }

    public String getMSSSIHUSER() {
        return MSSSIHUSER;
    }

    public void setMSSSIHUSER(String MSSSIHUSER) {
        this.MSSSIHUSER = MSSSIHUSER;
    }

}
