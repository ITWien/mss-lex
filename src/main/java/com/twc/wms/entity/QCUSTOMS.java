/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class QCUSTOMS {

    private String matGroup;
    private String desc;
    private String spec;
    private String price;
    private String editDate;
    private String createDate;
    private String comp;
    private String descE;
    private String descT;
    private String width;
    private String country;
    private String ref;
    private String expDate;
    private String refHS;
    private String taf;
    private String unit;
    private String HSC;
    private String edtpur;
    private String userpur;
    private String edtimp;
    private String userimp;
    private String brand;
    private String user;
    private String edt;
    private String unitC;
    private String issdt;
    private String noday;
    private String expdt;
    private String wandt;
    private String sysdt;

    public QCUSTOMS() {

    }

    public QCUSTOMS(String matGroup, String desc, String price, String unit,
            String comp, String descE, String descT, String spec, String width,
            String HSC, String taf, String country, String ref, String expDate, String refHS) {

        this.matGroup = matGroup;
        this.desc = desc;
        this.spec = spec;
        this.price = price;
        this.comp = comp;
        this.descE = descE;
        this.descT = descT;
        this.width = width;
        this.country = country;
        this.ref = ref;
        this.expDate = expDate;
        this.refHS = refHS;
        this.taf = taf;
        this.unit = unit;
        this.HSC = HSC;

    }

    public QCUSTOMS(String matGroup, String desc, String spec, String price, String editDate, String createDate, String comp, String descE, String descT, String width, String country, String ref, String expDate, String refHS, String taf, String unit, String HSC, String edtpur, String userpur, String edtimp, String userimp, String brand, String user, String edt, String unitC, String issdt, String noday, String expdt, String wandt, String sysdt) {
        this.matGroup = matGroup;
        this.desc = desc;
        this.spec = spec;
        this.price = price;
        this.editDate = editDate;
        this.createDate = createDate;
        this.comp = comp;
        this.descE = descE;
        this.descT = descT;
        this.width = width;
        this.country = country;
        this.ref = ref;
        this.expDate = expDate;
        this.refHS = refHS;
        this.taf = taf;
        this.unit = unit;
        this.HSC = HSC;
        this.edtpur = edtpur;
        this.userpur = userpur;
        this.edtimp = edtimp;
        this.userimp = userimp;
        this.brand = brand;
        this.user = user;
        this.edt = edt;
        this.unitC = unitC;
        this.issdt = issdt;
        this.noday = noday;
        this.expdt = expdt;
        this.wandt = wandt;
        this.sysdt = sysdt;
    }

    public String getSysdt() {
        return sysdt;
    }

    public void setSysdt(String sysdt) {
        this.sysdt = sysdt;
    }

    public String getIssdt() {
        return issdt;
    }

    public void setIssdt(String issdt) {
        this.issdt = issdt;
    }

    public String getNoday() {
        return noday;
    }

    public void setNoday(String noday) {
        this.noday = noday;
    }

    public String getExpdt() {
        return expdt;
    }

    public void setExpdt(String expdt) {
        this.expdt = expdt;
    }

    public String getWandt() {
        return wandt;
    }

    public void setWandt(String wandt) {
        this.wandt = wandt;
    }

    public String getUnitC() {
        return unitC;
    }

    public void setUnitC(String unitC) {
        this.unitC = unitC;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEdt() {
        return edt;
    }

    public void setEdt(String edt) {
        this.edt = edt;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setEdtpur(String edtpur) {
        this.edtpur = edtpur;
    }

    public void setUserpur(String userpur) {
        this.userpur = userpur;
    }

    public void setEdtimp(String edtimp) {
        this.edtimp = edtimp;
    }

    public void setUserimp(String userimp) {
        this.userimp = userimp;
    }

    public String getEdtpur() {
        return edtpur;
    }

    public String getUserpur() {
        return userpur;
    }

    public String getEdtimp() {
        return edtimp;
    }

    public String getUserimp() {
        return userimp;
    }

    public String getHSC() {

        return HSC;

    }

    public void setHSC(String HSC) {

        this.HSC = HSC;

    }

    public String getUnit() {

        return unit;

    }

    public void setUnit(String unit) {

        this.unit = unit;

    }

    public String getTaf() {

        return taf;

    }

    public void setTaf(String taf) {

        this.taf = taf;

    }

    public String getRefHS() {

        return refHS;

    }

    public void setRefHS(String refHS) {

        this.refHS = refHS;

    }

    public String getExpDate() {

        return expDate;

    }

    public void setExpDate(String expDate) {

        this.expDate = expDate;

    }

    public String getRef() {

        return ref;

    }

    public void setRef(String ref) {

        this.ref = ref;

    }

    public String getCountry() {

        return country;

    }

    public void setCountry(String country) {

        this.country = country;

    }

    public String getWidth() {

        return width;

    }

    public void setWidth(String width) {

        this.width = width;

    }

    public String getDescT() {

        return descT;

    }

    public void setDescT(String descT) {

        this.descT = descT;

    }

    public String getDescE() {

        return descE;

    }

    public void setDescE(String descE) {

        this.descE = descE;

    }

    public String getComp() {

        return comp;

    }

    public void setComp(String comp) {

        this.comp = comp;

    }

    public String getMatGroup() {

        return matGroup;

    }

    public void setMatGroup(String matGroup) {

        this.matGroup = matGroup;

    }

    public String getDesc() {

        return desc;

    }

    public void setDesc(String desc) {

        this.desc = desc;

    }

    public String getSpec() {

        return spec;

    }

    public void setSpec(String spec) {

        this.spec = spec;

    }

    public String getPrice() {

        return price;

    }

    public void setPrice(String price) {

        this.price = price;

    }

    public String getEditDate() {

        return editDate;

    }

    public void setEditDate(String editDate) {

        this.editDate = editDate;

    }

    public String getCreateDate() {

        return createDate;

    }

    public void setCreateDate(String createDate) {

        this.createDate = createDate;

    }

}
