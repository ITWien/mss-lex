/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class MSSSIDETAIL {

    private String MSSSIDQNO;
    private String MSSSIDINVNO;
    private String MSSSIDDEPT;
    private String MSSSIDCIF;
    private String MSSSIDINS;
    private String MSSSIDFRE;
    private String MSSSIDFOB;
    private String MSSSIDCTN;
    private String MSSSIDMETER;
    private String MSSSIDPC;
    private String MSSSIDSET;
    private String MSSSIDCONE;
    private String MSSSIDKGS;
    private String MSSSIDNW;
    private String MSSSIDGW;
    private String MSSSIDM3;

    public MSSSIDETAIL() {

    }

    public String getMSSSIDQNO() {
        return MSSSIDQNO;
    }

    public void setMSSSIDQNO(String MSSSIDQNO) {
        this.MSSSIDQNO = MSSSIDQNO;
    }

    public String getMSSSIDINVNO() {
        return MSSSIDINVNO;
    }

    public void setMSSSIDINVNO(String MSSSIDINVNO) {
        this.MSSSIDINVNO = MSSSIDINVNO;
    }

    public String getMSSSIDDEPT() {
        return MSSSIDDEPT;
    }

    public void setMSSSIDDEPT(String MSSSIDDEPT) {
        this.MSSSIDDEPT = MSSSIDDEPT;
    }

    public String getMSSSIDCIF() {
        return MSSSIDCIF;
    }

    public void setMSSSIDCIF(String MSSSIDCIF) {
        this.MSSSIDCIF = MSSSIDCIF;
    }

    public String getMSSSIDINS() {
        return MSSSIDINS;
    }

    public void setMSSSIDINS(String MSSSIDINS) {
        this.MSSSIDINS = MSSSIDINS;
    }

    public String getMSSSIDFRE() {
        return MSSSIDFRE;
    }

    public void setMSSSIDFRE(String MSSSIDFRE) {
        this.MSSSIDFRE = MSSSIDFRE;
    }

    public String getMSSSIDFOB() {
        return MSSSIDFOB;
    }

    public void setMSSSIDFOB(String MSSSIDFOB) {
        this.MSSSIDFOB = MSSSIDFOB;
    }

    public String getMSSSIDCTN() {
        return MSSSIDCTN;
    }

    public void setMSSSIDCTN(String MSSSIDCTN) {
        this.MSSSIDCTN = MSSSIDCTN;
    }

    public String getMSSSIDMETER() {
        return MSSSIDMETER;
    }

    public void setMSSSIDMETER(String MSSSIDMETER) {
        this.MSSSIDMETER = MSSSIDMETER;
    }

    public String getMSSSIDPC() {
        return MSSSIDPC;
    }

    public void setMSSSIDPC(String MSSSIDPC) {
        this.MSSSIDPC = MSSSIDPC;
    }

    public String getMSSSIDSET() {
        return MSSSIDSET;
    }

    public void setMSSSIDSET(String MSSSIDSET) {
        this.MSSSIDSET = MSSSIDSET;
    }

    public String getMSSSIDCONE() {
        return MSSSIDCONE;
    }

    public void setMSSSIDCONE(String MSSSIDCONE) {
        this.MSSSIDCONE = MSSSIDCONE;
    }

    public String getMSSSIDKGS() {
        return MSSSIDKGS;
    }

    public void setMSSSIDKGS(String MSSSIDKGS) {
        this.MSSSIDKGS = MSSSIDKGS;
    }

    public String getMSSSIDNW() {
        return MSSSIDNW;
    }

    public void setMSSSIDNW(String MSSSIDNW) {
        this.MSSSIDNW = MSSSIDNW;
    }

    public String getMSSSIDGW() {
        return MSSSIDGW;
    }

    public void setMSSSIDGW(String MSSSIDGW) {
        this.MSSSIDGW = MSSSIDGW;
    }

    public String getMSSSIDM3() {
        return MSSSIDM3;
    }

    public void setMSSSIDM3(String MSSSIDM3) {
        this.MSSSIDM3 = MSSSIDM3;
    }

}
