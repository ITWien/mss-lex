/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class MSSMASH {

    private String code;
    private String name;
    private String addr1;
    private String addr2;

    public MSSMASH() {

    }

    public MSSMASH(String code, String name, String addr1, String addr2) {

        this.code = code;
        this.name = name;
        this.addr1 = addr1;
        this.addr2 = addr2;

    }

    public String getCode() {

        return code;

    }

    public void setCode(String code) {

        this.code = code;

    }

    public String getName() {

        return name;

    }

    public void setName(String name) {

        this.name = name;

    }

    public String getAddr1() {

        return addr1;

    }

    public void setAddr1(String addr1) {

        this.addr1 = addr1;

    }

    public String getAddr2() {

        return addr2;

    }

    public void setAddr2(String addr2) {

        this.addr2 = addr2;

    }

}
