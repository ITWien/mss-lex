/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.twc.wms.entity;

/**
 *
 * @author nutthawoot.noo
 */
public class MSSMASM {

    private String matGroup;
    private String desc;
    private String price;
    private String unit;
    private String spec;

    public MSSMASM() {

    }

    public MSSMASM(String matGroup, String desc, String price, String unit, String spec) {

        this.matGroup = matGroup;
        this.desc = desc;
        this.price = price;
        this.unit = unit;
        this.spec = spec;

    }

    public String getMatGroup() {

        return matGroup;

    }

    public void setMatGroup(String matGroup) {

        this.matGroup = matGroup;

    }

    public String getDesc() {

        return desc;

    }

    public void setDesc(String desc) {

        this.desc = desc;

    }

    public String getPrice() {

        return price;

    }

    public void setPrice(String price) {

        this.price = price;

    }

    public String getUnit() {

        return unit;

    }

    public void setUnit(String unit) {

        this.unit = unit;

    }

    public String getSpec() {

        return spec;

    }

    public void setSpec(String spec) {

        this.spec = spec;

    }

}
