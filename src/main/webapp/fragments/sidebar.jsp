<div id="sidebar-wrapper">  
    <div class="navbar-default sidebar" role="navigation" style="margin: 0">
        <ul class="nav navbar-default" id="side-menu">       
            <li>
                <h4 class="head-menu">
                    <table  style="width:100%;">
                        <tr>
                            <td align="center">Warehouse Management</td>
                        </tr>
                    </table>
                </h4>
            </li>
            <li>
                <a href="#"><i class="fa fa-institution fa-fw"></i> Stock RM <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="/WMS/WMS100/RE"><i class="fa fa-cubes fa-fw"></i> RM Receipt</a></li>
                    <li><a href="/WMS2/WMS100/M"><i class="fa fa-cubes fa-fw"></i> MOLD Receipt</a></li>
                    <li><a href="/WMS/WMS001/DP"><i class="fa fa-sort-numeric-asc fa-fw"></i> Display on hand</a></li>
                    <li><a href="/WMS/WMS001Q/EQTY"><i class="fa fa-sort-numeric-asc fa-fw"></i> Change QTY</a></li>
                    <li><a href="/WMS/WMS007/SPRM"><i class="fa fa-cut fa-fw"></i> Separate RM</a></li>
                    <li><a href="/WMS2/WMS110/MOVE"><i class="fa fa-share-square-o fa-fw"></i> Location Movement</a></li>
                    <li><a href="/WMS/WMS600/PRINT"><i class="fa fa-print fa-fw"></i> QR Code Print</a></li>
                    <li><a href="/WMS2/WMS930/DP"><i class="fa fa-bar-chart fa-fw"></i> Summary Stock Checking</a></li>
                    <li><a href="/WMS2/WMS920/DP"><i class="fa fa-files-o fa-fw"></i> Onhand Comparison (SAP:WMS)</a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="fa fa-cogs fa-fw"></i> Configuration <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="/TABLE/WMS002/display"><i class="fa fa-cog fa-fw"></i> Warehouse Master</a></li>
                    <li><a href="/TABLE/WMS003/display"><i class="fa fa-cog fa-fw"></i> Warehouse Structure</a></li>
                    <li><a href="/TABLE/WMS006/display"><i class="fa fa-cog fa-fw"></i> RM Reason</a></li>
                    <li><a href="/TABLE/WMS165/display"><i class="fa fa-cog fa-fw"></i> Number Series</a></li>
                    <li><a href="/TABLE2/WMS004/display"><i class="fa fa-cog fa-fw"></i> Material Control Master</a></li>
                    <li><a href="/TABLE2/WMS010/display"><i class="fa fa-cog fa-fw"></i> Destination Master</a></li>
                    <li><a href="/TABLE2/WMS009/login"><i class="fa fa-cog fa-fw"></i> User Authorization</a></li>
                    <li><a href="/TABLE2/WMS011/display"><i class="fa fa-cog fa-fw"></i> Program Master</a></li>
                    <li><a href="/TABLE2/WMS070/display"><i class="fa fa-cog fa-fw"></i> Material History Transaction</a></li>
                    <li><a href="/TABLE2/WMS008/display"><i class="fa fa-cog fa-fw"></i> Rack Master</a></li>
                </ul>
            </li>
        </ul>    
    </div>
</div>