<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>MSS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <!--<script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>-->
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        ${showPDF}
        <script>
            function Customer() {
                var x = document.getElementById("customer");
                x.value = x.value.toUpperCase();

                var cus = document.getElementById("customer");
                var name = "";
                var ad1 = "";
                var ad2 = "";
            <c:forEach items="${cusList}" var="p" varStatus="i">
                if ("${p.code}" === cus.value) {
                    name = "${p.name}";
                    ad1 = "${p.addr1}";
                    ad2 = "${p.addr2}";

                    $('#submitBtn').removeClass('disabled');
                }
            </c:forEach>
                document.getElementById("cusname").innerHTML = name;
                document.getElementById("comname").value = name;
                document.getElementById("comaddr1").value = ad1;
                document.getElementById("comaddr2").value = ad2;
            }
        </script>
        <style>
            input[type=text], input[type=password], input[type=number], input[type=date], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            /*            button[type=submit] {
                            width: 100%;
                            background-color: #2bd14a;
                            color: white;
                            padding: 5px 5px;
                            margin: 8px 0;
                            border: none;
                            border-radius: 4px;
                            cursor: pointer;
                        }

                        button[type=submit]:hover {
                            background-color: #45a049;
                        }*/

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            #addContainerInput{
                cursor: pointer;
            }
        </style>
        <script>
            function toUpper(ele) {
                $(ele).val($(ele).val().toString().toUpperCase());
            }

            $(document).ready(function () {
                $('#addContainerInput').click(function () {
                    $('#conList').append('<input type="text" name="containerInput" class="containerInput" onkeyup="toUpper(this)">');
                    $('.containerInput').focus();
                });

                $('#submitBtn').click(function () {
                    $('.containerInput').filter(function () {
                        return $(this).val().toString().trim() === "";
                    }).remove();
                });
            });
        </script>
    </head>
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->

                <%-- PART 3 --%>
                <form action="../resources/manual/MSS217.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <form action="print" method="post" name="frm">
                    <table frame="box" width="100%" bordercolor="#e0e0e0">
                        <br>
                        <tr>
                            <td width="3%" align="left">
                            <td width="15%" align="left"><b style=" color: #00399b; font-size: 19px;">Customer : </b></td>
                            <td width="20%" align="left"><input type="text" id="customer" name="customer" onkeyup="return Customer();" ></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left"><b id="cusname"></b><input id="comname" name="comname" type="text" style=" display: none">
                                <input id="comaddr1" name="comaddr1" type="text" style=" display: none">
                                <input id="comaddr2" name="comaddr2" type="text" style=" display: none">
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td width="3%" align="left">
                            <td width="15%" align="left" style=" vertical-align: top;"><b style=" color: #00399b; font-size: 19px;">Container : </b></td>
                            <td width="20%" align="left" id="conList">
                                <input type="text" name="containerInput" class="containerInput" onkeyup="toUpper(this)">
                            </td>
                            <td width="2%" align="left" style=" vertical-align: bottom; text-align: center;">
                                <a id="addContainerInput"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i></a>
                            </td>
                            <td width="20%" align="left">
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="10%" align="left">
                        </tr>
                    </table>
                    <br>
                    <center>
                        <button id="submitBtn" style="width: 200px;" type="submit" class="btn btn-success disabled"><i class="fa fa-file-excel-o"></i> Print MSS217</button>
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>