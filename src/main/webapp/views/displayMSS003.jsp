<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>MSS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>  
        <!--<script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>-->  
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "lengthMenu": [[10, 25, 50, 100, 300, 500, -1], [10, 25, 50, 100, 300, 500, "All"]],
                    "bSortClasses": false,
                    columnDefs: [
                        {orderable: false, targets: [10]},
                        {"width": "12%", "targets": 10}
                    ]
                });
                // Setup - add a text input to each footer cell
                $('#showTable tfoot th').not(":eq(10)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });
                // DataTable
                var table = $('#showTable').DataTable();
                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#showTable1').DataTable({
                    "lengthMenu": [[10, 25, 50, 100, 300, 500, -1], [10, 25, 50, 100, 300, 500, "All"]],
                    "bSortClasses": false,
                    columnDefs: [
                        {orderable: false, targets: [5]},
                        {"width": "12%", "targets": 4}
                    ]
                });
                // Setup - add a text input to each footer cell
                $('#showTable1 tfoot th').not(":eq(5)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" />');
                });
                // DataTable
                var table = $('#showTable1').DataTable();
                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $('#showTable2').DataTable({
                    "lengthMenu": [[10, 25, 50, 100, 300, 500, -1], [10, 25, 50, 100, 300, 500, "All"]],
                    "bSortClasses": false,
                    columnDefs: [
                        {orderable: false, targets: [12]},
                        {orderable: false, targets: [9]},
                        {"width": "12%", "targets": 11}
                    ]
                });
                // Setup - add a text input to each footer cell
                $('#showTable2 tfoot th').not(":eq(12)").each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" id="' + title + '" />');
                });
                // DataTable
                var table = $('#showTable2').DataTable();
                // Apply the search
                table.columns().every(function () {
                    var that = this;
                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });
            });
        </script>
        <script>
            function sortOrder() {
                var sort = document.getElementById("sort");
                var bu = document.getElementById("business");
                var pu = document.getElementById("purchase");
                var im = document.getElementById("import");
                if (sort.value === "Business") {
                    bu.style.display = "block";
                    pu.style.display = "none";
                    im.style.display = "none";
                } else if (sort.value === "Purchase") {
                    bu.style.display = "none";
                    pu.style.display = "block";
                    im.style.display = "none";
                } else if (sort.value === "Import") {
                    bu.style.display = "none";
                    pu.style.display = "none";
                    im.style.display = "block";
                }

            }
        </script>
        <script>
            function checkAll(ele) {
                var checkboxes = document.getElementsByTagName('input');
                var cnt = 0;
                if (ele.checked) {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type === 'checkbox') {
                            if (checkboxes[i].name === 'printCk-${dept}') {
                                cnt++;
                                if (cnt <= 500) {
                                    checkboxes[i].checked = true;
                                }
                            }
                        }
                    }
                } else {
                    for (var i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].type === 'checkbox') {
                            if (checkboxes[i].name === 'printCk-${dept}') {
                                checkboxes[i].checked = false;
                                cnt++;
                            }
                        }
                    }
                }

                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].name === 'paraVal') {
                        checkboxes[i].value = cnt;
                    }
                }
            }
        </script>
        <style>
            input[type=text], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            button[type=submit] {
                background: transparent;
                border: none !important;
            }
        </style>
        <script>
            function WarnDate() {
                var wnd = document.getElementById('WND').value;
                var table = $('#showTable2').DataTable();

                if (wnd.toString().trim() === "") {
                    table.column(9).search("${sysDate}").draw();
                    document.getElementById('WND').value = '${sysDate}';
                } else {
                    table.column(9).search("").draw();
                    document.getElementById('WND').value = '';
                }
            }

            function AL(uid) {
                var input = document.getElementsByTagName('input');
                for (var i = 0; i < input.length; i++) {
                    if (input[i].id === 'userid') {
                        input[i].value = uid;
                    }
                }
            }
        </script>
    </head>    
    <body onload="AL(sessionStorage.getItem('uid'))">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <form action="../resources/manual/MSS003.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <div align="right">
                    <b>Sort Order : </b>
                    <select name="sort" id="sort" style="width: 15%;" onchange="return sortOrder()" readonly>
                        <option value="Business" ${sortBU}> 1 : Business</option>
                        <option value="Purchase" ${sortPU}> 2 : Purchase</option>
                        <option value="Import" ${sortIM}> 3 : Import</option>
                    </select>
                </div>
                <form action="print" method="post">
                    <input type="hidden" id="userid" name="userid">
                    <input type="hidden" name="paraVal">
                    <input type="hidden" name="dept" value="bus">
                    <div id="business" style="border: 2px solid #ccc; border-radius: 5px; padding: 20px; display: ${disBU};">
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th>Material Group</th>
                                    <th>Description of Goods</th>
                                    <th>Composition</th>
                                    <th>Description(SAP)</th>
                                    <th>Width/Size</th>
                                    <th>Unit</th>
                                    <th>Price</th>
                                    <th>Brand</th>
                                    <th>User</th>
                                    <th>Business Edit Date</th>
                                    <th>Option
                                        <!--<a href="create?dept=bus"><i class="glyphicon glyphicon-plus-sign" style="font-size:15px; padding-left: 10px"></i></a>-->
                                        <button type="submit" style=""><a><i class="fa fa-print" style="font-size:20px; padding-left: 10px"></i></a></button>
                                        <input type="checkbox" name="selectAll" onchange="checkAll(this)">
                                    </th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Material Group</th>
                                    <th>Description of Goods</th>
                                    <th>Composition</th>
                                    <th>Description(SAP)</th>
                                    <th>Width/Size</th>
                                    <th>Unit</th>
                                    <th>Price</th>
                                    <th>Brand</th>
                                    <th>User</th>
                                    <th>Business Edit Date</th>
                                    <td></td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${QCUSTOMSList}" var="p" varStatus="i">
                                    <tr>
                                        <td>${p.matGroup}</td>
                                        <td>${p.descE}</td>
                                        <td>${p.spec}</td>
                                        <td>${p.desc}</td>
                                        <td>${p.width}</td>
                                        <td>${p.unit}</td>
                                        <td style="text-align: right;">${p.price}</td>
                                        <td>${p.brand}</td>
                                        <td>${p.user}</td>
                                        <td>${p.edt}</td>
                                        <td>
                                            <a onclick="window.location.href = 'edit?mat=${p.matGroup}&dept=bus&uidx=' + sessionStorage.getItem('uid');" style="cursor: pointer;"><i class="glyphicon glyphicon-edit" style="font-size:15px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a onclick="window.location.href = 'delete?mat=${p.matGroup}&dept=bus&uidx=' + sessionStorage.getItem('uid');" style="cursor: pointer;"><i class="glyphicon glyphicon-trash" style="font-size:15px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" name="printCk-bus" value="${p.matGroup}">
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </form>
                <form action="print" method="post">
                    <input type="hidden" id="userid" name="userid">
                    <input type="hidden" name="paraVal">
                    <input type="hidden" name="dept" value="pur">
                    <div id="purchase" style="border: 2px solid #ccc; border-radius: 5px; padding: 20px; display: ${disPU};">
                        <table id="showTable1" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th>Material Group</th>
                                    <th>Company</th>
                                    <th>Description of Goods(THAI)</th>
                                    <th>User</th>
                                    <th>Purchase Edit Date</th>
                                    <th>Option
                                        <!--<a href="create?dept=pur"><i class="glyphicon glyphicon-plus-sign" style="font-size:15px; padding-left: 10px"></i></a>-->
                                        <button type="submit" style=""><a><i class="fa fa-print" style="font-size:20px; padding-left: 10px"></i></a></button>
                                        <input type="checkbox" name="selectAll" onchange="checkAll(this)">
                                    </th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Material Group</th>
                                    <th>Company</th>
                                    <th>Description of Goods(THAI)</th>
                                    <th>User</th>
                                    <th>Purchase Edit Date</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${QCUSTOMSList}" var="p" varStatus="i">
                                    <tr>
                                        <td>${p.matGroup}</td>
                                        <td>${p.comp}</td>
                                        <td>${p.descT}</td>
                                        <td>${p.userpur}</td>     
                                        <td>${p.edtpur}</td>
                                        <td>
                                            <a onclick="window.location.href = 'edit?mat=${p.matGroup}&dept=pur&uidx=' + sessionStorage.getItem('uid');" style="cursor: pointer;"><i class="glyphicon glyphicon-edit" style="font-size:15px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a onclick="window.location.href = 'delete?mat=${p.matGroup}&dept=pur&uidx=' + sessionStorage.getItem('uid');" style="cursor: pointer;"><i class="glyphicon glyphicon-trash" style="font-size:15px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" name="printCk-pur" value="${p.matGroup}">
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </form>
                <form action="print" method="post">
                    <input type="hidden" id="userid" name="userid">
                    <input type="hidden" name="paraVal">
                    <input type="hidden" name="dept" value="imp">
                    <div id="import" style="border: 2px solid #ccc; border-radius: 5px; padding: 20px; display: ${disIM};">
                        <table id="showTable2" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th>Material Group</th>
                                    <th>H.S. Code</th>
                                    <th>Cost Asean Ref # H.S. Code</th>
                                    <th>#REF NO. COST ASEAN</th>
                                    <th>Country of Origin</th>
                                    <th>Unit CUSTOMS</th>
                                    <th>Issue Date</th>
                                    <th>No of Days</th>
                                    <th>Exp Date</th>
                                    <th>Warning Date   
                                        <br>
                                        <span class="fa-stack fa-lg" onclick="WarnDate();">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fa fa-exclamation-triangle fa-stack-1x fa-inverse" style=" cursor: pointer; color: #d9a711;"></i>
                                        </span>
                                    </th>
                                    <th>User</th>
                                    <th>Import Edit Date</th>
                                    <th>Option
                                        <a onclick="window.location.href = 'create?dept=imp&uidx=' + sessionStorage.getItem('uid');" style="cursor: pointer;"><i class="glyphicon glyphicon-plus-sign" style="font-size:15px; padding-left: 10px"></i></a>
                                        <button type="submit" style=""><a><i class="fa fa-print" style="font-size:20px; padding-left: 10px"></i></a></button>
                                        <input type="checkbox" name="selectAll" onchange="checkAll(this)">
                                    </th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Material Group</th>
                                    <th>H.S. Code</th>
                                    <th>Cost Asean Ref # H.S. Code</th>
                                    <th>#REF NO. COST ASEAN</th>
                                    <th>Country of Origin</th>
                                    <th>Unit CUSTOMS</th>
                                    <th>Issue Date</th>
                                    <th>No of Days</th>
                                    <th>Exp Date</th>
                                    <th>WND</th>
                                    <th>User</th>
                                    <th>Import Edit Date</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <c:forEach items="${QCUSTOMSList}" var="x" varStatus="i">
                                    <tr id="${x.sysdt}" style="color: ${x.sysdt};">
                                        <td>${x.matGroup}</td>
                                        <td>${x.HSC}</td>
                                        <td>${x.refHS}</td>
                                        <td>${x.ref}</td>
                                        <td>${x.country}</td>
                                        <td>${x.unitC}</td>
                                        <td>${x.issdt}</td>
                                        <td>${x.noday}</td>
                                        <td>${x.expDate}</td>
                                        <td>${x.wandt}</td>
                                        <td>${x.userimp}</td>
                                        <td>${x.edtimp}</td>
                                        <td>
                                            <a onclick="window.location.href = 'edit?mat=${x.matGroup}&dept=imp&uidx=' + sessionStorage.getItem('uid');" style="cursor: pointer;"><i class="glyphicon glyphicon-edit" style="font-size:15px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a onclick="window.location.href = 'delete?mat=${x.matGroup}&dept=imp&uidx=' + sessionStorage.getItem('uid');" style="cursor: pointer;"><i class="glyphicon glyphicon-trash" style="font-size:15px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="checkbox" name="printCk-imp" value="${x.matGroup}">
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </form>
                <!--End Part 3-->
                <br>
                <!--</div>  end #wrapper-top--> 
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>