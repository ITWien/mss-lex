<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>CREATE MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            function Uppercase(x) {
                x.value = x.value.toUpperCase();
            }
        </script>
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            function validateForm(dept) {
                var mg = document.getElementById('matGroup').value;

                if (mg.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("matGroup").style.cssText = "border: 2px solid #ff9999";
                    document.getElementById("matGroup2").style.cssText = "border: 2px solid #ff9999";
                    document.getElementById("matGroup3").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("matGroup").style.cssText = "border: 1px solid #ccc";
                    document.getElementById("matGroup2").style.cssText = "border: 1px solid #ccc";
                    document.getElementById("matGroup3").style.cssText = "border: 1px solid #ccc";
                }

                if (dept === "imp") {
                    var refHS = document.getElementById('refHS').value;
                    var ref = document.getElementById('ref').value;

                    if (refHS.trim() === "" && ref.trim() === "") {
                        document.getElementById("refHS").style.cssText = "border: 1px solid #ccc";
                        document.getElementById("ref").style.cssText = "border: 1px solid #ccc";
                    } else {
                        if (refHS.trim() !== "" && ref.trim() !== "") {
                            document.getElementById("refHS").style.cssText = "border: 1px solid #ccc";
                            document.getElementById("ref").style.cssText = "border: 1px solid #ccc";
                        } else {
                            window.setTimeout(show1, 0);
                            document.getElementById("refHS").style.cssText = "border: 2px solid #ff9999";
                            document.getElementById("ref").style.cssText = "border: 2px solid #ff9999";
                            return false;
                        }
                    }
                }
            }
        </script>
        <script>
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 46 || charCode > 57))
                    return false;
                return true;
            }
        </script>
        <style>
            input[type=text], input[type=password], input[type=number], input[type=date], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
        <script>
            function mgx(val) {
                var input = document.getElementsByTagName('input');
                for (var i = 0; i < input.length; i++) {
                    if (input[i].name === 'matGroup') {
                        input[i].value = val;
                    }
                }
            }

            function IssCH() {
                var val = document.getElementById('issDate').value;

                var tt = val.toString().split("-")[1] + "/" + val.toString().split("-")[2] + "/" + val.toString().split("-")[0];

                var date = new Date(tt);
                var newdate = new Date(date);
                var newdate2 = new Date(date);

                var nod = document.getElementById('nod').value;
                if (nod.toString().trim() === "") {
                    nod = '0';
                }

                newdate.setDate(newdate.getDate() + parseInt(nod));
                newdate2.setDate(newdate2.getDate() + parseInt(nod) - 60);

                var dd = newdate.getDate();
                var mm = newdate.getMonth() + 1;
                var y = newdate.getFullYear();

                var dd2 = newdate2.getDate();
                var mm2 = newdate2.getMonth() + 1;
                var y2 = newdate2.getFullYear();

                var pm = '';
                var pd = '';

                var pm2 = '';
                var pd2 = '';

                if (mm < 10) {
                    pm = '0';
                }
                if (dd < 10) {
                    pd = '0';
                }

                if (mm2 < 10) {
                    pm2 = '0';
                }
                if (dd2 < 10) {
                    pd2 = '0';
                }

                var someFormattedDate = y + '-' + pm + mm + '-' + pd + dd;
                var someFormattedDate2 = y2 + '-' + pm2 + mm2 + '-' + pd2 + dd2;

                document.getElementById('expDate').value = someFormattedDate;
                document.getElementById('warnDate').value = someFormattedDate2;
            }
        </script>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <br>
                <form action="create" method="get" name="frm" onsubmit="return validateForm('${dept}')">
                    <input type="hidden" id="userid" name="userid">
                    <input type="hidden" id="dept" name="dept" value="${dept}">
                    <input type="hidden" id="uidx" name="uidx" value="${uidx}">
                    <table style="${busShow}" id="bus" frame="box" width="100%" bordercolor="#e0e0e0">
                        <tr>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8">CREATE MODE</th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h3>Business Information</h3></td>
                            <td width="30%" align="left">
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left">
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Material Group :</h4></td>
                            <td width="30%" align="left" style="padding-right: 130px;">
                                <input type="text" id="matGroup" name="matGroup" maxlength="20" onkeyup="Uppercase(this)" onchange="mgx(this.value);
                                        this.form.submit();" value="${matGroup}" list="matGroupList">
                                <datalist id="matGroupList">
                                    <c:forEach items="${matGroupList}" var="x" varStatus="i">
                                        <option value="${x.matGroup}"></option>
                                    </c:forEach>
                                </datalist>
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Description of Goods :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;">
                                <input type="text" id="dog" name="dog" maxlength="225" value="${dog}" >
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Composition :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;">
                                <input type="text" id="composit" name="composit" maxlength="225" value="${composit}">
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Description (SAP) :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;">
                                <input type="text" id="desc" name="desc" maxlength="225" value="${desc}" list="descList">
                                <datalist id="descList">
                                    <c:forEach items="${descList}" var="x" varStatus="i">
                                        <option value="${x.desc}"></option>
                                    </c:forEach>
                                </datalist>
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Width/Size :</h4></td>
                            <td width="30%" align="left" style="padding-right: 90px;"><input type="text" value="${width}" id="width" name="width" maxlength="20"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left">
                            <td width="30%" align="left"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Unit :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;">
                                <input type="text" id="unit" name="unit" maxlength="10" value="${unit}" list="unitList">
                                <datalist id="unitList">
                                    <c:forEach items="${unitList}" var="x" varStatus="i">
                                        <option value="${x.unit}"></option>
                                    </c:forEach>
                                </datalist>
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Price :</h4></td>
                            <td width="30%" align="left" style="padding-right: 130px;">
                                <input type="text" id="price" name="price" maxlength="12" value="${price}" onkeypress="return isNumberKey(event)" list="priceList">
                                <datalist id="priceList">
                                    <c:forEach items="${priceList}" var="x" varStatus="i">
                                        <option value="${x.price}"></option>
                                    </c:forEach>
                                </datalist>
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>

                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Brand :</h4></td>
                            <td width="30%" align="left" style="padding-right: 130px;">
                                <input type="text" id="brand" name="brand" maxlength="20" value="${brand}">
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 90px;"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left">
                            <td width="30%" align="left">
                                <input style="width: 100px;" type="button" value="Cancel" onclick="window.location.href = '/TABLE2/MSS003/display?uid=${uidx}'"/>
                                <input style="width: 100px;" type="submit" formmethod="post" value="Confirm" />
                            </td>
                        </tr>
                    </table>
                    <table style="${purShow}" id="pur" frame="box" width="100%" bordercolor="#e0e0e0">
                        <tr>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8">CREATE MODE</th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h3>Purchase Information</h3></td>
                            <td width="30%" align="left">
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Material Group :</h4></td>
                            <td width="30%" align="left" style="padding-right: 130px;">
                                <input type="text" id="matGroup2" name="matGroup" maxlength="20" onkeyup="Uppercase(this)" onchange="mgx(this.value);
                                        this.form.submit();" value="${matGroup}" list="matGroupList">
                                <datalist id="matGroupList">
                                    <c:forEach items="${matGroupList}" var="x" varStatus="i">
                                        <option value="${x.matGroup}"></option>
                                    </c:forEach>
                                </datalist>
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Company :</h4></td>
                            <td width="30%" align="left" style="padding-right: 90px;"><input type="text" value="${comp}" id="comp" name="comp" maxlength="50"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left">
                            <td width="30%" align="left">
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Description of Goods(THAI) :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;"><input type="text" value="${descT}" id="descT" name="descT" maxlength="225"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left">
                            <td width="30%" align="left">
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 90px;"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left">
                            <td width="30%" align="left">
                                <input style="width: 100px;" type="button" value="Cancel" onclick="window.location.href = '/TABLE2/MSS003/display?uid=${uidx}'"/>
                                <input style="width: 100px;" type="submit" formmethod="post" value="Confirm" />
                            </td>
                        </tr>
                    </table>
                    <table style="${impShow}" id="imp" frame="box" width="100%" bordercolor="#e0e0e0">
                        <tr>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8">CREATE MODE</th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h3>Import Information</h3></td>
                            <td width="30%" align="left">
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left">
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Material Group :</h4></td>
                            <td width="30%" align="left" style="padding-right: 130px;">
                                <input type="text" id="matGroup3" name="matGroup" maxlength="20" onkeyup="Uppercase(this)" onchange="mgx(this.value);
                                        this.form.submit();" value="${matGroup}" list="matGroupList">
                                <datalist id="matGroupList">
                                    <c:forEach items="${matGroupList}" var="x" varStatus="i">
                                        <option value="${x.matGroup}"></option>
                                    </c:forEach>
                                </datalist>
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>H.S. Code :</h4></td>
                            <td width="30%" align="left" style="padding-right: 130px;"><input type="text" value="${hsc}" id="hsc" name="hsc" maxlength="12"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Tariff Code :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;"><input type="text" id="taf" value="${taf}" name="taf" maxlength="20"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Cost Asean Ref#H.S. Code :</h4></td>
                            <td width="30%" align="left"><input type="text" value="${refHS}" id="refHS" name="refHS" maxlength="12" onkeypress="return isNumberKey(event)"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;">
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>#REF NO. COST ASEAN :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;"><input type="text" value="${ref}" id="ref" name="ref" maxlength="12" onkeypress="return isNumberKey(event)"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Country of Origin :</h4></td>
                            <td width="30%" align="left" style="padding-right: 130px;"><input type="text" value="${country}" id="country" name="country" maxlength="20"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Unit CUSTOMS :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="text" value="${unitC}" id="unitC" name="unitC" maxlength="10"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Issue Date :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="date" value="${issDate}" id="issDate" name="issDate" onchange="IssCH();"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;">
                        </tr><tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>No of Days :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="number" value="${nod}" id="nod" name="nod" max="99999" onchange="IssCH();"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Expiration Date :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="date" value="${expDate}" id="expDate" name="expDate" readonly></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;">
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Warning Date :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="date" value="${warnDate}" id="warnDate" name="warnDate" readonly></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 100px;">
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 90px;"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left">
                            <td width="30%" align="left"><input style="width: 100px;" type="button" value="Cancel" onclick="window.location.href = '/TABLE2/MSS003/display?uid=${uidx}'"/>
                                <input style="width: 100px;" type="submit" formmethod="post" value="Confirm" /></td>
                        </tr>
                    </table>
                    <center>
                        <div id="myModal" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Duplicate Material Group !</h4>
                                        <br>
                                        <button name="ok" type="button" onclick="window.location.href = '/TABLE2/MSS003/create?dept=${dept}'">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Please fill in the blanks !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal2" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Create Failed !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>