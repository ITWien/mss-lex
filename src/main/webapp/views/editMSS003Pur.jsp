<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>EDIT MODE</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <!--<script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>-->
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        <script>
            function Uppercase() {
                var x = document.getElementById("matGroup");
                x.value = x.value.toUpperCase();
            }
        </script>
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            };

            function validateForm() {
//                var mg = document.forms["frm"]["matGroup"].value;
//
//                if (mg.trim() === "") {
//                    window.setTimeout(show1, 0);
//                    document.getElementById("matGroup").style.cssText = "border: 2px solid #ff9999";
//                    return false;
//                } else {
//                    document.getElementById("matGroup").style.cssText = "border: 1px solid #ccc";
//                }
            }
        </script>
        <script>
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 46 || charCode > 57))
                    return false;
                return true;
            }
        </script>
        <style>
            input[type=text], input[type=password], input[type=number], input[type=date], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }
        </style>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->

                <%-- PART 3 --%>
                <br>
                <form action="edit" method="post" name="frm" onsubmit="return validateForm()">
                    <input type="hidden" id="userid" name="userid">
                    <input type="hidden" id="dept" name="dept" value="${dept}">
                    <input type="hidden" id="uidx" name="uidx" value="${uidx}">
                    <table id="pur" frame="box" width="100%" bordercolor="#e0e0e0">
                        <tr>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8">EDIT MODE : PURCHASE</th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                            <th height="50" bgcolor="#f8f8f8"></th>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h3>Purchase Information</h3></td>
                            <td width="30%" align="left">
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h3>Import Information</h3></td>
                            <td width="30%" align="left">
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Material Group :</h4></td>
                            <td width="30%" align="left" style="padding-right: 130px;">
                                <input type="text" id="matGroup" name="matGroup" maxlength="20" onkeyup="Uppercase(this)" onchange="mgx(this.value); this.form.submit();" value="${matGroup}" list="matGroupList" style="background-color:  #ccc;" readonly>
                                <datalist id="matGroupList">
                                    <c:forEach items="${matGroupList}" var="x" varStatus="i">
                                        <option value="${x.matGroup}"></option>
                                    </c:forEach>
                                </datalist>
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>H.S. Code :</h4></td>
                            <td width="30%" align="left" style="padding-right: 130px;"><input type="text" value="${hsc}" id="hsc" name="hsc" maxlength="12" style="background-color:  #ccc;" readonly></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Company :</h4></td>
                            <td width="30%" align="left" style="padding-right: 90px;"><input type="text" value="${comp}" id="comp" name="comp" maxlength="50"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Tariff Code :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;"><input type="text" id="taf" value="${taf}" name="taf" maxlength="20" style="background-color:  #ccc;" readonly></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Description of Goods(THAI) :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;"><input type="text" value="${descT}" id="descT" name="descT" maxlength="225"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Cost Asean Ref#H.S. Code :</h4></td>
                            <td width="30%" align="left"><input type="text" value="${refHS}" id="refHS" name="refHS" maxlength="12" onkeypress="return isNumberKey(event)" style="background-color:  #ccc;" readonly></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 20px;">
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>#REF NO. COST ASEAN :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;"><input type="text" value="${ref}" id="ref" name="ref" maxlength="12" onkeypress="return isNumberKey(event)" style="background-color:  #ccc;" readonly></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h3>Business Information</h3></td>
                            <td width="30%" align="left" style="padding-right: 90px;">
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Country of Origin :</h4></td>
                            <td width="30%" align="left" style="padding-right: 130px;"><input type="text" value="${country}" id="country" name="country" maxlength="20" style="background-color:  #ccc;" readonly></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Description of Goods :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;">
                                <input type="text" id="dog" name="dog" maxlength="225" value="${dog}" style="background-color:  #ccc;" readonly >
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Unit CUSTOMS :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="text" value="${unitC}" id="unitC" name="unitC" maxlength="10" style="background-color:  #ccc;" readonly></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Composition :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;">
                                <input type="text" id="composit" name="composit" maxlength="225" value="${composit}" style="background-color:  #ccc;" readonly>
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Issue Date :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="date" value="${issDate}" id="issDate" name="issDate" style="background-color:  #ccc;" readonly></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Description (SAP) :</h4></td>
                            <td width="30%" align="left" style="padding-right: 20px;">
                                <input type="text" id="desc" name="desc" maxlength="225" value="${desc}" list="descList" style="background-color:  #ccc;" readonly>
                                <datalist id="descList">
                                    <c:forEach items="${descList}" var="x" varStatus="i">
                                        <option value="${x.desc}"></option>
                                    </c:forEach>
                                </datalist>
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>No of Days :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="number" value="${nod}" id="nod" name="nod" max="99999" style="background-color:  #ccc;" readonly></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Width/Size :</h4></td>
                            <td width="30%" align="left" style="padding-right: 90px;"><input type="text" value="${width}" id="width" name="width" maxlength="20" style="background-color:  #ccc;" readonly></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Expiration Date :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="date" value="${expDate}" id="expDate" name="expDate" style="background-color:  #ccc;" readonly></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Unit :</h4></td>
                            <td width="30%" align="left" style="padding-right: 200px;">
                                <input type="text" id="unit" name="unit" maxlength="10" value="${unit}" list="unitList" style="background-color:  #ccc;" readonly>
                                <datalist id="unitList">
                                    <c:forEach items="${unitList}" var="x" varStatus="i">
                                        <option value="${x.unit}"></option>
                                    </c:forEach>
                                </datalist>
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Warning Date :</h4></td>
                            <td width="30%" align="left" style="padding-right: 300px;"><input type="date" value="${warnDate}" id="warnDate" name="warnDate" style="background-color:  #ccc;" readonly></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Price :</h4></td>
                            <td width="30%" align="left" style="padding-right: 130px;">
                                <input type="text" id="price" name="price" maxlength="12" value="${price}" onkeypress="return isNumberKey(event)" list="priceList" style="background-color:  #ccc;" readonly>
                                <datalist id="priceList">
                                    <c:forEach items="${priceList}" var="x" varStatus="i">
                                        <option value="${x.price}"></option>
                                    </c:forEach>
                                </datalist>
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 300px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"><h4>Brand :</h4></td>
                            <td width="30%" align="left" style="padding-right: 130px;">
                                <input type="text" id="brand" name="brand" maxlength="20" value="${brand}" style="background-color:  #ccc;" readonly>
                            </td>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 300px;"></td>
                        </tr>
                        <tr>
                            <td width="5%" align="left">
                            <td width="15%" align="left"></td>
                            <td width="30%" align="left" style="padding-right: 90px;"></td>
                            <td width="5%" align="left">
                            <td width="15%" align="left">
                            <td width="30%" align="left">
                                <input style="width: 100px;" type="button" value="Cancel" onclick="window.location.href = '/TABLE2/MSS003/display?uid=${uidx}'"/>
                                <input style="width: 100px;" type="submit" formmethod="post" value="Confirm" />
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Please fill in the blanks !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal2" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Edit Failed !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>