<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>MSS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>
        <!--<script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>-->
        <script type="text/javascript"> /* add something code to your script */</script>
        ${sendMessage}
        ${showPDF}
        <script>
            function Customer() {
                var x = document.getElementById("customer");
                x.value = x.value.toUpperCase();

                var cus = document.getElementById("customer");
                var name = "";
                var ad1 = "";
                var ad2 = "";
            <c:forEach items="${cusList}" var="p" varStatus="i">
                if ("${p.code}" === cus.value) {
                    name = "${p.name}";
                    ad1 = "${p.addr1}";
                    ad2 = "${p.addr2}";
                    document.getElementById("invoice").disabled = false;
                    document.getElementById("invoice").style.cssText = "background-color: #ffffff";
                    document.getElementById("date").disabled = false;
                    document.getElementById("date").style.cssText = "height: 35px; background-color: #ffffff";
                }
            </c:forEach>
                document.getElementById("cusname").innerHTML = name;
                document.getElementById("comname").value = name;
                document.getElementById("comaddr1").value = ad1;
                document.getElementById("comaddr2").value = ad2;
            }

            function Uppercase() {
                var x = document.getElementById("customer");
                x.value = x.value.toUpperCase();

                var y = document.getElementById("shipFrom");
                y.value = y.value.toUpperCase();

                var z = document.getElementById("shipTo");
                z.value = z.value.toUpperCase();

                var a = document.getElementById("via");
                a.value = a.value.toUpperCase();

                var b = document.getElementById("cur");
                b.value = b.value.toUpperCase();
            }

            function CN() {
                var elm = document.getElementById('cn1from'),
                        df = document.createDocumentFragment(),
                        inv = document.getElementById('invoice').value;
                var cn = inv.split(" - ");
                elm.options.length = 0;
                var option = document.createElement('option');
                option.value = "";
                option.appendChild(document.createTextNode("From"));
                df.appendChild(option);
                for (var i = 1; i <= cn[1]; i++) {
                    var option = document.createElement('option');
                    option.value = i;
                    option.appendChild(document.createTextNode(i));
                    df.appendChild(option);
                }
                elm.appendChild(df);

                var elm = document.getElementById('cn2from'),
                        df = document.createDocumentFragment(),
                        inv = document.getElementById('invoice').value;
                var cn = inv.split(" - ");
                elm.options.length = 0;
                var option = document.createElement('option');
                option.value = "";
                option.appendChild(document.createTextNode("From"));
                df.appendChild(option);
                for (var i = 1; i <= cn[1]; i++) {
                    var option = document.createElement('option');
                    option.value = i;
                    option.appendChild(document.createTextNode(i));
                    df.appendChild(option);
                }
                elm.appendChild(df);

                var elm = document.getElementById('cn3from'),
                        df = document.createDocumentFragment(),
                        inv = document.getElementById('invoice').value;
                var cn = inv.split(" - ");
                elm.options.length = 0;
                var option = document.createElement('option');
                option.value = "";
                option.appendChild(document.createTextNode("From"));
                df.appendChild(option);
                for (var i = 1; i <= cn[1]; i++) {
                    var option = document.createElement('option');
                    option.value = i;
                    option.appendChild(document.createTextNode(i));
                    df.appendChild(option);
                }
                elm.appendChild(df);

                //************************************

                var elm = document.getElementById('cn1to'),
                        df = document.createDocumentFragment(),
                        inv = document.getElementById('invoice').value;
                var cn = inv.split(" - ");
                elm.options.length = 0;
                var option = document.createElement('option');
                option.value = "";
                option.appendChild(document.createTextNode("To"));
                df.appendChild(option);
                for (var i = cn[1]; i >= 1; i--) {
                    var option = document.createElement('option');
                    option.value = i;
                    option.appendChild(document.createTextNode(i));
                    df.appendChild(option);
                }
                elm.appendChild(df);

                var elm = document.getElementById('cn2to'),
                        df = document.createDocumentFragment(),
                        inv = document.getElementById('invoice').value;
                var cn = inv.split(" - ");
                elm.options.length = 0;
                var option = document.createElement('option');
                option.value = "";
                option.appendChild(document.createTextNode("To"));
                df.appendChild(option);
                for (var i = cn[1]; i >= 1; i--) {
                    var option = document.createElement('option');
                    option.value = i;
                    option.appendChild(document.createTextNode(i));
                    df.appendChild(option);
                }
                elm.appendChild(df);

                var elm = document.getElementById('cn3to'),
                        df = document.createDocumentFragment(),
                        inv = document.getElementById('invoice').value;
                var cn = inv.split(" - ");
                elm.options.length = 0;
                var option = document.createElement('option');
                option.value = "";
                option.appendChild(document.createTextNode("To"));
                df.appendChild(option);
                for (var i = cn[1]; i >= 1; i--) {
                    var option = document.createElement('option');
                    option.value = i;
                    option.appendChild(document.createTextNode(i));
                    df.appendChild(option);
                }
                elm.appendChild(df);

                var inv = document.getElementById("invoice");
                if (inv.value !== "") {
                    document.getElementById("cn1from").disabled = false;
                    document.getElementById("cn1from").style.cssText = "background-color: #ffffff";
                    document.getElementById("cn1to").disabled = false;
                    document.getElementById("cn1to").style.cssText = "background-color: #ffffff";
                    document.getElementById("cn2from").disabled = false;
                    document.getElementById("cn2from").style.cssText = "background-color: #ffffff";
                    document.getElementById("cn2to").disabled = false;
                    document.getElementById("cn2to").style.cssText = "background-color: #ffffff";
                    document.getElementById("cn3from").disabled = false;
                    document.getElementById("cn3from").style.cssText = "background-color: #ffffff";
                    document.getElementById("cn3to").disabled = false;
                    document.getElementById("cn3to").style.cssText = "background-color: #ffffff";
                }
            }
        </script>
        <script>
            var show1 = function () {
                $('#myModal1').modal('show');
            }
            ;

            var show2 = function () {
                $('#myModal2').modal('show');
            };

            function validateForm() {
                var cus = document.forms["frm"]["customer"].value;
                var inv = document.forms["frm"]["invoice"].value;
                var dt = document.forms["frm"]["date"].value;
                var ins = document.forms["frm"]["insur"].value;
                var fr = document.forms["frm"]["fr"].value;

                var cn1from = document.forms["frm"]["cn1from"].value;
                var cn1to = document.forms["frm"]["cn1to"].value;
                var cn2from = document.forms["frm"]["cn2from"].value;
                var cn2to = document.forms["frm"]["cn2to"].value;
                var cn3from = document.forms["frm"]["cn3from"].value;
                var cn3to = document.forms["frm"]["cn3to"].value;

                var c1f = parseInt(cn1from);
                var c1t = parseInt(cn1to);
                var c2f = parseInt(cn2from);
                var c2t = parseInt(cn2to);
                var c3f = parseInt(cn3from);
                var c3t = parseInt(cn3to);

                if (c1t < c1f) {
                    window.setTimeout(show2, 0);
                    document.getElementById("cn1to").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("cn1to").style.cssText = "border: 1px solid #ccc";
                }

                if (c2t < c2f) {
                    window.setTimeout(show2, 0);
                    document.getElementById("cn2to").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("cn2to").style.cssText = "border: 1px solid #ccc";
                }

                if (c3t < c3f) {
                    window.setTimeout(show2, 0);
                    document.getElementById("cn3to").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("cn3to").style.cssText = "border: 1px solid #ccc";
                }

                if (cus.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("customer").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("customer").style.cssText = "border: 1px solid #ccc";
                }

                if (inv.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("invoice").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("invoice").style.cssText = "border: 1px solid #ccc";
                }

                if (dt.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("date").style.cssText = "border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("date").style.cssText = "border: 1px solid #ccc";
                }

                if (ins.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("insur").style.cssText = "width: 55%; border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("insur").style.cssText = "width: 55%; border: 1px solid #ccc";
                }

                if (fr.trim() === "") {
                    window.setTimeout(show1, 0);
                    document.getElementById("fr").style.cssText = "width: 55%; border: 2px solid #ff9999";
                    return false;
                } else {
                    document.getElementById("fr").style.cssText = "width: 55%; border: 1px solid #ccc";
                }
            }
        </script>
        <script>
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 46 || charCode > 57))
                    return false;
                return true;
            }
        </script>
        <style>
            input[type=text], input[type=password], input[type=number], input[type=date], select {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            button[type=submit] {
                width: 100%;
                background-color: #2bd14a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[type=submit]:hover {
                background-color: #45a049;
            }

            input[type=button] {
                width: 100%;
                background-color: #ef604a;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=button]:hover {
                background-color: #e51e00;
            }

            button[name=ok] {
                width: 25%;
                background-color: #008cff;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            button[name=ok]:hover {
                background-color: #008cff;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }
        </style>
    </head>    
    <body >
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--                <div id="wrapper-top" align="left">
                <%-- PART 2 --%>
                <div class="row">
                    <div class="col-lg-12">
                        <div id="set-height" style="height:415px;margin-top:0px;">
                            <div id="sidebar-wrapper-top" class="">
                                <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
    </div>
    </div> -->

                <%-- PART 3 --%>
                <form action="../resources/manual/MSS212.pdf" target="_blank">
                    <table width="100%">
                        <tr>
                            <td width="94%"></td>
                            <td width="6%" align="right"><input type="submit" value="Manual" /></td>
                        </tr>
                    </table>
                </form>
                <form action="print" method="post" name="frm" onsubmit="return validateForm()" target="_blank">
                    <table frame="box" width="100%" bordercolor="#e0e0e0">
                        <br>
                        <tr>
                            <td width="3%" align="left">
                            <td width="15%" align="left"><b style=" color: #00399b; font-size: 19px;">Customer : </b></td>
                            <td width="20%" align="left"><input type="text" id="customer" name="customer" onkeyup="return Customer();"></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left"><b id="cusname"></b><input id="comname" name="comname" type="text" style=" display: none">
                                <input id="comaddr1" name="comaddr1" type="text" style=" display: none">
                                <input id="comaddr2" name="comaddr2" type="text" style=" display: none">
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td width="3%" align="left">
                            <td width="15%" align="left"><b style=" color: #00399b; font-size: 19px;">Invoice No. : </b></td>
                            <td width="20%" align="left"><select id="invoice" name="invoice" style=" background-color: #ddd" onchange="CN()" disabled>
                                    <option value="" hidden selected></option>
                                    <c:forEach items="${invList}" var="p" varStatus="i">
                                        <option value="${p.desc} - ${p.unit}">${p.desc}</option>
                                    </c:forEach>
                                </select></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td width="3%" align="left">
                            <td width="15%" align="left"><b style=" color: #00399b; font-size: 19px;">Date : </b></td>
                            <td width="20%" align="left"><input type="date" id="date" name="date" disabled style=" height: 35px; background-color: #ddd"></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td width="3%" align="left">
                            <td width="15%" align="left"><b style=" color: #00399b; font-size: 19px;">C/N [1] : </b></td>
                            <td width="20%" align="left"><select name="cn1from" id="cn1from" style="background-color: #ddd" disabled>

                                </select></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left"><select name="cn1to" id="cn1to" style=" background-color: #ddd" disabled>

                                </select></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td width="3%" align="left">
                            <td width="15%" align="left"><b style=" color: #00399b; font-size: 19px;">C/N [2] : </b></td>
                            <td width="20%" align="left"><select name="cn2from" id="cn2from" style=" background-color: #ddd" disabled>

                                </select></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left"><select name="cn2to" id="cn2to" style=" background-color: #ddd" disabled>

                                </select></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td width="3%" align="left">
                            <td width="15%" align="left"><b style=" color: #00399b; font-size: 19px;">C/N [3] : </b></td>
                            <td width="20%" align="left"><select name="cn3from" id="cn3from" style=" background-color: #ddd" disabled>

                                </select></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left"><select name="cn3to" id="cn3to" style=" background-color: #ddd" disabled>

                                </select></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="10%" align="left">
                        </tr>
                        <tr style="border-top: 1px solid #ddd;">
                            <td width="3%" align="left">
                            <td width="15%" align="left"><b style=" color: #00399b; font-size: 19px;">SHIPPED FROM : </b></td>
                            <td width="20%" align="left"><input type="text" id="shipFrom" name="shipFrom" onkeyup="return Uppercase();"></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left"><b style=" color: #00399b; font-size: 19px;">TO : </b><input type="text" id="shipTo" name="shipTo" style=" width: 75%" onkeyup="return Uppercase();"></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left"><b style=" color: #00399b; font-size: 19px;">VIA : </b><input type="text" id="via" name="via" style=" width: 75%" onkeyup="return Uppercase();"></td>
                            <td width="10%" align="left">
                        </tr>
                        <tr style="border-top: 1px solid #ddd;">
                            <td width="3%" align="left">
                            <td width="15%" align="left"><b style=" color: #00399b; font-size: 19px;">INSURANCE : </b></td>
                            <td width="20%" align="left"><input type="text" id="insur" name="insur" style=" width: 55%" onkeypress="return isNumberKey(event)"></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td width="3%" align="left">
                            <td width="15%" align="left"><b style=" color: #00399b; font-size: 19px;">FREIGHT : </b></td>
                            <td width="20%" align="left"><input type="text" id="fr" name="fr" style=" width: 55%" onkeypress="return isNumberKey(event)"></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="10%" align="left">
                        </tr>
                        <tr style="border-bottom: 1px solid #ddd;">
                            <td width="3%" align="left">
                            <td width="15%" align="left"><b style=" color: #00399b; font-size: 19px;">CURRENCY : </b></td>
                            <td width="20%" align="left"><input type="text" id="cur" name="cur" style=" width: 55%" onkeyup="return Uppercase();"></td>
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="10%" align="left">
                        </tr>
                        <tr>
                            <td width="3%" align="left">
                            <td width="15%" align="left">
                            <td width="20%" align="left">
                            <td width="2%" align="left">
                            <td width="20%" align="left"><button style="width: 200px;" type="submit"/><i class="fa fa-print"></i> Print MSS212</button>
                            <td width="2%" align="left">
                            <td width="20%" align="left">
                            <td width="10%" align="left">
                        </tr>
                    </table>
                    <br>
                    <center>
                        <div id="myModal1" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">Please fill in the blanks !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                    <center>
                        <div id="myModal2" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-sm"> <!-- ???????????? modal ?????????????? ???? xs, sm, md, lg -->
                                <div class="modal-md col-md-6" style="top: 150px; left:300px; background-color: #ddd; border: 1px solid #ccc; border-radius: 4px;">
                                    <div class="modal-header">
                                        <h4 class="modal-title" align="center">C/N[](To) must be greater than or equal to C/N[](From) !</h4>
                                        <br>
                                        <button name="ok" type="button" class="close" data-dismiss="modal">
                                            <font color = "white">OK</font>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- devbanban.com -->
                    </center>
                </form>
                <!--End Part 3-->
                <!--</div>  end #wrapper-top -->
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->


    </body>

</html>