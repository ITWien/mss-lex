<%@ include file="../fragments/taglibs.jsp" %>
<%@ include file="../fragments/imports.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*;" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>MSS</title>
        <!-- css :: vendors -->
        <jsp:include page = "../fragments/css.jsp" />
        <!-- additional custom :: my-style -->
        <link rel="stylesheet" href="../resources/assets/styles/myStyles.css">

        <!-- js :: vendors -->
        <jsp:include page = "../fragments/script.jsp" />
        <!-- additional custom :: my-script -->
        <script src="../resources/assets/scripts/myScripts.js" async></script>
        <script src="../resources/assets/scripts/toggleLoad.js" async></script>  
        <!--<script src="../../WMSMenuControl/resources/assets/scripts/isLogin.js"></script>-->  
        <script>
            $(document).ready(function () {
                $('#showTable').DataTable({
                    "paging": false,
                    "ordering": false,
                    "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
                    "bSortClasses": false,
                    columnDefs: [
//                        {orderable: false, targets: [10]},
                        {"width": "15%", "targets": 1},
                        {"width": "4%", "targets": 3},
                        {"width": "4%", "targets": 4},
                    ]
                });
                // Setup - add a text input to each footer cell
//                $('#showTable tfoot th').each(function () {
//                    var title = $(this).text();
//                    $(this).html('<input type="text" />');
//                });
                // DataTable
//                var table = $('#showTable').DataTable();
                // Apply the search
//                table.columns().every(function () {
//                    var that = this;
//                    $('input', this.footer()).on('keyup change', function () {
//                        if (that.search() !== this.value) {
//                            that
//                                    .search(this.value)
//                                    .draw();
//                        }
//                    });
//                });
            });
        </script>
        <style>
            input[type=text], select, input[type=date] {
                width: 100%;
                padding: 5px 5px;
                margin: 8px 0;
                display: inline-block;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            input[type=submit] {
                width: 100%;
                background-color: #00399b;
                color: white;
                padding: 5px 5px;
                margin: 8px 0;
                border: none;
                border-radius: 4px;
                cursor: pointer;
            }

            input[type=submit]:hover {
                background-color: #3973d6;
            }

            button[type=submit] {
                background: transparent;
                border: none !important;
            }

            i[id=ic]:hover {
                background-color: #042987;
                border-radius: 15px;
            }

            i[id=ic2]:hover {
                background-color: #165910;
                border-radius: 15px;
            }
        </style>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
        <script>
            function currencyFormat(num, fp) {
                var pp = Math.pow(10, fp);
                num = Math.round(num * pp) / pp;
                return num.toFixed(fp).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
            }

            function SUM() {
                var input = document.getElementsByTagName('input');

                var totcif = 0.00;
                var totins = 0.00;
                var totfre = 0.00;
                var totfob = 0.00;
                var totctn = 0;
                var totmeter = 0.00;
                var totpc = 0.00;
                var totset = 0.00;
                var totcone = 0.00;
                var totkgs = 0.00;
                var totnw = 0.00;
                var totgw = 0.00;
                var totm3 = 0.000;


                for (var i = 0; i < input.length; i++) {
                    if (input[i].name === 'cif') {
                        if (input[i].value.toString().trim() === "" || input[i].value.toString().trim() === "-") {
                            totcif += 0;
                        } else {
                            totcif += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'ins') {
                        if (input[i].value.toString().trim() === "" || input[i].value.toString().trim() === "-") {
                            totins += 0;
                        } else {
                            totins += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'fre') {
                        if (input[i].value.toString().trim() === "" || input[i].value.toString().trim() === "-") {
                            totfre += 0;
                        } else {
                            totfre += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'fob') {
                        if (input[i].value.toString().trim() === "" || input[i].value.toString().trim() === "-") {
                            totfob += 0;
                        } else {
                            totfob += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'ctn') {
                        if (input[i].value.toString().trim() === "" || input[i].value.toString().trim() === "-") {
                            totctn += 0;
                        } else {
                            totctn += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'meter') {
                        if (input[i].value.toString().trim() === "" || input[i].value.toString().trim() === "-") {
                            totmeter += 0;
                        } else {
                            totmeter += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'pc') {
                        if (input[i].value.toString().trim() === "" || input[i].value.toString().trim() === "-") {
                            totpc += 0;
                        } else {
                            totpc += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'set') {
                        if (input[i].value.toString().trim() === "" || input[i].value.toString().trim() === "-") {
                            totset += 0;
                        } else {
                            totset += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'cone') {
                        if (input[i].value.toString().trim() === "" || input[i].value.toString().trim() === "-") {
                            totcone += 0;
                        } else {
                            totcone += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'kgs') {
                        if (input[i].value.toString().trim() === "" || input[i].value.toString().trim() === "-") {
                            totkgs += 0;
                        } else {
                            totkgs += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'nw') {
                        if (input[i].value.toString().trim() === "" || input[i].value.toString().trim() === "-") {
                            totnw += 0;
                        } else {
                            totnw += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'gw') {
                        if (input[i].value.toString().trim() === "" || input[i].value.toString().trim() === "-") {
                            totgw += 0;
                        } else {
                            totgw += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    } else if (input[i].name === 'm3') {
                        if (input[i].value.toString().trim() === "" || input[i].value.toString().trim() === "-") {
                            totm3 += 0;
                        } else {
                            totm3 += parseFloat(input[i].value.replace(/,/g, ''));
                        }
                    }
                }

                document.getElementById('tot-cif').innerHTML = currencyFormat(totcif, 2);
                document.getElementById('tot-ins').innerHTML = currencyFormat(totins, 2);
                document.getElementById('tot-fre').innerHTML = currencyFormat(totfre, 2);
                document.getElementById('tot-fob').innerHTML = currencyFormat(totfob, 2);
                document.getElementById('tot-ctn').innerHTML = currencyFormat(totctn, 0);
                document.getElementById('tot-meter').innerHTML = currencyFormat(totmeter, 2);
                document.getElementById('tot-pc').innerHTML = currencyFormat(totpc, 2);
                document.getElementById('tot-set').innerHTML = currencyFormat(totset, 2);
                document.getElementById('tot-cone').innerHTML = currencyFormat(totcone, 2);
                document.getElementById('tot-kgs').innerHTML = currencyFormat(totkgs, 2);
                document.getElementById('tot-nw').innerHTML = currencyFormat(totnw, 2);
                document.getElementById('tot-gw').innerHTML = currencyFormat(totgw, 2);
                document.getElementById('tot-m3').innerHTML = currencyFormat(totm3, 3);

            }

            function UpdateFOB(idx) {
                var ac = document.getElementById('cif-' + idx).value.replace(/,/g, '');
                var ai = document.getElementById('ins-' + idx).value.replace(/,/g, '');
                var af = document.getElementById('fre-' + idx).value.replace(/,/g, '');

                if (ac.toString().trim() === "") {
                    ac = '0';
                }
                if (ai.toString().trim() === "") {
                    ai = '0';
                }
                if (af.toString().trim() === "") {
                    af = '0';
                }

                var cif = parseFloat(ac);
                var ins = parseFloat(ai);
                var fre = parseFloat(af);

                document.getElementById('ins-' + idx).value = currencyFormat(ins, 2);
                document.getElementById('fre-' + idx).value = currencyFormat(fre, 2);
                document.getElementById('fob-' + idx).value = currencyFormat((cif - ins - fre), 2);
                document.getElementById('fob-tmp-' + idx).innerHTML = currencyFormat((cif - ins - fre), 2);

                SUM();

            }

            function UpdateFOBadd() {
                var ac = document.getElementById('add-cif').value.replace(/,/g, '');
                var ai = document.getElementById('add-ins').value.replace(/,/g, '');
                var af = document.getElementById('add-fre').value.replace(/,/g, '');

                if (ac.toString().trim() === "") {
                    ac = '0';
                }
                if (ai.toString().trim() === "") {
                    ai = '0';
                }
                if (af.toString().trim() === "") {
                    af = '0';
                }

                var cif = parseFloat(ac);
                var ins = parseFloat(ai);
                var fre = parseFloat(af);

                document.getElementById('add-ins').value = currencyFormat(ins, 2);
                document.getElementById('add-fre').value = currencyFormat(fre, 2);
                document.getElementById('add-fob').value = currencyFormat((cif - ins - fre), 2);

            }

            function FormatAdd(id, fp) {
                var x = parseFloat(document.getElementById(id).value.replace(/,/g, ''));
                document.getElementById(id).value = currencyFormat(x, fp);

            }

            function AddDet() {
                var inv = document.getElementById('add-inv').value;
                var dept = document.getElementById('add-dept').value;
                var cif = document.getElementById('add-cif').value.replace(/,/g, '');
                var ins = document.getElementById('add-ins').value.replace(/,/g, '');
                var fre = document.getElementById('add-fre').value.replace(/,/g, '');
                var fob = document.getElementById('add-fob').value.replace(/,/g, '');
                var ctn = document.getElementById('add-ctn').value.replace(/,/g, '');
                var meter = document.getElementById('add-meter').value.replace(/,/g, '');
                var pc = document.getElementById('add-pc').value.replace(/,/g, '');
                var set = document.getElementById('add-set').value.replace(/,/g, '');
                var cone = document.getElementById('add-cone').value.replace(/,/g, '');
                var kgs = document.getElementById('add-kgs').value.replace(/,/g, '');
                var nw = document.getElementById('add-nw').value.replace(/,/g, '');
                var gw = document.getElementById('add-gw').value.replace(/,/g, '');
                var m3 = document.getElementById('add-m3').value.replace(/,/g, '');

                var uid = document.getElementById('userid').value;

                var param = "qno=${qno}&inv=" + inv
                        + "&dept=" + dept
                        + "&cif=" + cif
                        + "&ins=" + ins
                        + "&fre=" + fre
                        + "&fob=" + fob
                        + "&ctn=" + ctn
                        + "&meter=" + meter
                        + "&pc=" + pc
                        + "&set=" + set
                        + "&cone=" + cone
                        + "&kgs=" + kgs
                        + "&nw=" + nw
                        + "&gw=" + gw
                        + "&m3=" + m3
                        + "&uid=" + uid;

                if (inv.toString().trim() !== "") {
//                    window.location.href = "addDet?" + param;
                    document.getElementById('to-inv').value = inv;
                    document.getElementById('to-dept').value = dept;
                    document.getElementById('to-cif').value = cif;
                    document.getElementById('to-ins').value = ins;
                    document.getElementById('to-fre').value = fre;
                    document.getElementById('to-fob').value = fob;
                    document.getElementById('to-ctn').value = ctn;
                    document.getElementById('to-meter').value = meter;
                    document.getElementById('to-pc').value = pc;
                    document.getElementById('to-set').value = set;
                    document.getElementById('to-cone').value = cone;
                    document.getElementById('to-kgs').value = kgs;
                    document.getElementById('to-nw').value = nw;
                    document.getElementById('to-gw').value = gw;
                    document.getElementById('to-m3').value = m3;

                    document.getElementById('formAddDet').submit();

                } else {
                    document.getElementById('add-inv').style.cssText = "background-color: #fff0f0;";
                    document.getElementById('myModal-3').style.display = 'block';
                }

            }

            function fun_AllowOnlyAmountAndDot(txt, fp)
            {
                if (event.keyCode > 47 && event.keyCode < 58 || event.keyCode == 46)
                {
                    var txtbx = document.getElementById(txt);
                    var amount = document.getElementById(txt).value;
                    var present = 0;
                    var count = 0;

                    if (amount.indexOf(".", present) || amount.indexOf(".", present + 1))
                        ;
                    {
                        // alert('0');
                    }

                    /*if(amount.length==2)
                     {
                     if(event.keyCode != 46)
                     return false;
                     }*/
                    do
                    {
                        present = amount.indexOf(".", present);
                        if (present != -1)
                        {
                            count++;
                            present++;
                        }
                    } while (present != -1);
                    if (present == -1 && amount.length == 0 && event.keyCode == 46)
                    {
                        event.keyCode = 0;
                        //alert("Wrong position of decimal point not  allowed !!");
                        return false;
                    }

                    if (count >= 1 && event.keyCode == 46)
                    {

                        event.keyCode = 0;
                        //alert("Only one decimal point is allowed !!");
                        return false;
                    }
                    if (count == 1)
                    {
                        var lastdigits = amount.substring(amount.indexOf(".") + 1, amount.length);
                        if (lastdigits.length >= fp)
                        {
                            //alert("Two decimal places only allowed");
                            event.keyCode = 0;
                            return false;
                        }
                    }
                    return true;
                } else
                {
                    event.keyCode = 0;
                    //alert("Only Numbers with dot allowed !!");
                    return false;
                }

            }
        </script>
        <style>
            body {font-family: Arial, Helvetica, sans-serif;}

            /* The Modal (background) */
            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                padding-top: 100px; /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content */
            .modal-content {
                background-color: #fefefe;
                margin: auto;
                padding: 20px;
                border: 1px solid #888;
                width: fit-content;
                height: 320px;
            }

            /* The Close Button */
            .close {
                color: #aaaaaa;
                float: right;
                font-size: 28px;
                font-weight: bold;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }
        </style>
    </head>    
    <body onload="document.getElementById('userid').value = sessionStorage.getItem('uid');
            document.getElementById('userid2').value = sessionStorage.getItem('uid');
            SUM();">
        <div id="wrapper">

            <!-- /#sidebar-wrapper -->
            <%@ include file="../fragments/sidebar.jsp" %>

            <!-- Page Content -->
            <!-- nav-head-custom -->
            <%@ include file="../fragments/nav_head.jsp" %>

            <div class="container-fluid">
                <!--<div id="wrapper-top">-->
                <%-- PART 2 --%>
                <!--                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="set-height" style="height:415px;margin-top:0px;">
                                                <div id="sidebar-wrapper-top" class="">
                                                    <b class="page-header" style="padding-left:5px;font-size:18px;">
                <%-- <hr style="margin:0px;margin-top:10px;margin-bottom:10px;"> --%>
            </b>
        </div>
    </div>
</div>
</div> -->
                <%-- PART 3 --%>
                <!--show Qdest table-->
                <br>
                <div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px;">
                    <table width="100%">
                        <tr>
                            <th width="6%">Loaded Date : </th>
                            <td width="6%">
                                <input type="date" id="loadDateTMP" name="loadDateTMP" value="${head.MSSSIHLOADDT}" style=" height: 35px;" onchange="document.getElementById('loadDate').value = this.value;">
                            </td>
                            <th width="5%" style="text-align: right;">Currency : &nbsp;</th>
                            <td width="5%">
                                <input type="text" id="ccyTMP" name="ccyTMP" value="${head.MSSSIHCUR}" onchange="document.getElementById('ccy').value = this.value;">
                            </td>
                            <th width="5%" style="text-align: right;">Remark : &nbsp;</th>
                            <td width="30%">
                                <input type="text" id="remarkTMP" name="remarkTMP" value="${head.MSSSIHREMARK}" onchange="document.getElementById('remark').value = this.value;">
                            </td>
                            <td width="43%" align="right">
                                <a style="width: 120px;" class="btn btn btn-danger" href="/TABLE2/MSS214/display"> Back </a> 
                            </td>
                        </tr>
                    </table>
                    <form id="formAddDet" action="addDet" method="post">
                        <input type="hidden" id="userid2" name="uid">
                        <input type="hidden" name="qno" value="${qno}">
                        <input type="hidden" name="inv" id="to-inv">
                        <input type="hidden" name="dept" id="to-dept">
                        <input type="hidden" name="cif" id="to-cif">
                        <input type="hidden" name="ins" id="to-ins">
                        <input type="hidden" name="fre" id="to-fre">
                        <input type="hidden" name="fob" id="to-fob">
                        <input type="hidden" name="ctn" id="to-ctn">
                        <input type="hidden" name="meter" id="to-meter">
                        <input type="hidden" name="pc" id="to-pc">
                        <input type="hidden" name="set" id="to-set">
                        <input type="hidden" name="cone" id="to-cone">
                        <input type="hidden" name="kgs" id="to-kgs">
                        <input type="hidden" name="nw" id="to-nw">
                        <input type="hidden" name="gw" id="to-gw">
                        <input type="hidden" name="m3" id="to-m3">
                    </form>
                    <form id="formData" name="formData" action="edit" method="post">
                        <input type="hidden" id="userid" name="userid">
                        <input type="hidden" id="loadDate" name="loadDate" value="${head.MSSSIHLOADDT}">
                        <input type="hidden" id="ccy" name="ccy" value="${head.MSSSIHCUR}">
                        <input type="hidden" id="remark" name="remark" value="${head.MSSSIHREMARK}">
                        <input type="hidden" id="qnoH" name="qnoH" value="${qno}">
                        <table id="showTable" class="display" style="width:100%; border: 1px solid #ccc; border-radius: 4px;">
                            <thead>
                                <tr>
                                    <th>INVOICE NO.</th>
                                    <th>DEPARTMENT</th>
                                    <th style="text-align: right;">CIF</th>
                                    <th style="text-align: right;">INSURANCE</th>
                                    <th style="text-align: right;">FREIGHT</th>
                                    <th style="text-align: right;">FOB</th>
                                    <th style="text-align: right;">CTN</th>
                                    <th style="text-align: right;">METER</th>
                                    <th style="text-align: right;">PC</th>
                                    <th style="text-align: right;">SET</th>
                                    <th style="text-align: right;">CONE</th>
                                    <th style="text-align: right;">KGS</th>
                                    <th style="text-align: right;">NW</th>
                                    <th style="text-align: right;">GW</th>
                                    <th style="text-align: right;">M<sup>3</sup></th>
                                    <th>Option</th>
                                </tr>
                            </thead>
                            <tbody id="det-body">
                                <tr>
                                    <td>
                                        <input type="text" id="add-inv">
                                    </td>
                                    <td>
                                        <input type="text" id="add-dept">
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="add-cif" onchange="UpdateFOBadd();" style="text-align: right;" >
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="add-ins" onchange="UpdateFOBadd();" style="text-align: right;" >
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="add-fre" onchange="UpdateFOBadd();" style="text-align: right;" >
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="add-fob" readonly style="text-align: right; background-color: #f5f5f5;" > 
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="add-ctn" onchange="FormatAdd(this.id, 0);" style="text-align: right;" >
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="add-meter" onchange="FormatAdd(this.id, 2);" style="text-align: right;" >
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="add-pc" onchange="FormatAdd(this.id, 2);" style="text-align: right;" >
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="add-set" onchange="FormatAdd(this.id, 2);" style="text-align: right;" >
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="add-cone" onchange="FormatAdd(this.id, 2);" style="text-align: right;" >
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="add-kgs" onchange="FormatAdd(this.id, 2);" style="text-align: right;" >
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="add-nw" onchange="FormatAdd(this.id, 2);" style="text-align: right;" >
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="add-gw" onchange="FormatAdd(this.id, 2);" style="text-align: right;" >
                                    </td>
                                    <td style="text-align: right;">
                                        <input type="text" id="add-m3" onchange="FormatAdd(this.id, 3);" style="text-align: right;">
                                    </td>
                                    <td>
                                        <a onclick="AddDet();" style="cursor: pointer;"><i class="glyphicon glyphicon-plus-sign" style="font-size:25px; padding-left: 10px"></i></a>
                                        <div id="myModal-3" class="modal">
                                            <!-- Modal content -->
                                            <div class="modal-content" style=" height: 200px; width: 500px;">
                                                <span id="span-3" class="close" onclick="document.getElementById('myModal-3').style.display = 'none'; document.getElementById('add-inv').focus();">&times;</span>
                                                <p><b><font size="4"></font></b></p>
                                                <table width="100%">
                                                    <tr style=" background-color: white;">
                                                        <td align="center">
                                                            <b style="color: #00399b;">
                                                                <font size="5">Please fill INVOICE NO. !</font>
                                                            </b>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <br><br><br>
                                                            <a style="width: 100px;" class="btn btn btn-outline btn-info" onclick="document.getElementById('myModal-3').style.display = 'none'; document.getElementById('add-inv').focus();">
                                                                OK 
                                                            </a>                     
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <c:forEach items="${detail}" var="p" varStatus="i">
                                    <tr>
                                        <td>
                                            <input type="hidden" name="qno" value="${p.MSSSIDQNO}">
                                            ${p.MSSSIDINVNO}
                                            <input type="hidden" name="inv" value="${p.MSSSIDINVNO}">
                                        </td>
                                        <td>
                                            <input type="text" name="dept" value="${p.MSSSIDDEPT}">
                                        </td>
                                        <td style="text-align: right;">
                                            <span id="cif-tmp-${i.index}">${p.MSSSIDCIF}</span>
                                            <input type="hidden" id="cif-${i.index}" name="cif" value="${p.MSSSIDCIF}" style="text-align: right;">
                                        </td>
                                        <td>
                                            <input type="text" id="ins-${i.index}" name="ins" value="${p.MSSSIDINS}" onchange="UpdateFOB('${i.index}');" style="text-align: right;">
                                        </td>
                                        <td>
                                            <input type="text" id="fre-${i.index}" name="fre" value="${p.MSSSIDFRE}" onchange="UpdateFOB('${i.index}');" style="text-align: right;">
                                        </td>
                                        <td style="text-align: right;">
                                            <span id="fob-tmp-${i.index}">${p.MSSSIDFOB}</span>
                                            <input type="hidden" id="fob-${i.index}" name="fob" value="${p.MSSSIDFOB}" style="text-align: right;">
                                        </td>
                                        <td style="text-align: right;">
                                            ${p.MSSSIDCTN}
                                            <input type="hidden" name="ctn" value="${p.MSSSIDCTN}" style="text-align: right;">
                                        </td>
                                        <td style="text-align: right;">
                                            ${p.MSSSIDMETER}
                                            <input type="hidden" name="meter" value="${p.MSSSIDMETER}" style="text-align: right;">
                                        </td>
                                        <td style="text-align: right;">
                                            ${p.MSSSIDPC}
                                            <input type="hidden" name="pc" value="${p.MSSSIDPC}" style="text-align: right;">
                                        </td>
                                        <td style="text-align: right;">
                                            ${p.MSSSIDSET}
                                            <input type="hidden" name="set" value="${p.MSSSIDSET}" style="text-align: right;">
                                        </td>
                                        <td style="text-align: right;">
                                            ${p.MSSSIDCONE}
                                            <input type="hidden" name="cone" value="${p.MSSSIDCONE}" style="text-align: right;">
                                        </td>
                                        <td style="text-align: right;">
                                            ${p.MSSSIDKGS}
                                            <input type="hidden" name="kgs" value="${p.MSSSIDKGS}" style="text-align: right;">
                                        </td>
                                        <td style="text-align: right;">
                                            ${p.MSSSIDNW}
                                            <input type="hidden" name="nw" value="${p.MSSSIDNW}" style="text-align: right;">
                                        </td>
                                        <td style="text-align: right;">
                                            ${p.MSSSIDGW}
                                            <input type="hidden" name="gw" value="${p.MSSSIDGW}" style="text-align: right;">
                                        </td>
                                        <td style="text-align: right;">
                                            ${p.MSSSIDM3}
                                            <input type="hidden" name="m3" value="${p.MSSSIDM3}" style="text-align: right;">
                                        </td>
                                        <td>
                                            <a onclick="document.getElementById('myModal-4-${p.MSSSIDINVNO}').style.display = 'block';" style="cursor: pointer;"><i class="fa fa-trash" style="font-size:25px; padding-left: 10px"></i></a>
                                            <div id="myModal-4-${p.MSSSIDINVNO}" class="modal">
                                                <!-- Modal content -->
                                                <div class="modal-content" style=" height: 200px; width: 500px;">
                                                    <span id="span-4-${p.MSSSIDINVNO}" class="close" onclick="document.getElementById('myModal-4-${p.MSSSIDINVNO}').style.display = 'none';">&times;</span>
                                                    <p><b><font size="4"></font></b></p>
                                                    <table width="100%">
                                                        <tr style=" background-color: white;">
                                                            <td align="center">
                                                                <b style="color: #00399b;">
                                                                    <font size="5">Delete Invoice No. : ${p.MSSSIDINVNO} ?</font>
                                                                </b>
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <br><br><br>
                                                                <a style="width: 100px;" class="btn btn btn-outline btn-danger" onclick="document.getElementById('myModal-4-${p.MSSSIDINVNO}').style.display = 'none';">
                                                                    Cancel 
                                                                </a>    
                                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                                <a style="width: 100px;" class="btn btn btn-outline btn-success" href="deleteDet?qno=${p.MSSSIDQNO}&inv=${p.MSSSIDINVNO}">
                                                                    Confirm 
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                                <tr>
                                    <th>TOTAL</th>
                                    <th></th>
                                    <th style="text-align: right;">
                                        <span id="tot-cif"></span>
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-ins"></span>
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-fre"></span>
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-fob"></span>
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-ctn"></span>
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-meter"></span>
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-pc"></span>
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-set"></span>
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-cone"></span>
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-kgs"></span>
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-nw"></span>
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-gw"></span>
                                    </th>
                                    <th style="text-align: right;">
                                        <span id="tot-m3"></span>
                                    </th>
                                    <th></th>
                                </tr>
                            </tbody>
                        </table>
                        <center>
                            <span class="fa-stack fa-lg fa-2x" onclick="document.getElementById('formData').action = 'edit';
                                    document.getElementById('formData').target = '';
                                    document.getElementById('formData').submit();" style=" cursor: pointer;">
                                <i id="ic" class="fa fa-circle fa-stack-2x" style="color: #154baf"></i>
                                <i id="ic" class="fa fa-save fa-stack-1x fa-inverse"></i>
                            </span>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <span class="fa-stack fa-lg fa-2x" onclick="document.getElementById('formData').action = 'print'; document.getElementById('formData').target = '_blank'; document.getElementById('formData').submit();" style=" cursor: pointer;">
                                <i id="ic2" class="fa fa-circle fa-stack-2x" style="color: #0b8c00"></i>
                                <i id="ic2" class="fa fa-print fa-stack-1x fa-inverse"></i>
                            </span>
                        </center>
                    </form>
                </div>
                <!--End Part 3-->
                <br>
                <!--</div>  end #wrapper-top--> 
            </div> <!-- end .container-fluid -->
        </div> <!-- end #wrapper -->
    </body>
</html>